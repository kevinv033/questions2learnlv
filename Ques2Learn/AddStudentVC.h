//
//  AddStudentVC.h
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NonRotatingUIImagePickerController.h"
@protocol AddStudentVCDelegate
- (void)AddStudentDone:(int)tagAUD ;
@end
@interface AddStudentVC : UIViewController <UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>{
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtAge;
    IBOutlet UITextField *txtGrade;
    IBOutlet UIButton *btnStudPhoto;
    id<AddStudentVCDelegate> delegate;
    UIImagePickerController * pickerPhoto;
    UIPopoverController *popover;
    UIViewController *containerController;
    DAL *objDAL;
    NSString *strStudID;
    NSInteger updateTag;
    IBOutlet UIButton *btnDelete;
    NSString *strSN;
    NSString *strSName;
    id obj;
}
@property (nonatomic, retain) id<AddStudentVCDelegate> delegate;
-(void)deleteImageFile:(NSString*)strFile;
-(UIImage *)rotateImage:(UIImage *)image;
-(void)getStudentDat:(NSString*)strStudID1:(NSInteger)tagUpdate;
-(IBAction)clickDone:(id)sender;
-(IBAction)clickDelete:(id)sender;
-(IBAction)clickChoosePhoto:(id)sender;
@end
