//
//  CreateNewCatVC.h
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsVC.h"
#import "SelectSVC.h"
#import <AVFoundation/AVFoundation.h>
#import "NonRotatingUIImagePickerController.h"
@interface CreateNewCatVC : UIViewController <OptionsVCDelegate,UIPopoverControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate,UINavigationControllerDelegate,UIScrollViewDelegate,SelectSVCDelegate>{
    
    AVAudioRecorder *audioRecorder;
	AVAudioPlayer *audioPlayer;
    UIButton *playButton;
	UIButton *recordButton;
	UIButton *stopButton;
    NSString *strTrackName;
    
    DAL *objDAL;
    SelectSVC *viewSVC;
    OptionsVC *viewOptionsVC;
    UIPopoverController *popOverOVC;
    UIImagePickerController * pickerPhoto;
    UIViewController *containerController;
    int btnTag;
    NSMutableArray *arrCat;
    IBOutlet UITableView *tblCat;
    NSString *strStudID;
    UITextField *txt;
    IBOutlet UITextView *txtQue;
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UIView *view4;
    IBOutlet UIView *view5;
    IBOutlet UITextField *txt1;
    IBOutlet UITextField *txt2;
    IBOutlet UITextField *txt3;
    IBOutlet UITextField *txt4;
    IBOutlet UITextField *txt5;
    IBOutlet UIButton *btn11;
    IBOutlet UIButton *btn22;
    IBOutlet UIButton *btn33;
    IBOutlet UIButton *btn44;
    IBOutlet UIButton *btn55;
    
    IBOutlet UIButton *btnCat;
    IBOutlet UIButton *btnAddCat;
    int btnTG;
    int btnTaG;
    int lvl;
    
    NSMutableArray *arrPhotos;
    int tagModify1;
    int tagModify2;
    int tagModify3;
    int tagModify4;
    int tagModify5;
    
    NSTimer *tmrRec;
    NSMutableDictionary *dicS;
    
    IBOutlet UILabel *lblInstr;
}
@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, retain) IBOutlet UIButton *recordButton;
@property (nonatomic, retain) IBOutlet UIButton *stopButton;
@property (nonatomic, retain) SelectSVC *viewSVC;
@property (nonatomic, retain) OptionsVC *viewOptionsVC;
@property (nonatomic, retain) UIPopoverController *popOverOVC;
-(void)choosePreProgrmmedPhoto:(id)sender;
-(UIImage *)rotateImage:(UIImage *)image;
-(void)getStudID:(NSString*)idStr;
-(IBAction)clickSetOptions:(id)sender;
-(IBAction)clickBack:(id)sender;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickSave:(id)sender;
-(IBAction)clickNextQue:(id)sender;
-(IBAction)clickSetAns:(id)sender event:(id)event;
-(IBAction)clickAddCategory:(id)sender;
-(void)clickDoneAddCategory:(id)sender;
-(IBAction)clickViewCategory:(id)sender;
-(IBAction)clickSetCorrectAns:(id)sender;
-(NSString*)clickSaveImage:(NSData*)dataBtnImg:(int)btn;
-(void)loadCatArr;
-(void)loadNewBlankView;
#pragma mark - Audio & Record custom methods..
-(IBAction) recordAudio;
-(IBAction) playAudio;
-(IBAction) stopPlayOrRecording;
-(void)audioInitiation;
-(void)timerForStop;
@end
