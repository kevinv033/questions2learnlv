//
//  CreateNewCatVC.m
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateNewCatVC.h"

@implementation CreateNewCatVC
@synthesize viewOptionsVC = _viewOptionsVC;
@synthesize popOverOVC = _popOverOVC;
@synthesize viewSVC = _viewSVC;
@synthesize playButton,recordButton,stopButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    strTrackName = @"";
    btnTG = 0;
    btnTaG = 0;
    lvl = 0;
    tagModify1=0;
    tagModify2=0;
    tagModify3=0;
    tagModify4=0;
    tagModify5=0;
    
    playButton.enabled = NO;
	stopButton.enabled = NO;
    [recordButton setBackgroundColor:[UIColor whiteColor]];
    [stopButton setBackgroundColor:[UIColor whiteColor]];
    [playButton setBackgroundColor:[UIColor whiteColor]];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    arrCat = [[NSMutableArray alloc] init];
//    arrCat = [NSMutableArray arrayWithObjects:@"School",@"Home",@"Food & Drinks",@"Community",@"Leisure/Recreational",@"Health", nil];
    btnTag = 0;
    view1.hidden=YES;
    view2.hidden=YES;
    view3.hidden=YES;
    view4.hidden=YES;
    view5.hidden=YES;
    
    arrPhotos = [[NSMutableArray alloc] init];
    dicS = [[NSMutableDictionary alloc] init];
    arrPhotos=[NSMutableArray arrayWithObjects:@"afternoon.png",@"airplane.png",@"apples.png",@"artroom.png",@"artteacher.png",@"auditorium.png",@"baboon.png",@"baby.png",@"backpack.png",@"bag.png",@"ball.png",@"balloons.png",@"bananas.png",@"bandaid.png",@"baseball.png",@"baseball2.png",@"baseballfield.png",@"basket.png",@"basketball.png",@"bathmat.png",@"bathroom.png",@"bathtub.png",@"beach.png",@"beads.png",@"bed.png",@"bedroom.png",@"bike.png",@"bird.png",@"birdflying.png",@"birthdaycake.png",@"blender.png",@"blocks.png",@"blownose.png",@"books.png",@"boy.png",@"bread.png",@"breakfast.png",@"bridge.png",@"broom.png",@"brushing.png",@"bug.png",@"building.png",@"butter.png",@"cabinet.png",@"cactus.png",@"cafeteria.png",@"cafeteriaworker.png",@"cake.png",@"calendar.png",@"camera.png",@"candy.png",@"canoe.png",@"canopener.png",@"cap.png",@"car.png",@"cashier.png",@"cashregister.png",@"cat.png",@"cat4.png",@"catfood.png",@"CDplayer.png",@"cereal.png",@"chair.png",@"chalkboard.png",@"cheerleader.png",@"cheeseburger.png",@"chef.png",@"chickennuggets.png",@"chipmunk.png",@"chipmunk2.png",@"chips.png",@"chocolatecake.png",@"christmastree.png",@"church.png",@"city.png",@"city2.png",@"classroom.png",@"cleaner.png",@"clock.png",@"closet.png",@"clothingstore.png",@"cloud.png",@"clouds.png",@"cloudy.png",@"coat.png",@"coffee.png",@"coffeemaker.png",@"cold.png",@"collar.png",@"coloring.png",@"computer.png",@"computerlab.png",@"computermouse.png",@"conditioner.png",@"container.png",@"cook.png",@"COOKIE.png",@"corn.png",@"couch.png",@"cow.png",@"crackers.png",@"crayons.png",@"creek.png",@"cry.png",@"cup.png",@"cupcake.png",@"dad.png",@"daytime.png",@"dentalfloss.png",@"dentalhygenist.png",@"dentist.png",@"dentistoffice.png",@"desk.png",@"dessert.png",@"detergent.png",@"diningroom.png",@"dinner.png",@"dinosaurs.png",@"dirty.png",@"dirtyclothes.png",@"doctor.png",@"doctoroffice.png",@"dog.png",@"dog3.png",@"dog4.png",@"dogbw.png",@"dogfood.png",@"doghouse.png",@"dogrunning.png",@"dolphin.png",@"dontknow.png",@"door.png",@"downescalator.png",@"dragonfly.png",@"dress.png",@"dresser.png",@"drinking.png",@"drinkingwater.png",@"drums.png",@"dryer.png",@"duck.png",@"ducks.png",@"dullpencil.png",@"earmuffs.png",@"eat.png",@"eggs.png",@"emergency.png",@"empty.png",@"emptyplate.png",@"emptywallet.png",@"fall.png",@"family.png",@"farm.png",@"farmer.png",@"fever.png",@"firefighter.png",@"firehydrant.png",@"fireplace.png",@"fish.png",@"fishfood.png",@"fishtank.png",@"flag.png",@"flashlight.png",@"floor.png",@"flower.png",@"flower2.png",@"flowers2.png",@"flying.png",@"folding.png",@"foodpantry.png",@"football.png",@"footballfield.png",@"forest.png",@"fork.png",@"fountain.png",@"fountain2.png",@"freezer.png",@"friends.png",@"frog.png",@"frozensection.png",@"fruitbowl.png",@"game.png",@"garage.png",@"garden.png",@"getonbus.png",@"gift.png",@"giraffe.png",@"girl.png",@"glasscleaner.png",@"glasses.png",@"gloves.png",@"glue.png",@"grandparents.png",@"grocerystore.png",@"gym.png",@"gymteacher.png",@"hairbrush.png",@"hairstylist.png",@"hammer.png",@"hand.png",@"happy.png",@"hat.png",@"helicopter.png",@"hippo.png",@"hockey.png",@"horses.png",@"hospital.png",@"house.png",@"hurt.png",@"icecream.png",@"interactivewhiteboard.png",@"iron.png",@"jumpinginpool.png",@"kicking.png",@"kitchen.png",@"knife.png",@"lamp.png",@"landscaper.png",@"laptop.png",@"laughing.png",@"laundryroom.png",@"leash.png",@"leaves.png",@"library.png",@"lion.png",@"lips.png",@"livingroom2.png",@"lost.png",@"luggage.png",@"lunch.png",@"mad.png",@"mailbox.png",@"mailbox2.png",@"markers2.png",@"measuringcup.png",@"medicine.png",@"medicine2.png",@"mensbathroom.png",@"menu.png",@"messyhair.png",@"messyroom.png",@"microwave.png",@"military.png",@"mixer.png",@"mobilephone.png",@"mom.png",@"money.png",@"moon.png",@"morning.png",@"mountain2.png",@"mountains.png",@"mouth.png",@"mouth2.png",@"musicroom.png",@"musicteacher.png",@"napkin.png",@"night.png",@"nurse.png",@"ocean.png",@"ocean2.png",@"office.png",@"orangejuice.png",@"oranges.png",@"orangetree.png",@"ot.png",@"oven.png",@"paint.png",@"painting.png",@"pajamas.png",@"pan.png",@"pan2.png",@"pancakes.png",@"paper.png",@"parents.png",@"party.png",@"pear.png",@"pencils.png",@"performance.png",@"pets.png",@"petstore.png",@"physicaltherapist.png",@"piano.png",@"pier.png",@"pillow.png",@"pizza.png",@"plant.png",@"plaque.png",@"plate.png",@"play.png",@"playdough.png",@"PLAYGROUND.png",@"policeofficer.png",@"pond.png",@"pool2.png",@"postalworker.png",@"pretzels.png",@"printer.png",@"produce.png",@"producesection.png",@"puzzle.png",@"rabbit.png",@"raining.png",@"rake.png",@"redflower.png",@"refridgerator.png",@"restaurant.png",@"restaurantkitchen.png",@"rhino.png",@"river2.png",@"rubberbands.png",@"running.png",@"sad.png",@"salad.png",@"sandbox.png",@"sandwich.png",@"saw.png",@"school.png",@"schoolbus.png",@"schoolsupplies.png",@"scissors.png",@"shampoo.png",@"ship.png",@"shirt.png",@"shoes.png",@"shoppingcart.png",@"shorts.png",@"shovel.png",@"shower.png",@"silverware.png",@"sing.png",@"sink.png",@"skateboard.png",@"sleep.png",@"slide.png",@"snow.png",@"soap.png",@"soccerball.png",@"socks.png",@"soda.png",@"soup.png",@"spaghetti.png",@"speechtherapist.png",@"spider.png",@"spoon.png",@"squirrel.png",@"stairs.png",@"stethescope.png",@"stopsign.png",@"stormclouds.png",@"strawberry.png",@"streetsign.png",@"summer.png",@"summer2.png",@"sunset.png",@"swamp.png",@"swan.png",@"swimming.png",@"swimsuit.png",@"table.png",@"talking.png",@"talktoteacher.png",@"tape.png",@"teeth.png",@"tent.png",@"thermometer.png",@"thermos.png",@"tiger.png",@"tired.png",@"tissues.png",@"toaster.png",@"toilet.png",@"toiletpaper.png",@"toothache.png",@"toothbrush.png",@"toothpaste.png",@"towelrack.png",@"towels.png",@"toytrain.png",@"train.png",@"trashcan.png",@"tree.png",@"truck.png",@"turkey.png",@"turtle.png",@"turtle2.png",@"tv.png",@"typing.png",@"umbrella.png",@"upescalator.png",@"vacuum.png",@"vegetables.png",@"veterinarian.png",@"waitingroom.png",@"waitress.png",@"walking.png",@"washer.png",@"washface.png",@"waterfall.png",@"waterfountain.png",@"weekend.png",@"wetclothes.png",@"wheelchair.png",@"winter.png",@"womensbathroom.png",@"woods2.png",@"wrinkledclothes.png",@"write.png",@"xylophone.png",@"yellowtree.png",@"read.png",@"sheets.png",@"mailcarrier.png", nil];
    
    if ([arrCat count]==0) {
        [btnCat setTitle:@"No Category Added" forState:UIControlStateNormal];
    }else {
        [btnCat setTitle:@"View Category" forState:UIControlStateNormal];    
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark-
#pragma mark UIImagePickerController Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
    btn.frame = CGRectMake(6, 5, 85, 88);
    switch (btnTag) {
        case 2:
            tagModify1=1;
            break;
        case 3:
            tagModify2=1;
            break;
        case 4:
            tagModify3=1;
            break;
        case 5:
            tagModify4=1;
            break;
        case 6:
            tagModify5=1;
            break;
            
        default:
            break;
    }
    UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage *imgCopy = [self rotateImage:img];
    [btn setImage:imgCopy forState:UIControlStateNormal];
    ;
    [picker dismissModalViewControllerAnimated:YES];
    [self.popOverOVC dismissPopoverAnimated:YES];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    [self.popOverOVC dismissPopoverAnimated:YES];
}
#pragma mark -
#pragma mark UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrCat count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    cell.textLabel.text = [arrCat objectAtIndex:indexPath.row];
    if (indexPath.row>5) {
    }else {
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (btnAddCat.tag == 10) {
        return 44;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (btnAddCat.tag == 10) {
        UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 285, 44)];
        txt = [[UITextField alloc] initWithFrame:CGRectMake(10, 6.5, 205, 31)];
        txt.borderStyle = UITextBorderStyleRoundedRect;
        txt.placeholder =@"Add New Category";
        UIButton *btnD = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnD.frame = CGRectMake(225, 5, 50, 34);
        [btnD setTitle:@"Done" forState:UIControlStateNormal];
        [btnD addTarget:self action:@selector(clickDoneAddCategory:) forControlEvents:UIControlEventTouchUpInside];
        [viewHeader addSubview:txt];
        [viewHeader addSubview:btnD];
        viewHeader.backgroundColor = [UIColor grayColor];
        return viewHeader;
    }
    return nil;
}
/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrCat removeObjectAtIndex:indexPath.row];
        [tblCat reloadData];
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView*)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
    int row = indexPath.row;
    if (row == 0 || row == 1 || row == 2 || row == 3 || row == 4 || row == 5) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row>5) {
        [btnCat setTitle:[arrCat objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        btnCat.tag = 7;
        tblCat.hidden =YES;
        btnAddCat.tag = 9;
        [tblCat reloadData];
    }else {

    }
}
#pragma mark -
#pragma mark UITextField delegate methods
- (void)animateTextField:(UITextField*) textField up: (BOOL) up {
	int movementDistance = 251; 
	if(movementDistance < 0)
		movementDistance = 0;
	float movementDuration = 0.3f; 
	int movement = (up ? -movementDistance : movementDistance);	
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	[UIView commitAnimations];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    [self animateTextField: textField up: YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField: txt1 up: YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextField: txt1 up: NO];
}
#pragma mark -
#pragma mark AVAudioRecorder & AVAudioPlayer delegate methods
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	recordButton.enabled = YES;
    [recordButton setBackgroundColor:[UIColor whiteColor]];
    [stopButton setBackgroundColor:[UIColor whiteColor]];
    [playButton setBackgroundColor:[UIColor whiteColor]];
	stopButton.enabled = NO;
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
	NSLog(@"Decode Error occurred");
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
}
-(void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
	NSLog(@"Encode Error occurred");
}
#pragma
#pragma mark - OptionVC delegate methods
- (void)rowOptionSelected:(NSString *)selectedStr {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if ([selectedStr isEqualToString:@""]) {
        return;
    }
    if (btnTaG == 0) {
        
    }else if (btnTaG == 1) {
        UIButton *btn = (UIButton*)[self.view viewWithTag:1];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
        if ([selectedStr isEqualToString:@"Level 1: 3 pictures"]) {
            lvl = 1;
            lblInstr.hidden=NO;
            [self loadCatArr];
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view1.frame = CGRectMake(376, 444, 100, 162);
            view2.frame = CGRectMake(535, 444, 100, 162);
            view3.frame = CGRectMake(693, 444, 100, 162);
            view4.hidden=YES;
            view5.hidden=YES;
        }else if ([selectedStr isEqualToString:@"Level 2: 4 pictures"]){
            lvl = 2;
            lblInstr.hidden=NO;
            [self loadCatArr];
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view4.hidden=NO;
            view1.frame = CGRectMake(294, 444, 100, 162);
            view2.frame = CGRectMake(453, 444, 100, 162);
            view3.frame = CGRectMake(611, 444, 100, 162);
            view4.frame = CGRectMake(774, 444, 100, 162);
            view5.hidden=YES;
        }else if ([selectedStr isEqualToString:@"Level 3: 5 pictures"]){
            lvl = 3;
            lblInstr.hidden=NO;
            [self loadCatArr];
            view1.hidden=NO;
            view2.hidden=NO;
            view3.hidden=NO;
            view4.hidden=NO;
            view5.hidden=NO;
            view1.frame = CGRectMake(144, 444, 100, 162);
            view2.frame = CGRectMake(303, 444, 100, 162);
            view3.frame = CGRectMake(461, 444, 100, 162);
            view4.frame = CGRectMake(624, 444, 100, 162);
            view5.frame = CGRectMake(781, 444, 100, 162);
        }else if ([selectedStr isEqualToString:@"Level 4: no pictures"]){
            lvl = 4;
            lblInstr.hidden=YES;
            [self loadCatArr];
            view1.hidden=YES;
            view2.hidden=YES;
            view3.hidden=YES;
            view4.hidden=YES;
            view5.hidden=YES;
        }
    }else {//Select Reinforcement Image and Audio
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTaG];
        [btn setTitle:selectedStr forState:UIControlStateNormal];    
    }
}
#pragma
#pragma mark - custom methods
-(void)choosePreProgrmmedPhoto:(id)sender
{
    [self.popOverOVC dismissPopoverAnimated:YES];
    UIButton *btn =sender;
    if ([arrPhotos containsObject:btn.titleLabel.text]) {
        int ind = [arrPhotos indexOfObject:btn.titleLabel.text];
        NSLog(@"ind:%d",ind);
        UIButton *btn1 = (UIButton*)[self.view viewWithTag:btnTag];
        switch (btnTag) {
            case 2:{
                tagModify1=2;
                btn1.frame = CGRectMake(0, 0, 100, 100);
                [btn1 setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];
            }
                break;
            case 3:{
                tagModify2=2;
                btn1.frame = CGRectMake(0, 0, 100, 100);
                [btn1 setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];      
            }
                break;
            case 4:{
                tagModify3=2;
                btn1.frame = CGRectMake(0, 0, 100, 100);
                [btn1 setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal]; 
            }
                break;
            case 5:{
                tagModify4=2;
                btn1.frame = CGRectMake(0, 0, 100, 100);
                [btn1 setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];  
            }
                break;
            case 6:{
                tagModify5=2;
                btn1.frame = CGRectMake(0, 0, 100, 100);
                [btn1 setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];  
            }
                break;
                
            default:
                break;
        }
    }
}
-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(UIImage *)rotateImage:(UIImage *)image {
    
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
} 
-(void)clickSetOptions:(id)sender
{
    UIButton *btn = sender;
    btnTaG = btn.tag;
//    NSLog(@"Tag:%d",btn.tag);
    switch (btn.tag) {
        case 1:
        {
            NSString *strS=@"5";
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Level 1: 3 pictures",@"Level 2: 4 pictures",@"Level 3: 5 pictures",@"Level 4: no pictures", nil]:strS];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(442, -130, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        case 12:
        {
            NSString *strS=@"5";
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"who",@"what",@"where",@"when", nil]:strS];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(442, -19, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        case 13:
        {
            NSString *strS=@"5";
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"super",@"awesome",@"fantastic",@"good answer",@"great job",@"nice work",@"right answer",@"way to go",@"you are right",@"you’re correct", nil]:strS];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(442, 36, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        case 14:
        {
            NSString *strS=@"5";
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"choose another one",@"keep trying",@"not quiet",@"opps not it",@"pick another one",@"try again",@"try another one",@"uh oh not correct", nil]:strS];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(442, 93, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
            
        default:
            break;
    }
}
-(void)clickBack:(id)sender
{
    if ([strTrackName isEqualToString:@""]) {
        
    }else {
        NSArray *dirPaths;
        NSString *docsDir;
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager removeItemAtPath:[docsDir stringByAppendingPathComponent:strTrackName] error:NULL]; 
        strTrackName = @"";
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)clickHome:(id)sender
{
    if ([strTrackName isEqualToString:@""]) {
        
    }else {
        NSArray *dirPaths;
        NSString *docsDir;
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager removeItemAtPath:[docsDir stringByAppendingPathComponent:strTrackName] error:NULL]; 
        strTrackName = @"";
    }    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickSave:(id)sender
{
    if (audioRecorder.recording) {
        [self stopPlayOrRecording];
    }
    NSMutableArray *arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"tblCustom" withCondition:[NSString stringWithFormat:@"StudID='%@'",strStudID] withColumnValue:@""]];
    if ([arrTmp count]==4) {
        if ([strTrackName isEqualToString:@""]) {
            
        }else {
            NSArray *dirPaths;
            NSString *docsDir;
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            [fileManager removeItemAtPath:[docsDir stringByAppendingPathComponent:strTrackName] error:NULL]; 
            strTrackName = @"";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy the full version of Questions2Learn to add more customized questions." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return; 
    }
    btnCat.tag = 7;//
    tblCat.hidden =YES;//
    btnAddCat.tag = 9;//
    [tblCat reloadData];//
    NSMutableArray *arrMissed = [NSMutableArray array];
    UIButton *btnLevel = (UIButton*)[self.view viewWithTag:1];
    if ([btnLevel.titleLabel.text isEqualToString:@"Select Level"]) {
        [arrMissed addObject:@"No select level"];
    }else {
        if ([btnLevel.titleLabel.text isEqualToString:@"Level 1: 3 pictures"]) {
            if (tagModify1 ==0) {
                [arrMissed addObject:@"\nNo visual answer 1"];
            }else {
                
            }
            if (tagModify2 ==0) {
                [arrMissed addObject:@"\nNo visual answer 2"];
            }else {
                            } 
            if (tagModify3 ==0) {
                [arrMissed addObject:@"\nNo visual answer 3"];
            }else {
                
            }            
        }else if ([btnLevel.titleLabel.text isEqualToString:@"Level 2: 4 pictures"]){
            if (tagModify1 ==0) {
                [arrMissed addObject:@"\nNo visual answer 1"];
            }else {
                            }
            if (tagModify2 ==0) {
                [arrMissed addObject:@"\nNo visual answer 2"];
            }else {
                
            }  
            if (tagModify3 ==0) {
                [arrMissed addObject:@"\nNo visual answer 3"];
            }else {
                            }
            if (tagModify4 ==0) {
                [arrMissed addObject:@"\nNo visual answer 4"];
            }else {
                
            }            
        }else if ([btnLevel.titleLabel.text isEqualToString:@"Level 3: 5 pictures"]){
            if (tagModify1 ==0) {
                [arrMissed addObject:@"\nNo visual answer 1"];
            }else {
                
            }
            if (tagModify2 ==0) {
                [arrMissed addObject:@"\nNo visual answer 2"];
            }else {
                
            }  
            if (tagModify3 ==0) {
                [arrMissed addObject:@"\nNo visual answer 3"];
            }else {
                
            }
            if (tagModify4 ==0) {
                [arrMissed addObject:@"\nNo visual answer 4"];
            }else {
                
            }
            if (tagModify5 ==0) {
                [arrMissed addObject:@"\nNo visual answer 5"];
            }else {
                
            }            
        }else if ([btnLevel.titleLabel.text isEqualToString:@"Level 4: no pictures"]){
            
        }
    }
    if ([btnCat.titleLabel.text isEqualToString:@"View Category"]) {
        [arrMissed addObject:@"\nNo category added for level"];
    }else {
        NSLog(@"btnCat:%@",btnCat.titleLabel.text);
    }
    UIButton *btn = (UIButton*)[self.view viewWithTag:12];
    if ([btn.titleLabel.text isEqualToString:@"Select Question Type"]) {
        [arrMissed addObject:@"\nNo question type selected"];
    }else {
    }
    UIButton *btn13 = (UIButton*)[self.view viewWithTag:13];
    if ([btn13.titleLabel.text isEqualToString:@"Select Reinforcement Image and Audio"]) {
        [arrMissed addObject:@"\nNo reinforcement image and audio are selected"];
    }else {
    }
    UIButton *btn14 = (UIButton*)[self.view viewWithTag:14];
    if ([btn14.titleLabel.text isEqualToString:@"Select Feedback Audio"]) {
        [arrMissed addObject:@"\nNo feedback audio selected"];
    }else {
    }    
    if ([txtQue.text isEqualToString:@""]) {
        [arrMissed addObject:@"\nNo question text"];
    }else {
    }
    if ([strTrackName isEqualToString:@""]) {
        [arrMissed addObject:@"\nNo audio record for question"];
    }else {

    }
    if (lvl == 4) {
        
    }else {
        if (btnTG == 0) {
            [arrMissed addObject:@"\nNo correct answer choose"];        
        }else {
            if (lvl == 1 && btnTG<=3) {
                
            }else if (lvl == 2 && btnTG<=4) {
                
            }else if (lvl == 3 && btnTG<=5) {
                
            }else {
                [arrMissed addObject:@"\nNo correct answer choose"];
            }
        }
    }

    if ([arrMissed count]==0) {        
        NSString *strQT = [NSString stringWithFormat:@"%@",btn.titleLabel.text];
        NSString *strO1 = @"";
        NSString *strO2 = @"";
        NSString *strO3 = @"";
        NSString *strO4 = @"";
        NSString *strO5 = @"";
        NSString *strM1 = @"";
        NSString *strM2 = @"";
        NSString *strM3 = @"";
        NSString *strM4 = @"";
        NSString *strM5 = @"";
        NSString *strCorA=@"";
        switch (lvl) {
            case 1:
            {
                UIButton *btn = (UIButton*)[self.view viewWithTag:2];
                if (tagModify1 == 1) {
                    strO1 = [self clickSaveImage:UIImagePNGRepresentation([btn imageForState:UIControlStateNormal]):1];
                    strM1=@"1";
                }else if (tagModify1 == 2) {
                    strO1 = btn.titleLabel.text;
                    strM1=@"0";
                }
                UIButton *btn1 = (UIButton*)[self.view viewWithTag:3];
                if (tagModify2 == 1) {
                    strO2 = [self clickSaveImage:UIImagePNGRepresentation([btn1 imageForState:UIControlStateNormal]):2];
                    strM2=@"1";
                }else if (tagModify2 == 2) {
                    strO2 = btn1.titleLabel.text;
                    strM2=@"0";
                }
                UIButton *btn2 = (UIButton*)[self.view viewWithTag:4]; 
                if (tagModify3 == 1) {
                    strO3 = [self clickSaveImage:UIImagePNGRepresentation([btn2 imageForState:UIControlStateNormal]):3];
                    strM3=@"1";
                }else if (tagModify3 == 2) {
                    strO3 = btn2.titleLabel.text;
                    strM3=@"0";
                }
                switch (btnTG) {
                    case 1:
                        strCorA = @"E & J";
                        break;
                    case 2:
                        strCorA = @"F & K";
                        break;
                    case 3:
                        strCorA = @"G & L";
                        break;
                    default:
                        break;
                }
            }
                break;
            case 2:
            {
                UIButton *btn = (UIButton*)[self.view viewWithTag:2];
                if (tagModify1 == 1) {
                    strO1 = [self clickSaveImage:UIImagePNGRepresentation([btn imageForState:UIControlStateNormal]):1];
                    strM1=@"1";
                }else if (tagModify1 == 2) {
                    strO1 = btn.titleLabel.text;
                    strM1=@"0";
                }
                
                UIButton *btn1 = (UIButton*)[self.view viewWithTag:3];
                if (tagModify2 == 1) {
                    strO2 = [self clickSaveImage:UIImagePNGRepresentation([btn1 imageForState:UIControlStateNormal]):2];
                    strM2=@"1";
                }else if (tagModify2 == 2) {
                    strO2 = btn1.titleLabel.text;
                    strM2=@"0";
                }
                
                UIButton *btn2 = (UIButton*)[self.view viewWithTag:4]; 
                if (tagModify3 == 1) {
                    strO3 = [self clickSaveImage:UIImagePNGRepresentation([btn2 imageForState:UIControlStateNormal]):3];
                    strM3=@"1";
                }else if (tagModify3 == 2) {
                    strO3 = btn2.titleLabel.text;
                    strM3=@"0";
                }
                UIButton *btn3 = (UIButton*)[self.view viewWithTag:5]; 
                if (tagModify4 == 1) {
                    strO4 = [self clickSaveImage:UIImagePNGRepresentation([btn3 imageForState:UIControlStateNormal]):4];
                    strM4=@"1";
                }else if (tagModify4 == 2) {
                    strO4 = btn3.titleLabel.text;
                    strM4=@"0";
                }
                switch (btnTG) {
                    case 1:
                        strCorA = @"E & K";
                        break;
                    case 2:
                        strCorA = @"F & L";
                        break;
                    case 3:
                        strCorA = @"G & M";
                        break;
                    case 4:
                        strCorA = @"H & N";
                        break;
                    default:
                        break;
                }
            }
                break;
            case 3:
            {
                UIButton *btn = (UIButton*)[self.view viewWithTag:2];
                if (tagModify1 == 1) {
                    strO1 = [self clickSaveImage:UIImagePNGRepresentation([btn imageForState:UIControlStateNormal]):1];
                    strM1=@"1";
                }else if (tagModify1 == 2) {
                    strO1 = btn.titleLabel.text;
                    strM1=@"0";
                }
                UIButton *btn1 = (UIButton*)[self.view viewWithTag:3];
                if (tagModify2 == 1) {
                    strO2 = [self clickSaveImage:UIImagePNGRepresentation([btn1 imageForState:UIControlStateNormal]):2];
                    strM2=@"1";
                }else if (tagModify2 == 2) {
                    strO2 = btn1.titleLabel.text;
                    strM2=@"0";
                }
                UIButton *btn2 = (UIButton*)[self.view viewWithTag:4]; 
                if (tagModify3 == 1) {
                    strO3 = [self clickSaveImage:UIImagePNGRepresentation([btn2 imageForState:UIControlStateNormal]):3];
                    strM3=@"1";
                }else if (tagModify3 == 2) {
                    strO3 = btn2.titleLabel.text;
                    strM3=@"0";
                }
                UIButton *btn3 = (UIButton*)[self.view viewWithTag:5]; 
                if (tagModify4 == 1) {
                    strO4 = [self clickSaveImage:UIImagePNGRepresentation([btn3 imageForState:UIControlStateNormal]):4];
                    strM4=@"1";
                }else if (tagModify4 == 2) {
                    strO4 = btn3.titleLabel.text;
                    strM4=@"0";
                }
                UIButton *btn4 = (UIButton*)[self.view viewWithTag:6]; 
                if (tagModify5 == 1) {
                    strO5 = [self clickSaveImage:UIImagePNGRepresentation([btn4 imageForState:UIControlStateNormal]):5];
                    strM5=@"1";
                }else if (tagModify5 == 2) {
                    strO5 = btn4.titleLabel.text;
                    strM5=@"0";
                }
                switch (btnTG) {
                    case 1:
                        strCorA = @"E & L";
                        break;
                    case 2:
                        strCorA = @"F & M";
                        break;
                    case 3:
                        strCorA = @"G & N";
                        break;
                    case 4:
                        strCorA = @"H & O";
                        break;
                    case 5:
                        strCorA = @"I & P";
                        break;
                    default:
                        break;
                }
            }
                break;
            case 4:
            {
                
            }
                break;
                
            default:
                break;
        }
        NSString *strReinImg =@"";
        NSString *strReinM4a =@"";
        if ([btn13.titleLabel.text isEqualToString:@"super"]){
            strReinImg =@"super.png";
            strReinM4a =@"super.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"awesome"]) {
            strReinImg =@"awesome.png";
            strReinM4a =@"Awesome.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"fantastic"]) {
            strReinImg =@"fantastic.png";
            strReinM4a =@"Fantastic.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"good answer"]) {
            strReinImg =@"goodanswer.png";
            strReinM4a =@"goodanswer.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"great job"]) {
            strReinImg =@"greatjob.png";
            strReinM4a =@"greatjob.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"nice work"]) {
            strReinImg =@"nicework.png";
            strReinM4a =@"nicework.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"right answer"]) {
            strReinImg =@"rightanswer.png";
            strReinM4a =@"rightanswer.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"way to go"]) {
            strReinImg =@"Waytogo.png";
            strReinM4a =@"waytogo.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"you are right"]) {
            strReinImg =@"youareright.png";
            strReinM4a =@"youareright.m4a";
        }else if ([btn13.titleLabel.text isEqualToString:@"you’re correct"]) {
            strReinImg =@"yourecorrect.png";
            strReinM4a =@"yourecorrect.m4a";
        }
        NSString *strAF =@"";
        if ([btn14.titleLabel.text isEqualToString:@"choose another one"]){
            strAF =@"chooseanotherone.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"keep trying"]) {
            strAF =@"keeptrying.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"not quiet"]) {
            strAF =@"notquiet.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"opps not it"]) {
            strAF =@"oppsnotit.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"pick another one"]) {
            strAF =@"pickanotherone.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"try again"]) {
            strAF =@"tryagain.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"try another one"]) {
            strAF =@"tryanotherone.m4a";
        }else if ([btn14.titleLabel.text isEqualToString:@"uh oh not correct"]) {
            strAF =@"uhohnotcorrect.m4a";
        }
        NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblCustom where StudID=%@ and Level='Level %d' and QCat='%@' and QType='%@'",strStudID,lvl,btnCat.titleLabel.text,strQT]]];
        int cnt = [[[diC valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] intValue];
        switch (lvl) {
            case 1:
            {
                dicS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strO1,@"Ans1Img",strO2,@"Ans2Img",strO3,@"Ans3Img",strReinM4a,@"AudioRein",strCorA,@"CorrectAns",strAF,@"QAFeedback",strTrackName,@"QAudio",btnCat.titleLabel.text,@"QCat",txtQue.text,@"QText",strQT,@"QType",strReinImg,@"VisualRein",[NSString stringWithFormat:@"%@,%@,%@",strM1,strM2,strM3],@"Modify",@"Level 1",@"Level",[NSString stringWithFormat:@"%d",cnt+261],@"id",strStudID,@"StudID", nil];
                [objDAL insertRecord:dicS inTable:@"tblCustom"];            
            }
                break;
            case 2:
            {
                dicS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strO1,@"Ans1Img",strO2,@"Ans2Img",strO3,@"Ans3Img",strO4,@"Ans4Img",strReinM4a,@"AudioRein",strCorA,@"CorrectAns",strAF,@"QAFeedback",strTrackName,@"QAudio",btnCat.titleLabel.text,@"QCat",txtQue.text,@"QText",strQT,@"QType",strReinImg,@"VisualRein",[NSString stringWithFormat:@"%@,%@,%@,%@",strM1,strM2,strM3,strM4],@"Modify",@"Level 2",@"Level",[NSString stringWithFormat:@"%d",cnt+261],@"id",strStudID,@"StudID", nil];
                [objDAL insertRecord:dicS inTable:@"tblCustom"];                
            }
                break;
            case 3:
            {
                dicS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strO1,@"Ans1Img",strO2,@"Ans2Img",strO3,@"Ans3Img",strO4,@"Ans4Img",strO5,@"Ans5Img",strReinM4a,@"AudioRein",strCorA,@"CorrectAns",strAF,@"QAFeedback",strTrackName,@"QAudio",btnCat.titleLabel.text,@"QCat",txtQue.text,@"QText",strQT,@"QType",strReinImg,@"VisualRein",[NSString stringWithFormat:@"%@,%@,%@,%@,%@",strM1,strM2,strM3,strM4,strM5],@"Modify",@"Level 3",@"Level",[NSString stringWithFormat:@"%d",cnt+261],@"id",strStudID,@"StudID", nil];
                [objDAL insertRecord:dicS inTable:@"tblCustom"];                
            }
                break;
            case 4:
            {
                dicS = [NSMutableDictionary dictionaryWithObjectsAndKeys:strReinM4a,@"AudioRein",strAF,@"QAFeedback",strTrackName,@"QAudio",btnCat.titleLabel.text,@"QCat",txtQue.text,@"QText",strQT,@"QType",strReinImg,@"VisualRein",@"",@"Modify",@"Level 4",@"Level",[NSString stringWithFormat:@"%d",cnt+261],@"id",strStudID,@"StudID", nil];
                [objDAL insertRecord:dicS inTable:@"tblCustom"];                
            }
                break;
                
            default:
                break;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Category saved successfully" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];

//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Category saved successfully" message:@"Save in another student’s settings?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
//        alert.tag =12;
//        [alert show];
        strTrackName=@"";
//        [self loadNewBlankView];
    }else {
        if ([strTrackName isEqualToString:@""]) {
        }else {
            NSArray *dirPaths;
            NSString *docsDir;
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            [fileManager removeItemAtPath:[docsDir stringByAppendingPathComponent:strTrackName] error:NULL]; 
            strTrackName = @"";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Required field are missing" message:[NSString stringWithFormat:@"%@",[arrMissed componentsJoinedByString:@","]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)clickNextQue:(id)sender
{
    [self loadNewBlankView];
}
-(void)clickSetAns:(id)sender event:(id)event
{
    [txtQue resignFirstResponder];
    UIButton *btn = sender;
    btnTag = btn.tag;
//    NSSet *touches = [event allTouches];
//    UITouch *touch = [touches anyObject];
//    CGPoint currentTouchPosition = [touch locationInView:self.view];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Take photo" message:@"From" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Photos library",@"Preprogrammed pictures", nil];
    alert.tag = 11;
    [alert show];
}
-(void)clickAddCategory:(id)sender
{
    if (lvl == 0) {
        return;
    }
    if ([arrCat count] == 7) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy the full version of Questions2Learn to add more customized questions." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;         
    }
    if (btnAddCat.tag == 9) {
        btnAddCat.tag = 10;
        btnCat.tag = 8;
        tblCat.hidden = NO;
        [tblCat reloadData];
    }else if (btnAddCat.tag == 10) {
    
    }

//        [tblCat setEditing:YES animated:YES];

}
-(void)clickDoneAddCategory:(id)sender
{
    if ([[txt text] length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please add category" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [txt resignFirstResponder];
    [arrCat addObject:txt.text];
    btnAddCat.tag = 9;
    [tblCat reloadData];
    NSIndexPath *topIndexPath;
    int c = [arrCat count]-1;
    topIndexPath = [NSIndexPath indexPathForRow:c inSection:0];
    [tblCat scrollToRowAtIndexPath:topIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
-(void)clickViewCategory:(id)sender
{
    if (lvl == 0) {
        return;
    }
    if (btnCat.tag == 7) {
        btnCat.tag = 8;
        tblCat.hidden =NO;
    }else if (btnCat.tag == 8) {
        btnCat.tag = 7;
        tblCat.hidden =YES;
        btnAddCat.tag = 9;
        [tblCat reloadData];
    }
}
-(void)clickSetCorrectAns:(id)sender
{
    if (sender == btn11) {
        btnTG = 1;
        [btn11 setBackgroundImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btn22 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn33 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn44 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn55 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btn22){
        btnTG = 2;
        [btn11 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn22 setBackgroundImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btn33 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn44 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn55 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btn33){
        btnTG = 3;
        [btn11 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn22 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn33 setBackgroundImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btn44 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn55 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btn44){
        btnTG = 4;
        [btn11 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn22 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn33 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn44 setBackgroundImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
        [btn55 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    }else if (sender == btn55){
        btnTG = 5;
        [btn11 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn22 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn33 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn44 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
        [btn55 setBackgroundImage:[UIImage imageNamed:@"checkTrue.png"] forState:UIControlStateNormal];
    }
}
-(NSString*)clickSaveImage:(NSData*)dataBtnImg:(int)btn
{
    NSString *strImgN = [NSString stringWithFormat:@"%@%d.png",[NSDate date],btn];
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *imgPath = [docsDir stringByAppendingPathComponent:strImgN];
    [[NSFileManager defaultManager] createFileAtPath:imgPath contents:dataBtnImg attributes:nil];
    return strImgN;
}
-(void)loadCatArr
{
     [arrCat removeAllObjects];
    [arrCat addObjectsFromArray:[NSArray arrayWithObjects:@"Community",@"Food & Drinks",@"Health",@"Home",@"Leisure/Recreational",@"School", nil]];
    NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT DISTINCT QCat FROM tblCustom where StudID='%@';",strStudID]]];
    for (int i=0; i<[diC count]; i++) {
        [arrCat addObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]];
    }
    [tblCat reloadData];
}
-(void)loadNewBlankView
{
    UIButton *btn00 = (UIButton*)[self.view viewWithTag:1];
    [btn00 setTitle:@"Select Level" forState:UIControlStateNormal];
    [btnCat setTitle:@"View Category" forState:UIControlStateNormal];
    UIButton *btn01 = (UIButton*)[self.view viewWithTag:12];
    [btn01 setTitle:@"Select Question Type" forState:UIControlStateNormal];
    UIButton *btn0 = (UIButton*)[self.view viewWithTag:2];
    [btn0 setImage:nil forState:UIControlStateNormal];
    txt1.text = @"";
    UIButton *btn1 = (UIButton*)[self.view viewWithTag:3];
    [btn1 setImage:nil forState:UIControlStateNormal];
    txt2.text = @"";
    UIButton *btn2 = (UIButton*)[self.view viewWithTag:4];
    [btn2 setImage:nil forState:UIControlStateNormal];
    txt3.text = @"";
    UIButton *btn3 = (UIButton*)[self.view viewWithTag:5];
    [btn3 setImage:nil forState:UIControlStateNormal];
    txt4.text = @"";
    UIButton *btn4 = (UIButton*)[self.view viewWithTag:6];
    [btn4 setImage:nil forState:UIControlStateNormal];
    txt5.text = @"";
    txtQue.text = @"";
    [btn11 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    [btn22 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    [btn33 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    [btn44 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    [btn55 setBackgroundImage:[UIImage imageNamed:@"checkFalse.png"] forState:UIControlStateNormal];
    btnTG = 0;
    view1.hidden=YES;
    view2.hidden=YES;
    view3.hidden=YES;
    view4.hidden=YES;
    view5.hidden=YES;
}
#pragma
#pragma mark - UIAlertview delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 12) {
        if (buttonIndex==0) {
            self.viewSVC = [[SelectSVC alloc] initWithNibName:nil bundle:nil];
            _viewSVC.delegate=self;
            _viewSVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            _viewSVC.modalPresentationStyle = UIModalPresentationFormSheet;
            [_viewSVC getStudID:strStudID];
            [self presentModalViewController:_viewSVC animated:YES];            
        }else {
            //strTrackName=@"";
        }
    }
    if (alertView.tag == 11) {
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        if (buttonIndex == 1) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                pickerPhoto = [[UIImagePickerController alloc] init];
                pickerPhoto.delegate = self;
                pickerPhoto.sourceType = UIImagePickerControllerSourceTypeCamera;
                self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:pickerPhoto];
                [self.popOverOVC presentPopoverFromRect:CGRectMake([btn superview].frame.origin.x-110, 444, 320, 250) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
       
            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"your deveice not compatible to take photo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }else if (buttonIndex==2){
            pickerPhoto = [[NonRotatingUIImagePickerController alloc] init];
            pickerPhoto.delegate = self;
            if ([UIImagePickerController isSourceTypeAvailable:
                UIImagePickerControllerSourceTypePhotoLibrary]) 
            {
                pickerPhoto.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
                
            }
            self.popOverOVC  = [[UIPopoverController alloc] initWithContentViewController:pickerPhoto];
            [self.popOverOVC  presentPopoverFromRect:CGRectMake([btn superview].frame.origin.x-110, 444, 320, 250) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }else if (buttonIndex==3){
            UIViewController *v= [[UIViewController alloc] init];
            v.view.frame = CGRectMake(0, 0, 320, 310);
            UIScrollView *scr = [[UIScrollView alloc] initWithFrame:v.view.frame];
            scr.delegate=self;
            scr.scrollEnabled=YES;
            scr.showsVerticalScrollIndicator=YES;
            scr.showsHorizontalScrollIndicator=NO;
            scr.alwaysBounceVertical=YES;
            scr.alwaysBounceHorizontal=NO;
            double getCountNo = (double)[arrPhotos count]/4;
            NSString *strIV = [NSString stringWithFormat:@"%f",getCountNo];
            NSArray *arrSeperate = [strIV componentsSeparatedByString:@"."];
            NSString *strDiv = [arrSeperate objectAtIndex:0];  
            NSString *strMode = [arrSeperate objectAtIndex:1];
            NSInteger imgVC1=0;
            NSInteger imgVC2=0;
            NSInteger imgVC3=0;
            NSInteger imgVC4=0;
            if ([strMode isEqualToString:@"25"]) {
                imgVC1 = [strDiv integerValue]+1;
                imgVC2 = [strDiv integerValue];
                imgVC3 = [strDiv integerValue];
                imgVC4 = [strDiv integerValue];
            }else if ([strMode isEqualToString:@"50"]) {
                imgVC1 = [strDiv integerValue]+1;
                imgVC2 = [strDiv integerValue]+1;
                imgVC3 = [strDiv integerValue];
                imgVC4 = [strDiv integerValue];
            }else if ([strMode isEqualToString:@"75"]) {
                imgVC1 = [strDiv integerValue]+1;
                imgVC2 = [strDiv integerValue]+1;
                imgVC3 = [strDiv integerValue]+1;
                imgVC4 = [strDiv integerValue];
            }else {
                imgVC1 = [strDiv integerValue];
                imgVC2 = [strDiv integerValue];
                imgVC3 = [strDiv integerValue];
                imgVC4 = [strDiv integerValue];
            }
            float Yis=0;
            int ind =0;
            for (int i = 0; i<imgVC1; i++) {
                Yis = Yis+4;
                UIButton *photo = [UIButton buttonWithType:UIButtonTypeCustom];
                photo.frame = CGRectMake(4, Yis, 75, 73);
                [photo setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [photo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];
                [photo addTarget:self action:@selector(choosePreProgrmmedPhoto:) forControlEvents:UIControlEventTouchUpInside];
                Yis=Yis+73;
                [scr addSubview:photo];
                ind++;
            }
            Yis=0;
            for (int i = 0; i<imgVC2; i++) {
                Yis = Yis+4;
                UIButton *photo = [UIButton buttonWithType:UIButtonTypeCustom];
                photo.frame = CGRectMake(83, Yis, 75, 73);
                [photo setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [photo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];
                [photo addTarget:self action:@selector(choosePreProgrmmedPhoto:) forControlEvents:UIControlEventTouchUpInside];
                Yis=Yis+73;
                [scr addSubview:photo];
                ind++;
            }
            Yis=0;
            for (int i = 0; i<imgVC3; i++) {
                Yis = Yis+4;
                UIButton *photo = [UIButton buttonWithType:UIButtonTypeCustom];
                photo.frame = CGRectMake(162, Yis, 75, 73);
                [photo setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [photo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];		
                [photo addTarget:self action:@selector(choosePreProgrmmedPhoto:) forControlEvents:UIControlEventTouchUpInside];
                Yis=Yis+73;
                [scr addSubview:photo];
                ind++;
            }
            Yis=0;
            for (int i = 0; i<imgVC4; i++) {
                Yis = Yis+4;
                UIButton *photo = [UIButton buttonWithType:UIButtonTypeCustom];
                photo.frame = CGRectMake(241, Yis, 75, 73);
                [photo setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [photo setTitle:[arrPhotos objectAtIndex:ind] forState:UIControlStateNormal];
                [photo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrPhotos objectAtIndex:ind]]] forState:UIControlStateNormal];
                [photo addTarget:self action:@selector(choosePreProgrmmedPhoto:) forControlEvents:UIControlEventTouchUpInside];
                Yis=Yis+73;
                [scr addSubview:photo];
                ind++;
            }
            [scr setContentSize:CGSizeMake(200 ,(imgVC1*73)+((imgVC1-1)*4)+8)];
            [v.view addSubview:scr];
            self.popOverOVC  = [[UIPopoverController alloc] initWithContentViewController:v];
            self.popOverOVC.popoverContentSize = CGSizeMake(320, 310);
            [self.popOverOVC presentPopoverFromRect:CGRectMake([btn superview].frame.origin.x-110, 444, 320, 310) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];                    
        }
    }
}
#pragma mark -
#pragma mark OptionVC delegate methods
-(void)selectStudsDone:(NSString *)idStr
{
    NSLog(@"idStr:%@",idStr);
    if ([idStr isEqualToString:@""]) {
        
    }else {
        NSArray *arrS = [idStr componentsSeparatedByString:@","];
        UIButton *btn = (UIButton*)[self.view viewWithTag:12];
        NSString *strQT = [NSString stringWithFormat:@"%@",btn.titleLabel.text];
        for (int i=0; i<[arrS count]; i++) {
            [dicS setObject:[arrS objectAtIndex:i] forKey:@"StudID"];
            NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(id) FROM tblCustom where StudID='%@' and Level='Level %d' and QCat='%@' and QType='%@'",[arrS objectAtIndex:i],lvl,btnCat.titleLabel.text,strQT]]];
            int cnt = [[[diC valueForKey:@"Table1"] valueForKey:@"COUNT(id)"] intValue];
            [dicS setObject:[NSString stringWithFormat:@"%d",cnt+261] forKey:@"id"];
            [objDAL insertRecord:dicS inTable:@"tblCustom"];
        }        
    }
    //strTrackName=@"";
}
#pragma
#pragma mark - Audio Recorder custom methods
-(void) recordAudio
{
	if (!audioRecorder.recording)
	{
        if (tmrRec !=nil) {
            [tmrRec invalidate];
            tmrRec=nil;
        }
		[self audioInitiation];
		playButton.enabled = NO;
        [playButton setBackgroundColor:[UIColor whiteColor]];
		stopButton.enabled = YES;
		[audioRecorder record];
	}
}
-(void)stopPlayOrRecording
{
    if (tmrRec !=nil) {
        [tmrRec invalidate];
        tmrRec=nil;
    }
    stopButton.enabled = NO;
    [stopButton setBackgroundColor:[UIColor whiteColor]];
    [playButton setBackgroundColor:[UIColor whiteColor]];
    playButton.enabled = YES;
    recordButton.enabled = YES;
    [recordButton setBackgroundColor:[UIColor whiteColor]];
    if (audioRecorder.recording)
    {
		[audioRecorder stop];
    } else if (audioPlayer.playing) {
		[audioPlayer stop];
    }
}
-(void) playAudio
{
    if (!audioRecorder.recording)
    {
		stopButton.enabled = YES;
		recordButton.enabled = NO;
		
        NSError *error;
		
        audioPlayer = [[AVAudioPlayer alloc] 
					   initWithContentsOfURL:audioRecorder.url                                    
					   error:&error];
		
        audioPlayer.delegate = self;
		
        if (error)
			NSLog(@"Error: %@", 
				  [error localizedDescription]);
        else{
            [playButton setBackgroundColor:[UIColor blueColor]];
            [stopButton setBackgroundColor:[UIColor redColor]];
			[audioPlayer play];
        }
	}
}
-(void)audioInitiation
{
	NSArray *dirPaths;
	NSString *docsDir;
	dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = [dirPaths objectAtIndex:0];
    if ([strTrackName isEqualToString:@""]) {
        
    }else {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		[fileManager removeItemAtPath:[docsDir stringByAppendingPathComponent:strTrackName] error:NULL]; 
        strTrackName = @"";
    }    
	NSDate *date = [NSDate date];
	strTrackName = [NSString stringWithFormat:@"%@.caf",date];
	NSString *soundFilePath = [docsDir stringByAppendingPathComponent:strTrackName];
	NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
	
	NSDictionary *recordSettings = [NSDictionary 
									dictionaryWithObjectsAndKeys:
									[NSNumber numberWithInt:AVAudioQualityMin],
									AVEncoderAudioQualityKey,
									[NSNumber numberWithInt:16], 
									AVEncoderBitRateKey,
									[NSNumber numberWithInt: 2], 
									AVNumberOfChannelsKey,
									[NSNumber numberWithFloat:44100.0], 
									AVSampleRateKey,
									nil];
	
	NSError *error = nil;
	
	audioRecorder = [[AVAudioRecorder alloc]
					 initWithURL:soundFileURL
					 settings:recordSettings
					 error:&error];
	
	if (error)
	{
		NSLog(@"error: %@", [error localizedDescription]);
		
	} else {
        [recordButton setBackgroundColor:[UIColor greenColor]];
        [stopButton setBackgroundColor:[UIColor redColor]];
		[audioRecorder prepareToRecord];
        tmrRec = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timerForStop) userInfo:nil repeats:NO];
	}
}
-(void)timerForStop
{
    [self stopPlayOrRecording];
}
@end
