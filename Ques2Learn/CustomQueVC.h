//
//  CustomQueVC.h
//  Ques2Learn
//
//  Created by apple on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomQueVC : UIViewController {
    NSString *strStudID;
    IBOutlet UILabel *lblStudName;
    IBOutlet UIButton *btnSImg;
    DAL *objDAL;
}

-(void)getStudID:(NSString*)idStr;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickBack:(id)sender;
-(IBAction)clickCNC:(id)sender;
-(IBAction)clickMPAC:(id)sender;
@end
