//
//  CustomQueVC.m
//  Ques2Learn
//
//  Created by apple on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomQueVC.h"
#import "CreateNewCatVC.h"
#import "ModifyPreProgVC.h"
@implementation CustomQueVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%@'",strStudID]]];
    if ([dict count]>0) {
        lblStudName.text=[[dict valueForKey:@"Table1"] valueForKey:@"StudName"];
        NSString *strSN = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
        if ([strSN isEqualToString:@"OK.png"]) {
        }else {
            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docsDir = [dirPaths objectAtIndex:0];
            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
            UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
            if (imgGet == nil) {
                
            }else {
                [btnSImg setBackgroundImage:imgGet forState:UIControlStateNormal];
            }
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark-
#pragma mark custom methods
-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)clickCNC:(id)sender
{
    CreateNewCatVC *viewController = [[CreateNewCatVC alloc] initWithNibName:@"CreateNewCatVC" bundle:nil];
    [viewController getStudID:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(IBAction)clickMPAC:(id)sender
{
    ModifyPreProgVC *viewController = [[ModifyPreProgVC alloc] initWithNibName:nil bundle:nil];
    [viewController getStudID:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
