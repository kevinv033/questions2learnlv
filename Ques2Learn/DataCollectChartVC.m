//
//  DataCollectChartVC.m
//  Ques2Learn
//
//  Created by apple on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataCollectChartVC.h"
#import "DataCollectVC.h"
#import "CustomQueVC.h"
#import "GoVC.h"
@implementation DataCollectChartVC
@synthesize viewOptionsVC = _viewOptionsVC;
@synthesize viewOptionsVC1 = _viewOptionsVC1;
@synthesize popOverOVC = _popOverOVC;
@synthesize viewAddStud = _viewAddStud;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    strStuID =@"";
    tagR=nil;
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    btnTag = 0;
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
        btnEdit.hidden=YES;
        btnSImg.hidden=YES;
        lblStudName.hidden=YES;
        lblTAge.hidden=YES;
        lblStudAge.hidden=YES;
        lblTGrade.hidden=YES;
        lblStudGrade.hidden=YES;
        lblNOT.hidden =YES;
        UISwitch *swtNOT = (UISwitch*)[self.view viewWithTag:14];
        swtNOT.hidden=YES;
        imgSwtNT.hidden=YES;
        [self loadGroupSettings];
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:15];
        swt.on=YES;
    }else if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 1){
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:15];
        swt.on=NO;
    }
    arrGroup = [[NSMutableArray alloc] init];
    arrCollect = [[NSMutableArray alloc] init];
    arrStu_List = [[NSMutableArray alloc] init];
    [arrStu_List addObjectsFromArray:[self getSudentsRecords]];
    for (int i=0; i<[arrStu_List count]; i++) {
        if ([[[arrStu_List objectAtIndex:i] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
            [arrCollect addObject:@"YES"];
            [arrGroup addObject:[arrStu_List objectAtIndex:i]];
        }else{
            [arrCollect addObject:@"NO"];
        }
    }
    if ([arrStu_List count]>0) {
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"]==2) {
            
        }else {
            NSString *str=@"";
            for (int i = 0; i<[arrStu_List count]; i++) {
                if ([[[arrStu_List objectAtIndex:i] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
                    str = [NSString stringWithFormat:@"%d",i];
                }
            }
            if ([str isEqualToString:@""]) {
            }else{
                [tblStudents selectRowAtIndexPath:[NSIndexPath indexPathForRow:[str intValue] inSection:0] animated:NO scrollPosition: UITableViewScrollPositionNone];
                [self clickStudRecord:[str intValue]];

            }
        }
    }
    [self clickSetAddBtn];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark -
#pragma mark UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrStu_List count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2) {
        if ([[arrCollect objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }   
    }else {
        if ([[[arrStu_List objectAtIndex:indexPath.row] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
            tagR = indexPath;
        }else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    cell.textLabel.text = [[arrStu_List objectAtIndex:indexPath.row] valueForKey:@"StudName"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"]==2) {
        if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark){
            tagR = indexPath;//me
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"StudGroup", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%d'",[[[arrStu_List objectAtIndex:indexPath.row] valueForKey:@"StudID"] integerValue]]];
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
            [arrGroup removeObject:[arrStu_List objectAtIndex:indexPath.row]];
            [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"NO"];
        }
        else {
            if ([arrGroup count]==4) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Four Students are seleted" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            tagR = indexPath;//me
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"StudGroup", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%d'",[[[arrStu_List objectAtIndex:indexPath.row] valueForKey:@"StudID"] integerValue]]];
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
            [arrGroup addObject:[arrStu_List objectAtIndex:indexPath.row]];
            [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"YES"];
        }
        
    }else {
        if ([arrGroup count]==1) {
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"StudGroup", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%d'",[[[arrStu_List objectAtIndex:tagR.row] valueForKey:@"StudID"] integerValue]]];
            [arrGroup removeObjectAtIndex:0];
            [arrCollect replaceObjectAtIndex:tagR.row withObject:@"NO"];
        }
        tagR = indexPath;
        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"StudGroup", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%d'",[[[arrStu_List objectAtIndex:indexPath.row] valueForKey:@"StudID"] integerValue]]];
        [arrGroup addObject:[arrStu_List objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"YES"];
    }
    [self clickStudRecord:indexPath.row];
}
#pragma mark-
#pragma mark OptionVC delegate methods
- (void)rowOptionSelected:(NSString *)selectedStr {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if ([selectedStr isEqualToString:@""]) {
        return;
    }
    if (btnTag==0) {
        
    }else if (btnTag==1 || btnTag==2 || btnTag==3){
        switch (btnTag) {
            case 1:
            {
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:[[selectedStr componentsSeparatedByString:@":"] objectAtIndex:0],@"Level", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:[[selectedStr componentsSeparatedByString:@":"] objectAtIndex:0],@"Level", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
                }
                
            }
                break;
            case 2:
            {
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinType", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinType", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
                }
                    
                UIButton *bt = (UIButton*)[self.view viewWithTag:3];
                if ([selectedStr isEqualToString:@"None"]) {
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"None",@"ReinSchedule", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                    }else {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"None",@"ReinSchedule", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
                    }
                    [bt setTitle:@"None" forState:UIControlStateNormal];
                }else if ([bt.titleLabel.text isEqualToString:@"None"]) {
                    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"Intermittent",@"ReinSchedule",@"3",@"ReinInterval",nil];
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                        [objDAL updateRecord:dict forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                    }else {
                        [objDAL updateRecord:dict forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
                    }
                    [bt setTitle:@"Intermittent" forState:UIControlStateNormal];
                }
            }
                break;
            case 3:
            {
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinSchedule", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinSchedule", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
                }
            }
                break;            
            default:
                break;
        }
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else{
    }
}
#pragma mark-
#pragma mark OptionVC1 delegate methods
- (void)rowOptionSelected1:(NSString *)selectedStr {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if ([selectedStr isEqualToString:@""]) {
        return;
    }
    if (btnTag==0) {
        
    }else if (btnTag==5){
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueType", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        }else {
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueType", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
        }
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else if (btnTag==4){
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueCat", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        }else {
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueCat", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:strStuID];
        }
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else {
    }
}
#pragma mark -
#pragma mark AddStudentVC delegate methods
- (void)AddStudentDone:(int)tagAUD {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if (btnTag == 16) {
        [arrStu_List removeAllObjects];
        [arrCollect removeAllObjects];
        [arrGroup removeAllObjects];
        [arrStu_List addObjectsFromArray:[self getSudentsRecords]];    
        for (int i=0; i<[arrStu_List count]; i++) {
            if ([[[arrStu_List objectAtIndex:i] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
                [arrCollect addObject:@"YES"];
                [arrGroup addObject:[arrStu_List objectAtIndex:i]];
            }else{
                [arrCollect addObject:@"NO"];
            }
        }
        [tblStudents reloadData];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            
        }else {
            [self loadBlankView];
        }
    }else if (btnTag == 17) {
        if (tagAUD==2) {
            [arrStu_List removeAllObjects];
            [arrCollect removeAllObjects];
            [arrGroup removeAllObjects];
            [arrStu_List addObjectsFromArray:[self getSudentsRecords]];
            for (int i=0; i<[arrStu_List count]; i++) {
                if ([[[arrStu_List objectAtIndex:i] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
                    [arrCollect addObject:@"YES"];
                    [arrGroup addObject:[arrStu_List objectAtIndex:i]];
                }else{
                    [arrCollect addObject:@"NO"];
                }
            }
            [tblStudents reloadData];
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                
            }else {
                [self loadBlankView];
            }
        }else if (tagAUD==1) {
            [arrStu_List removeAllObjects];
            [arrStu_List addObjectsFromArray:[self getSudentsRecords]];
            [tblStudents reloadData];
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                
            }else {           
                [self clickStudRecord:tagR.row];
                [tblStudents selectRowAtIndexPath:tagR animated:NO scrollPosition: UITableViewScrollPositionNone];
            }
        }
    }
    [self clickSetAddBtn];
}
#pragma mark-
#pragma mark custom methods
-(void)clickHome:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)clickSetOptions:(id)sender
{
    if (tagR == nil && [[NSUserDefaults standardUserDefaults] integerForKey:@"Group"]!=2) {
        if ([arrStu_List count]==0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No student created" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select the student" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    UIButton *btn = sender;
    NSLog(@"Tag:%d",btn.tag);
    if (btn.tag == 3 && [btn.titleLabel.text isEqualToString:@"None"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select Reinforcement Type" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    btnTag = btn.tag;
    switch (btn.tag) {
        case 1:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Level 1: 3 pictures",@"Level 2: 4 pictures",@"Level 3: 5 pictures",@"Level 4: no pictures", nil]:strStuID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(460, 23, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];        
        }
            break;
        case 2:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Auditory",@"Visual",@"Both",@"None", nil]:strStuID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(460, 159, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 3:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Intermittent",@"Continuous", nil]:strStuID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(460, 208, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 4:
        {
            NSMutableArray *arrC = [NSMutableArray array];
            [arrC addObjectsFromArray:[NSArray arrayWithObjects:@"Community",@"Food & Drinks",@"Health",@"Home",@"Leisure/Recreational",@"School", nil]];
            //
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                NSMutableArray *arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings" withCondition:@"StudGroup=" withColumnValue:@"'ON'"]];
                for (int i=0 ; i<[arrStud count]; i++) {
                    NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT DISTINCT QCat FROM tblCustom where StudID='%@';",[[arrStud objectAtIndex:i] valueForKey:@"StudID"]]]];
                    for (int i=0; i<[diC count]; i++) {
                        if ([arrC containsObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]]) {
                            
                        }else {
                            [arrC addObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]];
                        }
                    }
                }                
            }else {
                NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT DISTINCT QCat FROM tblCustom where StudID='%@';",strStuID]]];
                for (int i=0; i<[diC count]; i++) {
                    if ([arrC containsObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]]) {
                        
                    }else {
                        [arrC addObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]];
                    }
                }
            }
            //
            [arrC insertObject:@"all" atIndex:0];
            self.viewOptionsVC1 = [[OptionVC1 alloc] initWithNibName:@"OptionVC1" bundle:nil];
            _viewOptionsVC1.delegate=self;
            [_viewOptionsVC1 getOptions:arrC:2];
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC1];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 174);            
            [self.popOverOVC presentPopoverFromRect:CGRectMake(460, 300, 285, 174) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;
        case 5:
        {
            self.viewOptionsVC1 = [[OptionVC1 alloc] initWithNibName:@"OptionVC1" bundle:nil];
            _viewOptionsVC1.delegate=self;
            [_viewOptionsVC1 getOptions:[NSArray arrayWithObjects:@"all",@"who",@"what",@"where",@"when", nil]:1];
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC1];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 174);
            [self.popOverOVC presentPopoverFromRect:CGRectMake(460, 344, 285, 174) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        default:
            break;
    }
}
-(void)clickSwitchOpt:(id)sender
{
    NSLog(@"Switch:strStuID:%@",strStuID);
    UISwitch *swt = sender;
    switch (swt.tag) {
        case 10:
        {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                if ([swt isOn]) {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VPrompt", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else{
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VPrompt", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
                }
            }else {
                if ([strStuID isEqualToString:@""]) {
                    
                }else {
                    if ([swt isOn]) {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VPrompt", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];
                    }else{
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VPrompt", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];                
                    }
                }
            }
        }
            break;
        case 11:
        {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                if ([swt isOn]) {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"AFeedback", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else{
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"AFeedback", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
                }
            }else {
                if ([strStuID isEqualToString:@""]) {
                    
                }else {
                    if ([swt isOn]) {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"AFeedback", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];
                    }else{
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"AFeedback", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];                
                    }
                }
            }
        }
            break;
        case 12:
        {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                if ([swt isOn]) {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"SpokenVoice", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else{
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"SpokenVoice", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
                }
            }else {
                if ([strStuID isEqualToString:@""]) {
                    
                }else {
                    if ([swt isOn]) {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"SpokenVoice", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];
                    }else{
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"SpokenVoice", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];                
                    }
                }
            }
        }
            break;
        case 13:
        {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                if ([swt isOn]) {
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VisualTrials", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
                }else{
                    [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VisualTrials", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
                }
            }else {
                if ([strStuID isEqualToString:@""]) {
                    
                }else {
                    if ([swt isOn]) {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VisualTrials", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];
                        lblNOT.hidden = NO;
                        imgSwtNT.hidden = NO;
                        UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                        swt1.hidden = NO;
                    }else{
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VisualTrials", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];      
                        lblNOT.hidden = YES;
                        imgSwtNT.hidden = YES;
                        UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                        swt1.hidden = YES;
                    }
                }
            }
        }
            break;
        case 14:{
            if ([strStuID isEqualToString:@""]) {
                
            }else {
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                }else {
                    if ([swt isOn]) {
                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"5",@"NoOfTrials", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]];                
                        imgSwtNT.image = [UIImage imageNamed:@"On5.png"];
                        swt.on =YES;
                    }else{
//                        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"10",@"NoOfTrials", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStuID]]; 
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy full version to work on 10 trials." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        imgSwtNT.image = [UIImage imageNamed:@"On10.png"];
                        swt.on =NO;
                    }
                }                
            }
        }
            break;
        case 15:{
            if ([swt isOn]) {
                [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"Group"];
                [[NSUserDefaults standardUserDefaults] synchronize]; 
                btnEdit.hidden=YES;
                btnSImg.hidden=YES;
                lblStudName.hidden=YES;
                lblTAge.hidden=YES;
                lblStudAge.hidden=YES;
                lblTGrade.hidden=YES;
                lblStudGrade.hidden=YES;
                lblNOT.hidden =YES;
                UISwitch *swtNOT = (UISwitch*)[self.view viewWithTag:14];
                swtNOT.hidden=YES;
                imgSwtNT.hidden=YES;
                [self loadBlankView];
                tagR=nil;
                strStuID =@"";
                [self loadGroupSettings];
            }else{
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"Group"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                btnEdit.hidden=NO;
                btnSImg.hidden=NO;
                lblStudName.hidden=NO;
                lblTAge.hidden=NO;
                lblStudAge.hidden=NO;
                lblTGrade.hidden=NO;
                lblStudGrade.hidden=NO;
                lblNOT.hidden =NO;
                UISwitch *swtNOT = (UISwitch*)[self.view viewWithTag:14];
                swtNOT.hidden=NO;
                imgSwtNT.hidden=NO;
                [self loadBlankView];
                tagR=nil;
                strStuID =@"";
            }
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"StudGroup", nil] forID:@"StudGroup='" inTable:@"StudSettings" withValue:@"ON'"];
            
            [arrCollect removeAllObjects];
            [arrGroup removeAllObjects];
            [arrStu_List removeAllObjects];
            [arrStu_List addObjectsFromArray:[self getSudentsRecords]];
            if ([arrStu_List count]>0) {
                for (int i=0; i<[arrStu_List count]; i++) {
                    if ([[[arrStu_List objectAtIndex:i] valueForKey:@"StudGroup"] isEqualToString:@"ON"]) {
                        [arrCollect addObject:@"YES"];
                        [arrGroup addObject:[arrStu_List objectAtIndex:i]];
                    }else{
                        [arrCollect addObject:@"NO"];
                    }                
                }
            [tblStudents reloadData];
            }
        }
            break;
        default:
            break;
    }
}
-(void)clickCreateNewStudent:(id)sender
{
    if ([arrStu_List count]==3) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy the full version to add more students" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;        
    }
    btnTag = 16;
    self.viewAddStud = [[AddStudentVC alloc] initWithNibName:@"AddStudentVC" bundle:nil];
    _viewAddStud.delegate=self;
    self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewAddStud];
    self.popOverOVC.popoverContentSize = CGSizeMake(430, 445);
    [self.popOverOVC presentPopoverFromRect:CGRectMake(-170, 225, 430, 445) 
                                     inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)clickEdit:(id)sender
{
    if ([strStuID isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No student created" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    btnTag = 17;
    self.viewAddStud = [[AddStudentVC alloc] initWithNibName:@"AddStudentVC" bundle:nil];
    _viewAddStud.delegate=self;
    [_viewAddStud getStudentDat:strStuID :2];
    self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewAddStud];
    self.popOverOVC.popoverContentSize = CGSizeMake(430, 445);
    [self.popOverOVC presentPopoverFromRect:CGRectMake(-170, 110, 430, 445) 
                                     inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)clickDC:(id)sender
{
    if ([strStuID isEqualToString:@""]) {
        
    }else {
        DataCollectVC *viewController = [[DataCollectVC alloc] initWithNibName:@"DataCollectVC" bundle:nil];
        [viewController getStudID:strStuID];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}
-(void)clickCustomQue:(id)sender
{
    if ([strStuID isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select the student" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    CustomQueVC *viewController = [[CustomQueVC alloc] initWithNibName:@"CustomQueVC" bundle:nil];
    [viewController getStudID:strStuID];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)clickGo:(id)sender
{
    NSMutableArray *arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings" withCondition:@"StudGroup=" withColumnValue:@"'ON'"]];
    NSString *strMsg=@"";
    if ([arrStud count]==0) {
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            strMsg=@"Students Group not Created";
        }else {
            strMsg=@"Please select the student";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    GoVC *viewController = [[GoVC alloc] initWithNibName:@"GoVC" bundle:nil];
    [viewController getViewTag:18];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(NSMutableArray *)getSudentsRecords
{
    NSMutableArray *arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings order by lower(StudName)"]];
    return arrStud;
}
-(void)loadGroupSettings
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblGroup where GroupID='1'"]]];
    UIButton *btn = (UIButton*)[self.view viewWithTag:1];
    UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
    UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
    UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
    UIButton *btn4 = (UIButton*)[self.view viewWithTag:5];
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 1"]) {
        [btn setTitle:@"Level 1: 3 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 2"]) {
        [btn setTitle:@"Level 2: 4 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 3"]) {
        [btn setTitle:@"Level 3: 5 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 4"]) {
        [btn setTitle:@"Level 4: no pictures" forState:UIControlStateNormal];
    }
    [btn1 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] forState:UIControlStateNormal];
    [btn2 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"ReinSchedule"] forState:UIControlStateNormal];
    [btn3 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"QueCat"] forState:UIControlStateNormal];
    [btn4 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"QueType"] forState:UIControlStateNormal];
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"VPrompt"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:10];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:10];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"AFeedback"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:11];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:11];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"SpokenVoice"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:12];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:12];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"VisualTrials"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:13];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:13];
        swt.on=NO;
    }
}
-(void)clickStudRecord:(NSInteger)tagRow
{
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
        strStuID = [NSString stringWithFormat:@"%d",[[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudID"] integerValue]];
        lblStudName.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudName"];
        lblStudAge.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudAge"];
        lblStudGrade.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudGrade"];
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
            [btnSImg setImage:nil forState:UIControlStateNormal];
        }else{
            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docsDir = [dirPaths objectAtIndex:0];
            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudPhoto"]];
            UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
            [btnSImg setImage:imgGet forState:UIControlStateNormal];
        }        
    }else {
        strStuID = [NSString stringWithFormat:@"%d",[[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudID"] integerValue]];
        lblStudName.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudName"];
        lblStudAge.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudAge"];
        lblStudGrade.text = [[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudGrade"];
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
            [btnSImg setImage:nil forState:UIControlStateNormal];
        }else{
            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docsDir = [dirPaths objectAtIndex:0];
            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"StudPhoto"]];
            UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
            [btnSImg setImage:imgGet forState:UIControlStateNormal];
        }
        UIButton *btn = (UIButton*)[self.view viewWithTag:1];
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"Level"] isEqualToString:@"Level 1"]) {
            [btn setTitle:@"Level 1: 3 pictures" forState:UIControlStateNormal];
        }else if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"Level"] isEqualToString:@"Level 2"]) {
            [btn setTitle:@"Level 2: 4 pictures" forState:UIControlStateNormal];
        }else if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"Level"] isEqualToString:@"Level 3"]) {
            [btn setTitle:@"Level 3: 5 pictures" forState:UIControlStateNormal];
        }else if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"Level"] isEqualToString:@"Level 4"]) {
            [btn setTitle:@"Level 4: no pictures" forState:UIControlStateNormal];
        } 
        UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
        [btn1 setTitle:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"ReinType"] forState:UIControlStateNormal];
        UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
        [btn2 setTitle:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"ReinSchedule"] forState:UIControlStateNormal];
        UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
        [btn3 setTitle:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"QueCat"] forState:UIControlStateNormal];
        UIButton *btn4 = (UIButton*)[self.view viewWithTag:5];
        [btn4 setTitle:[[arrStu_List objectAtIndex:tagRow] valueForKey:@"QueType"] forState:UIControlStateNormal];
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"VPrompt"] isEqualToString:@"ON"]) {
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:10];
            swt.on=YES;
        }else{
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:10];
            swt.on=NO;
        }
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"AFeedback"] isEqualToString:@"ON"]) {
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:11];
            swt.on=YES;
        }else{
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:11];
            swt.on=NO;
        }
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"SpokenVoice"] isEqualToString:@"ON"]) {
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:12];
            swt.on=YES;
        }else{
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:12];
            swt.on=NO;
        }
        if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"VisualTrials"] isEqualToString:@"ON"]) {
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:13];
            swt.on=YES;
            lblNOT.hidden = NO;
            imgSwtNT.hidden = NO;
            if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"NoOfTrials"] isEqualToString:@"5"]) {
                UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                swt1.hidden = NO;
                swt1.on=YES;
                imgSwtNT.image = [UIImage imageNamed:@"On5.png"];
            }else{
                UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                swt1.hidden = NO;
                swt1.on=NO;
                imgSwtNT.image = [UIImage imageNamed:@"On10.png"];
            }
        }else{
            UISwitch *swt = (UISwitch *)[self.view viewWithTag:13];
            swt.on=NO;
            lblNOT.hidden = YES;
            imgSwtNT.hidden = YES;
            if ([[[arrStu_List objectAtIndex:tagRow] valueForKey:@"NoOfTrials"] isEqualToString:@"5"]) {
                UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                swt1.hidden = YES;
                swt1.on=YES;
                imgSwtNT.image = [UIImage imageNamed:@"On5.png"];
            }else{
                UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:14];
                swt1.hidden = YES;
                swt1.on=NO;
                imgSwtNT.image = [UIImage imageNamed:@"On10.png"];
            }
        }
    }
}
-(void)loadBlankView
{
    strStuID = @"";
    lblStudName.text = @"";
    lblStudAge.text = @"";
    lblStudGrade.text = @"";
    [btnSImg setImage:nil forState:UIControlStateNormal];
    UIButton *btn = (UIButton*)[self.view viewWithTag:1];
    [btn setTitle:@"" forState:UIControlStateNormal];
    UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
    [btn1 setTitle:@"" forState:UIControlStateNormal];
    UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
    [btn2 setTitle:@"" forState:UIControlStateNormal];
    UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
    [btn3 setTitle:@"" forState:UIControlStateNormal];
    UIButton *btn4 = (UIButton*)[self.view viewWithTag:5];
    [btn4 setTitle:@"" forState:UIControlStateNormal];
    UISwitch *swt = (UISwitch *)[self.view viewWithTag:10];
    swt.on=YES;
    UISwitch *swt1 = (UISwitch *)[self.view viewWithTag:11];
    swt1.on=YES;
    UISwitch *swt2 = (UISwitch *)[self.view viewWithTag:12];
    swt2.on=YES;
    UISwitch *swt3 = (UISwitch *)[self.view viewWithTag:13];
    swt3.on=YES;
    UISwitch *swt4 = (UISwitch *)[self.view viewWithTag:14];
    swt4.on=YES;
    imgSwtNT.image = [UIImage imageNamed:@"On5.png"];
}
-(void)clickSetAddBtn
{
    if ([arrStu_List count] == 0) {
        tblStudents.hidden = YES;
        btnAddS.frame = CGRectMake(36, 108, 219, 37);   
    }else if ([arrStu_List count] >= 13) {
        tblStudents.hidden = NO;
        tblStudents.frame = CGRectMake(36, 108, 219, 571);
        btnAddS.frame = CGRectMake(36, 678, 219, 37);
    }else {
        tblStudents.hidden = NO;
        tblStudents.frame = CGRectMake(36, 108, 219, [arrStu_List count]*44);
        btnAddS.frame = CGRectMake(36,108+([arrStu_List count]*44), 219, 37);
    }
}
@end
