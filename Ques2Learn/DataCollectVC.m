//
//  DataCollectVC.m
//  Ques2Learn
//
//  Created by apple on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataCollectVC.h"
//#import "Reachability.h"
@interface DataCollectVC (Private)
- (void) generatePdfWithFilePath: (NSString *)thefilePath;
- (void) drawText;
- (void) drawText1:(CGRect)rectToDraw:(NSString*)strToDraw;
@end
@implementation DataCollectVC
@synthesize viewRVC = _viewRVC;
@synthesize viewSelectStudVC = _viewSelectStudVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Private Methods
- (void) drawText
{
    
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    [strName drawInRect:CGRectMake(0, 0, 606, 23.5) 
                  withFont:font
             lineBreakMode:UILineBreakModeWordWrap
                 alignment:UITextAlignmentCenter];
    [@"Date" drawInRect:CGRectMake(0, 23.5, 85, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"Level" drawInRect:CGRectMake(85, 23.5, 50, 44.5)
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"Category" drawInRect:CGRectMake(135, 23.5, 89, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"Who" drawInRect:CGRectMake(224, 23.5, 67, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"What" drawInRect:CGRectMake(291, 23.5, 67, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"When" drawInRect:CGRectMake(358, 23.5, 67, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"Where" drawInRect:CGRectMake(425, 23.5, 67, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
    [@"Visual Prompt" drawInRect:CGRectMake(492, 23.5, 114, 44.5) 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
}
- (void) drawText1:(CGRect)rectToDraw:(NSString*)strToDraw{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:13];
    [strToDraw drawInRect:rectToDraw 
               withFont:font
          lineBreakMode:UILineBreakModeWordWrap
              alignment:UITextAlignmentCenter];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    tagS = 0;
    tagP1M1 = 0;
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    arrSDetail = [[NSMutableArray alloc] init];
    NSArray *arr = [strStudID componentsSeparatedByString:@","];
    if ([arr count]>0 && [[arr objectAtIndex:0] isEqualToString:@""]) {
        self.viewSelectStudVC = [[SelectStudVC alloc] initWithNibName:@"SelectStudVC" bundle:nil];
        _viewSelectStudVC.delegate=self;
        _viewSelectStudVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        _viewSelectStudVC.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:_viewSelectStudVC animated:YES];        
    }else if ([arr count]>0){
        CGFloat X=0;
        for (int i = 0; i<[arr count]; i++) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%@'",[arr objectAtIndex:i]]]];
            if ([dict count]>0) {
                NSString *strSN = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
                UIView *viewStud = [[UIView alloc] initWithFrame:CGRectMake(X, 0, 234, 100)];
                viewStud.backgroundColor = [UIColor clearColor];
                UIButton *btnSImg = [UIButton buttonWithType:UIButtonTypeCustom];
                btnSImg.frame = CGRectMake(9, 15, 60, 71);
                btnSImg.tag = [[arr objectAtIndex:i] intValue];
                [btnSImg addTarget:self action:@selector(clickStud:) forControlEvents:UIControlEventTouchUpInside];
                UILabel *lblSName = [[UILabel alloc] initWithFrame:CGRectMake(79, 19, 143, 62)];
                lblSName.text = [[dict valueForKey:@"Table1"] valueForKey:@"StudName"];
                lblSName.backgroundColor = [UIColor clearColor];
                lblSName.font = [UIFont fontWithName:@"Helvetica Bold" size:25];
                if ([strSN isEqualToString:@"OK.png"]) {
                }else {
                    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *docsDir = [dirPaths objectAtIndex:0];
                    NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
                    UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                    if (imgGet == nil) {
                        
                    }else {
                        [btnSImg setBackgroundImage:imgGet forState:UIControlStateNormal];
                    }
                }
                [viewStud addSubview:btnSImg];
                [viewStud addSubview:lblSName];
                [scrVSList addSubview:viewStud];
                if (i==0) {
                    [self clickStud:btnSImg];
                }
            }
            X = X+234;
        }
        scrVSList.contentSize = CGSizeMake(X, 100);    
    }else {
        self.viewSelectStudVC = [[SelectStudVC alloc] initWithNibName:@"SelectStudVC" bundle:nil];
        _viewSelectStudVC.delegate=self;
        _viewSelectStudVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        _viewSelectStudVC.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:_viewSelectStudVC animated:YES];    
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma
#pragma mark - UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 713, 63)];
    UILabel *lbl0 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 713, 21)];
    lbl0.text = strName;
    lbl0.textAlignment = UITextAlignmentCenter;
    lbl0.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl0.backgroundColor = [UIColor clearColor];
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 21, 105, 42)];
    lbl1.text = @"Date";
    lbl1.textAlignment = UITextAlignmentCenter;
    lbl1.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl1.backgroundColor = [UIColor clearColor];
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(105, 21, 83, 42)];
    lbl2.text = @"Level";
    lbl2.textAlignment = UITextAlignmentCenter;
    lbl2.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl2.backgroundColor = [UIColor clearColor];
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(188, 21, 99, 42)];
    lbl3.text = @"Category";
    lbl3.textAlignment = UITextAlignmentCenter;
    lbl3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl3.backgroundColor = [UIColor clearColor];
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(285, 21, 77, 42)];
    lbl4.text = @"Who";
    lbl4.textAlignment = UITextAlignmentCenter;
    lbl4.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl4.backgroundColor = [UIColor clearColor];
    UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(362, 21, 77, 42)];
    lbl5.text = @"What";
    lbl5.textAlignment = UITextAlignmentCenter;
    lbl5.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl5.backgroundColor = [UIColor clearColor];
    UILabel *lbl6 = [[UILabel alloc] initWithFrame:CGRectMake(439, 21, 77, 42)];
    lbl6.text = @"When";
    lbl6.textAlignment = UITextAlignmentCenter;
    lbl6.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl6.backgroundColor = [UIColor clearColor];
    UILabel *lbl7 = [[UILabel alloc] initWithFrame:CGRectMake(516, 21, 77, 42)];
    lbl7.text = @"Where";
    lbl7.textAlignment = UITextAlignmentCenter;
    lbl7.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl7.backgroundColor = [UIColor clearColor];
    UILabel *lbl8 = [[UILabel alloc] initWithFrame:CGRectMake(591, 21, 124, 42)];
    lbl8.text = @"Visual Prompt";
    lbl8.textAlignment = UITextAlignmentCenter;
    lbl8.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl8.backgroundColor = [UIColor clearColor];
    [viewHeader addSubview:lbl0];
    [viewHeader addSubview:lbl1];
    [viewHeader addSubview:lbl2];
    [viewHeader addSubview:lbl3];
    [viewHeader addSubview:lbl4];
    [viewHeader addSubview:lbl5];
    [viewHeader addSubview:lbl6];
    [viewHeader addSubview:lbl7];
    [viewHeader addSubview:lbl8];
    viewHeader.backgroundColor = [UIColor whiteColor];
    return viewHeader;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSDetail count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 105, 44)];
    lbl1.text = [[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"Date"];
    lbl1.textAlignment = UITextAlignmentCenter;
    lbl1.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(105, 0, 83, 44)];
    lbl2.text = [[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"Level"];
    lbl2.textAlignment = UITextAlignmentCenter;
    lbl2.font = [UIFont fontWithName:@"Helvetica" size:14];
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(188, 0, 99, 44)];
    NSString *strCat = [[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"Category"];
//    strCat = [strCat substringWithRange:NSMakeRange(0, [strCat length]-2)];
    lbl3.text = strCat;
    lbl3.textAlignment = UITextAlignmentCenter;
    lbl3.font = [UIFont fontWithName:@"Helvetica" size:14];
    lbl3.numberOfLines = 2;
    
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(287, 0, 78, 44)];
    double in1 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"ScoreWho"] doubleValue];
    double cn1 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"CntWho"] doubleValue];
    if (cn1 == 0) {
        lbl4.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl4.text = [NSString stringWithFormat:@"%.f %@",(in1/cn1)*100,@"%"];
    }
    lbl4.textAlignment = UITextAlignmentCenter;
    lbl4.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(364, 0, 77, 44)];
    double in2 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"ScoreWhat"] intValue];
    double cn2 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"CntWhat"] intValue];
    if (cn2 == 0) {
        lbl5.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl5.text = [NSString stringWithFormat:@"%.f %@",(in2/cn2)*100,@"%"];
    }
    lbl5.textAlignment = UITextAlignmentCenter;
    lbl5.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    UILabel *lbl6 = [[UILabel alloc] initWithFrame:CGRectMake(441, 0, 77, 44)];
    double in3 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"ScoreWhen"] intValue];
    double cn3 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"CntWhen"] intValue];
    if (cn3 == 0) {
        lbl6.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl6.text = [NSString stringWithFormat:@"%.f %@",(in3/cn3)*100,@"%"];
    }
    lbl6.textAlignment = UITextAlignmentCenter;
    lbl6.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    UILabel *lbl7 = [[UILabel alloc] initWithFrame:CGRectMake(518, 0, 77, 44)];
    double in4 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"ScoreWhere"] intValue];
    double cn4 = [[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"CntWhere"] intValue];
    if (cn4 == 0) {
        lbl7.text = [NSString stringWithFormat:@"0 %@",@"%"];
    }else {
        lbl7.text = [NSString stringWithFormat:@"%.f %@",(in4/cn4)*100,@"%"];
    }
    lbl7.textAlignment = UITextAlignmentCenter;
    lbl7.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    UILabel *lbl8 = [[UILabel alloc] initWithFrame:CGRectMake(595, 0, 120, 44)];
    if ([[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"VPrompt"] isEqualToString:@"1"]) {
        lbl8.text = @"ON";
    }else {
        lbl8.text = @"OFF";
    }
    lbl8.textAlignment = UITextAlignmentCenter;
    lbl8.font = [UIFont fontWithName:@"Helvetica" size:14];

    [cell.contentView addSubview:lbl1];
    [cell.contentView addSubview:lbl2];
    [cell.contentView addSubview:lbl3];
    [cell.contentView addSubview:lbl4];
    [cell.contentView addSubview:lbl5];
    [cell.contentView addSubview:lbl6];
    [cell.contentView addSubview:lbl7];
    [cell.contentView addSubview:lbl8];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [objDAL deleteFromTable:@"tblScore" WhereField:[NSString stringWithFormat:@"id='%@'",[[arrSDetail objectAtIndex:indexPath.row] valueForKey:@"id"]] Condition:@""];
        [arrSDetail removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}
#pragma mark -
#pragma mark OptionVC delegate methods
-(void)selectStudsDone:(NSInteger)idStr
{
    NSLog(@"idStr:%d",idStr);
    if (idStr==0) {
        
    }else {
        NSLog(@"arrGet:%@",arrGet);
        for (int i=0; i<[arrGet count]; i++) {
            NSMutableDictionary *dic = [[[arrGet objectAtIndex:i] valueForKey:@"root"] valueForKey:@"dic"];
            if ([[dic valueForKey:@"bool"] intValue]==1) {
                [self saveScore:[dic valueForKey:@"condition"] :[dic valueForKey:@"record"] :[NSString stringWithFormat:@"%d",idStr]];  
                [self updateScore:[dic valueForKey:@"condition"] :[dic valueForKey:@"record"] :[NSString stringWithFormat:@"%d",idStr]];
            }else if ([[dic valueForKey:@"bool"] intValue]==2) {
                [self updateScore:[dic valueForKey:@"condition"] :[dic valueForKey:@"record"] :[NSString stringWithFormat:@"%d",idStr]];
            }
        }
        NSMutableDictionary *dict =[NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%d'",idStr]]];
        UIView *viewStud = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 234, 100)];
        viewStud.backgroundColor = [UIColor clearColor];
        UIButton *btnSImg = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSImg.frame = CGRectMake(9, 15, 60, 71);
        btnSImg.tag = idStr;
        [btnSImg addTarget:self action:@selector(clickStud:) forControlEvents:UIControlEventTouchUpInside];
        NSString *strSN = [[dict valueForKey:@"Table1"] valueForKey:@"StudPhoto"];
        if ([strSN isEqualToString:@"OK.png"]) {
        }else {
            NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docsDir = [dirPaths objectAtIndex:0];
            NSString *imgFilePath = [docsDir stringByAppendingPathComponent:strSN];
            UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
            if (imgGet == nil) {
                
            }else {
                [btnSImg setBackgroundImage:imgGet forState:UIControlStateNormal];
            }
        }
        [viewStud addSubview:btnSImg];
        [scrVSList addSubview:viewStud];
        [self clickStud:btnSImg];
        scrVSList.contentSize = CGSizeMake(234, 100);
    }
}
#pragma mark -
#pragma mark OptionVC delegate methods
-(void)selectRecordsDone:(NSMutableArray*)arrIndStr
{
    NSLog(@"arrIndStr:%@",arrIndStr);
    if ([arrIndStr containsObject:@"YES"]) {
        if (tagP1M1 == 2) {
            tagP1M1 = 0;
            [self reloadTbl:arrIndStr];
            //[self joinPDF];
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(SendMail) userInfo:nil repeats:NO];
        }else if (tagP1M1 == 4) {
            tagP1M1 = 0;
            [self reloadTbl:arrIndStr];
            //[self joinPDF];
            Class printControllerClass = NSClassFromString(@"UIPrintInteractionController");
            if (printControllerClass) {
                printController = [printControllerClass sharedPrintController];
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(printTapped) userInfo:nil repeats:NO];
            } else {
                //[self setupCantPrintLabel];
            }
        }
    }else {

    }
}
#pragma mark -
#pragma mark MFMailComposeViewController delegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];                      
}
#pragma mark -
#pragma mark UIAlertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99999) {
        if (buttonIndex==2) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to share all reports?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes",@"No", nil];
            alert.tag = 999999;
            [alert show];            
        }else if (buttonIndex==1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to print all reports?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes",@"No", nil];
            alert.tag = 9999999;
            [alert show];        
        }else {
        
        }
    }
    if (alertView.tag == 999999) {
        if (buttonIndex==1) {

            if ([MFMailComposeViewController canSendMail]) {
                //tagP1M1 = 1;
                [self makeScorePDF];
                //[self joinPDF];
                MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
                mailViewController.mailComposeDelegate = self;
                [mailViewController setSubject:@"My Score"];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *pdfPathOutput = [documentsDirectory stringByAppendingPathComponent:@"out.pdf"];
                NSData * pdfData = [NSData dataWithContentsOfFile:pdfPathOutput];
                [mailViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"out.pdf"];
                
                NSString * body = @"";
                [mailViewController setMessageBody:body isHTML:NO];

                [[mailViewController navigationBar] setBarStyle:UIBarStyleBlackOpaque];
                [self presentModalViewController:mailViewController animated:YES];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You are not logged in your device email account." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                [alert show];
            }            
        }else if (buttonIndex == 2) {
            tagP1M1 = 2;
            self.viewRVC = [[SelectRecordVC alloc] initWithNibName:nil bundle:nil];
            _viewRVC.delegate=self;
            _viewRVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            _viewRVC.modalPresentationStyle = UIModalPresentationFormSheet;
            [_viewRVC getRecords:arrSDetail];
            [self presentModalViewController:_viewRVC animated:YES];
        } else {
        
        }
    }
    if (alertView.tag == 9999999) {
        if (buttonIndex==1) {
            //tagP1M1 = 3;
            [self makeScorePDF];
            //[self joinPDF];
            Class printControllerClass = NSClassFromString(@"UIPrintInteractionController");
            if (printControllerClass) {
                printController = [printControllerClass sharedPrintController];
                [self printTapped];
            } else {
                //[self setupCantPrintLabel];
            }
        }else if (buttonIndex == 2) {
            tagP1M1 = 4;
            self.viewRVC = [[SelectRecordVC alloc] initWithNibName:nil bundle:nil];
            _viewRVC.delegate=self;
            _viewRVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            _viewRVC.modalPresentationStyle = UIModalPresentationFormSheet;
            [_viewRVC getRecords:arrSDetail];
            [self presentModalViewController:_viewRVC animated:YES];
        } else {
            
        }
    }
}
#pragma mark-
#pragma mark custom methods
//- (BOOL) isconnectedToNetwork
//{
//	Reachability *reachability = [Reachability reachabilityForInternetConnection];  
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus]; 
//    return !(networkStatus == NotReachable);
//}
-(void)reloadTbl:(NSMutableArray*)arr
{
    NSMutableArray *arrTmp = [NSMutableArray array];
    [arrTmp setArray:arrSDetail];
    [arrSDetail removeAllObjects];
    for (int i = 0; i<[arr count]; i++) {
        if ([[arr objectAtIndex:i] isEqualToString:@"YES"]) {
            [arrSDetail addObject:[arrTmp objectAtIndex:i]];   
        }
    }
    [tblSRecords reloadData];
    [self makeScorePDF];
    [arrSDetail removeAllObjects];
    [arrSDetail setArray:arrTmp];
    [tblSRecords reloadData];
    
}
- (void)joinPDF{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // File paths
    NSString *pdfPath1 = [documentsDirectory stringByAppendingPathComponent:@"456.pdf"];
    NSString *pdfPath2 = [documentsDirectory stringByAppendingPathComponent:@"123.pdf"];
    NSString *pdfPathOutput = [documentsDirectory stringByAppendingPathComponent:@"out.pdf"];
    // File URLs
    CFURLRef pdfURL1 = CFURLCreateWithFileSystemPath (NULL,(__bridge CFStringRef)pdfPath1, kCFURLPOSIXPathStyle, FALSE);
    CFURLRef pdfURL2 = CFURLCreateWithFileSystemPath (NULL,(__bridge CFStringRef)pdfPath2, kCFURLPOSIXPathStyle, FALSE);
    CFURLRef pdfURLOutput = CFURLCreateWithFileSystemPath (NULL,(__bridge CFStringRef)pdfPathOutput, kCFURLPOSIXPathStyle, FALSE);    
    // File references
    CGPDFDocumentRef pdfRef1 = CGPDFDocumentCreateWithURL(pdfURL1);
    CGPDFDocumentRef pdfRef2 = CGPDFDocumentCreateWithURL(pdfURL2);
    // Number of pages
    NSInteger numberOfPages1 = CGPDFDocumentGetNumberOfPages(pdfRef1);
    NSInteger numberOfPages2 = CGPDFDocumentGetNumberOfPages(pdfRef2);
    // Create the output context
    CGContextRef writeContext = CGPDFContextCreateWithURL(pdfURLOutput, NULL, NULL);
    // Loop variables
    CGPDFPageRef page;
    CGRect mediaBox;
    // Read the first PDF and generate the output pages
    for (int i=1; i<=numberOfPages1; i++) {
        page = CGPDFDocumentGetPage(pdfRef1, i);
        mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        CGContextBeginPage(writeContext, &mediaBox);
        CGContextDrawPDFPage(writeContext, page);
        CGContextEndPage(writeContext);
    }
    // Read the second PDF and generate the output pages
    for (int i=1; i<=numberOfPages2; i++) {
        page = CGPDFDocumentGetPage(pdfRef2, i);
        mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        CGContextBeginPage(writeContext, &mediaBox);
        CGContextDrawPDFPage(writeContext, page);
        CGContextEndPage(writeContext);      
    }
    //NSLog(@"DONE!");
    // Finalize the output file
    CGPDFContextClose(writeContext);
    // Release from memory
    CFRelease(pdfURL1);
    CFRelease(pdfURL2);
    CFRelease(pdfURLOutput);
    CGPDFDocumentRelease(pdfRef1);
    CGPDFDocumentRelease(pdfRef2);
    CGContextRelease(writeContext);
}
-(void)makeScorePDF
{    
    pageSize = CGSizeMake(606, 764);
    NSString *fileName = @"out.pdf";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    [self generatePdfWithFilePath:pdfFileName];
    
//    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
//    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
//    NSMutableData *pdfData = [NSMutableData data];
//    
//    CGPoint savedContentOffset = tblSRecords.contentOffset;
//    CGRect savedFrame = tblSRecords.frame;
//    
//    tblSRecords.contentOffset = CGPointZero;
//    tblSRecords.frame = CGRectMake(0, 0, tblSRecords.contentSize.width, tblSRecords.contentSize.height);        
//    
//    // Image 2
//    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(tblSRecords.contentOffset.x, 0, tblSRecords.contentSize.width, 792), nil);
//    UIGraphicsBeginPDFPage();
//    
//    [tblSRecords.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIGraphicsEndPDFContext();
//    tblSRecords.contentOffset = savedContentOffset;
//    tblSRecords.frame = savedFrame;
//    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:@"out.pdf"];
//    
//    [pdfData writeToFile:documentDirectoryFilename atomically:YES];        
}
- (void) generatePdfWithFilePath: (NSString *)thefilePath
{
    UIGraphicsBeginPDFContextToFile(thefilePath, CGRectZero, nil);
    
    NSInteger currentPage = 0;
    BOOL done = NO;
    do 
    {
        //Start a new page.
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil);
        currentPage++;
        [self drawText];
        CGFloat Y=60;
        for (int i=0; i<[arrSDetail count]; i++) {
            [self drawText1:CGRectMake(0, Y, 85, 44) :[[arrSDetail objectAtIndex:i] valueForKey:@"Date"]];
            [self drawText1:CGRectMake(85, Y, 50, 44) :[[arrSDetail objectAtIndex:i] valueForKey:@"Level"]];
            [self drawText1:CGRectMake(135, Y, 89, 44) :[[arrSDetail objectAtIndex:i] valueForKey:@"Category"]];
            
            double in1 = [[[arrSDetail objectAtIndex:i] valueForKey:@"ScoreWho"] doubleValue];
            double cn1 = [[[arrSDetail objectAtIndex:i] valueForKey:@"CntWho"] doubleValue];
            if (cn1 == 0) {
                [self drawText1:CGRectMake(224, Y, 67, 44) :[NSString stringWithFormat:@"0 %@",@"%"]];
            }else {
                [self drawText1:CGRectMake(224, Y, 67, 44) :[NSString stringWithFormat:@"%.f %@",(in1/cn1)*100,@"%"]];
            }
            
            double in2 = [[[arrSDetail objectAtIndex:i] valueForKey:@"ScoreWhat"] intValue];
            double cn2 = [[[arrSDetail objectAtIndex:i] valueForKey:@"CntWhat"] intValue];
            if (cn2 == 0) {
                [self drawText1:CGRectMake(291, Y, 67, 44) :[NSString stringWithFormat:@"0 %@",@"%"]];
            }else {
                [self drawText1:CGRectMake(291, Y, 67, 44) :[NSString stringWithFormat:@"%.f %@",(in2/cn2)*100,@"%"]];
            }
            
            double in3 = [[[arrSDetail objectAtIndex:i] valueForKey:@"ScoreWhen"] intValue];
            double cn3 = [[[arrSDetail objectAtIndex:i] valueForKey:@"CntWhen"] intValue];
            if (cn3 == 0) {
                [self drawText1:CGRectMake(358, Y, 67, 44) :[NSString stringWithFormat:@"0 %@",@"%"]];
            }else {
                [self drawText1:CGRectMake(358, Y, 67, 44) :[NSString stringWithFormat:@"%.f %@",(in3/cn3)*100,@"%"]];
            }
            double in4 = [[[arrSDetail objectAtIndex:i] valueForKey:@"ScoreWhere"] intValue];
            double cn4 = [[[arrSDetail objectAtIndex:i] valueForKey:@"CntWhere"] intValue];
            if (cn4 == 0) {
                [self drawText1:CGRectMake(425, Y, 67, 44) :[NSString stringWithFormat:@"0 %@",@"%"]];
            }else {
                [self drawText1:CGRectMake(425, Y, 67, 44) :[NSString stringWithFormat:@"%.f %@",(in4/cn4)*100,@"%"]];
            }
            if ([[[arrSDetail objectAtIndex:i] valueForKey:@"VPrompt"] isEqualToString:@"1"]) {
                [self drawText1:CGRectMake(492, Y, 114, 44) :@"ON"];
            }else {
                [self drawText1:CGRectMake(492, Y, 114, 44) :@"OFF"];
            }
            Y=Y+44; 
        }
        done = YES;
    } 
    while (!done);
    UIGraphicsEndPDFContext();
}
- (void)printTapped {  
	void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
	^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
		if (!completed && error) NSLog(@"Print error: %@", error);
	};
	UIPrintInfo *printInfo = [UIPrintInfo printInfo];
	printInfo.outputType=UIPrintInfoOutputPhoto;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfPathOutput = [documentsDirectory stringByAppendingPathComponent:@"out.pdf"];
    
    NSData * pdfData = [NSData dataWithContentsOfFile:pdfPathOutput];
    
	printController.printingItem= pdfData;
	[printController presentFromRect:CGRectMake(670, -30, 320, 200) inView:self.view
								animated:YES completionHandler:completionHandler];
}
-(void)SendMail
{
//    if (![self isconnectedToNetwork]) {
//        return;
//    }
    if ([MFMailComposeViewController canSendMail]) {
        //
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        //[mailViewController setToRecipients:[NSArray arrayWithObject:@""]];
        [mailViewController setSubject:@"My Score"];

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *pdfPathOutput = [documentsDirectory stringByAppendingPathComponent:@"out.pdf"];
        NSData * pdfData = [NSData dataWithContentsOfFile:pdfPathOutput];
        [mailViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"out.pdf"];
        
        NSString * body = @"";
        [mailViewController setMessageBody:body isHTML:NO];
        
        [[mailViewController navigationBar] setBarStyle:UIBarStyleBlackOpaque];
        [self presentModalViewController:mailViewController animated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You are not logged in your device email account." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
    }
}
-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(void)getStudIDFrmMain:(NSString*)idStr:(NSMutableArray*)scoreArr
{
    strStudID = idStr;
    arrGet = [[NSMutableArray alloc] init];
    [arrGet setArray:scoreArr];
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickStud:(id)sender
{
    UIButton *btn = sender;
    if (tagS == 0) {
        
    }else {
        UIButton *btn1 = (UIButton*)[self.view viewWithTag:tagS]; 
        btn1.layer.cornerRadius = 0.0;
        btn1.layer.borderWidth = 0.0;
        btn1.layer.borderColor = [UIColor clearColor].CGColor;
        btn1.layer.masksToBounds = NO;
    }
    btn.layer.cornerRadius = 1.0;
    btn.layer.borderWidth = 2.0;
    btn.layer.borderColor = [UIColor blueColor].CGColor;
    btn.layer.masksToBounds = YES;
    tagS = btn.tag;
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select StudName from StudSettings where StudID='%d'",tagS]]];
    strName = [[dict valueForKey:@"Table1"] valueForKey:@"StudName"];
    arrSDetail = [objDAL SelectWithStar:@"tblScore" withCondition:[NSString stringWithFormat:@"StudID='%d' ORDER BY Date DESC",btn.tag] withColumnValue:@""];
//    NSLog(@"arrSDetail:%@",arrSDetail);
    [tblSRecords reloadData];
}
-(void)clickShare:(id)sender
{
    if ([arrSDetail count]==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No data collected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [tblSRecords setContentOffset:CGPointMake(0, 0)];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Share tour score by" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Print",@"Email", nil];
    alert.tag = 99999;
    [alert show];
}
-(void)saveScore:(NSString*)strCon:(NSMutableDictionary*)dicRecord:(NSString*)strID
{
    NSArray *arrSep = [strCon componentsSeparatedByString:@","];
    NSString *str = [arrSep objectAtIndex:0];
    NSString *strLevel = [arrSep objectAtIndex:1];
    NSString *strVP = [arrSep objectAtIndex:2];
    NSString *strQCat = [arrSep objectAtIndex:3];
    NSString *strQType = [arrSep objectAtIndex:4];
    if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(StudID) FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strID,str,strLevel,strVP,strQCat]] valueForKey:@"Table1"] valueForKey:@"COUNT(StudID)"] isEqualToString:@"0"]) {
        //            NSLog(@"strQType:%@",strQType);
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:strID,@"StudID",str,@"Date",strLevel,@"Level",strQCat,@"Category",strVP,@"VPrompt",@"1",strQType, nil];
        [objDAL insertRecord:dict inTable:@"tblScore"];
    }else {
        int cstPre = [[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT %@ FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strQType,strID,str,strLevel,strVP,strQCat]] valueForKey:@"Table1"] valueForKey:strQType] intValue];
        cstPre= cstPre+1;
        NSString *strM = [NSString stringWithFormat:@"UPDATE tblScore SET %@='%d' WHERE StudID=%@ and Date='%@' and Level=%d and VPrompt=%d and Category='%@'",strQType,cstPre,strID,str,[strLevel intValue],[strVP intValue],strQCat];
        NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:strM]];
        NSLog(@"DiI:%@",d);
    }
}
-(void)updateScore:(NSString*)strCon:(NSMutableDictionary*)dicRecord:(NSString*)strID 
{
    NSArray *arrSep = [strCon componentsSeparatedByString:@","];
    NSString *str = [arrSep objectAtIndex:0];
    NSString *strLevel = [arrSep objectAtIndex:1];
    NSString *strVP = [arrSep objectAtIndex:2];
    NSString *strQCat = [arrSep objectAtIndex:3];
//    NSString *strQType = [arrSep objectAtIndex:4];
    NSString *strCnt = [arrSep objectAtIndex:5];
    NSString *strAttempt = [NSString stringWithFormat:@"%d",[[[dicRecord valueForKey:@"opt1"] valueForKey:@"attempt"] intValue]];
    if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(StudID) FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strID,str,strLevel,strVP,strQCat]] valueForKey:@"Table1"] valueForKey:@"COUNT(StudID)"] isEqualToString:@"0"]) {
        //            NSLog(@"strQType:%@",strQType);
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:strID,@"StudID",str,@"Date",strLevel,@"Level",strQCat,@"Category",strVP,@"VPrompt",strAttempt,strCnt, nil];
        [objDAL insertRecord:dict inTable:@"tblScore"];
    }else {
        int cntPre = [[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT %@ FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strCnt,strID,str,strLevel,strVP,strQCat]] valueForKey:@"Table1"] valueForKey:[NSString stringWithFormat:@"%@",strCnt]] intValue];            
        cntPre = cntPre+[strAttempt intValue];
        
        NSString *strM = [NSString stringWithFormat:@"UPDATE tblScore SET %@='%d' WHERE StudID=%@ and Date='%@' and Level=%d and VPrompt=%d and Category='%@'",strCnt,cntPre,strID,str,[strLevel intValue],[strVP intValue],strQCat];
        NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:strM]];
        NSLog(@"DiU:%@",d);
    }        
}
@end
