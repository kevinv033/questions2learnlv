//
//  GoVC.m
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GoVC.h"
#import "DataCollectVC.h"
@implementation GoVC
@synthesize viewGroupVC = _viewGroupVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    dicMQ = [[NSMutableDictionary alloc] init];
    dictQue5 = [[NSMutableDictionary alloc] init];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    countQue = 0;
    selectedOption = 0;
    selectStudent = 0;
    selectLevel = 0;
    tagVPrompt = 0;
    tagReinSch = 0;
    tagQue = 0;
    tagSV = 0;
    cntCorrect = 0;
    cntCorrect1 = 0;
    tagReinI = 0;
    tagAFB = 0;
    tagReinType = 0;
//    tagSOrN = 0;
    tagSOrN5 = 0;
    imgReinS.frame = CGRectMake(0 , 0, 1024, 768);
    arrQue = [[NSMutableArray alloc] init];
    arrStud = [[NSMutableArray alloc] init];
    arrMVScore = [[NSMutableArray alloc] init];
    arrRecordQueNo = [[NSMutableArray alloc] init];
    if (tagView == 18) {
        arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings" withCondition:@"StudGroup=" withColumnValue:@"'ON'"]];
        if ([arrStud count]==0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a Student" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [dirPaths objectAtIndex:0];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            [self performSelector:@selector(groupSettingsDone) withObject:self afterDelay:0.5];
            switch ([arrStud count]) {
                case 0:
                {
                    
                }
                    break;
                case 1:
                {
                    lblStudent1.text = [[arrStud objectAtIndex:0] valueForKey:@"StudName"];
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent1 setImage:imgGet forState:UIControlStateNormal];
                    }
                    btnStudent1.hidden = NO;
                    lblStudent1.hidden = NO;              
                }
                    break;
                case 2:
                {
                    lblStudent1.text = [[arrStud objectAtIndex:0] valueForKey:@"StudName"];
                    lblStudent2.text = [[arrStud objectAtIndex:1] valueForKey:@"StudName"];
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent1 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent2 setImage:imgGet forState:UIControlStateNormal];
                    }
                    btnStudent1.hidden = NO;
                    lblStudent1.hidden = NO;
                    btnStudent2.hidden = NO;
                    lblStudent2.hidden = NO;            
                }
                    break;
                case 3:
                {
                    lblStudent1.text = [[arrStud objectAtIndex:0] valueForKey:@"StudName"];
                    lblStudent2.text = [[arrStud objectAtIndex:1] valueForKey:@"StudName"];
                    lblStudent3.text = [[arrStud objectAtIndex:2] valueForKey:@"StudName"];
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent1 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent2 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:2] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:2] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent3 setImage:imgGet forState:UIControlStateNormal];
                    }
                    btnStudent1.hidden = NO;
                    lblStudent1.hidden = NO;
                    btnStudent2.hidden = NO;
                    lblStudent2.hidden = NO;
                    btnStudent3.hidden = NO;
                    lblStudent3.hidden = NO;
                }
                    break;
                case 4:
                {
                    lblStudent1.text = [[arrStud objectAtIndex:0] valueForKey:@"StudName"];
                    lblStudent2.text = [[arrStud objectAtIndex:1] valueForKey:@"StudName"];
                    lblStudent3.text = [[arrStud objectAtIndex:2] valueForKey:@"StudName"];
                    lblStudent4.text = [[arrStud objectAtIndex:3] valueForKey:@"StudName"];
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:0] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent1 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:1] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent2 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:2] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:2] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent3 setImage:imgGet forState:UIControlStateNormal];
                    }
                    if ([[[arrStud objectAtIndex:3] valueForKey:@"StudPhoto"] isEqualToString:@"OK.png"]) {
                        
                    }else{
                        NSString *imgFilePath = [docsDir stringByAppendingPathComponent:[[arrStud objectAtIndex:3] valueForKey:@"StudPhoto"]];
                        UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgFilePath]];
                        [btnStudent4 setImage:imgGet forState:UIControlStateNormal];
                    }
                    btnStudent1.hidden = NO;
                    lblStudent1.hidden = NO;
                    btnStudent2.hidden = NO;
                    lblStudent2.hidden = NO;
                    btnStudent3.hidden = NO;
                    lblStudent3.hidden = NO;
                    btnStudent4.hidden = NO;
                    lblStudent4.hidden = NO;
                }
                    break;
                default:
                    break;
            }
        }else{
            switch ([arrStud count]) {
                case 0:
                {
                }
                    break;
                case 1:
                {
                    [self chooseStudent:btnStudent1];
                }
                    break;
                default:
                    break;
            }    
            
        }        
    }else if (tagView == 17){ 
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag =19;
        id sen = btn;
        [self chooseStudent:sen];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma
#pragma mark - UIAlertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 16) {
        if (buttonIndex == 0) {
            if ([arrMVScore count] == 0) {
                
                if (tagView == 18) {
                    DataCollectVC *viewController = [[DataCollectVC alloc] initWithNibName:nil bundle:nil];
                    [viewController getStudID:[[arrStud valueForKey:@"StudID"] componentsJoinedByString:@","]];
                    [self.navigationController pushViewController:viewController animated:YES];
                }else if (tagView == 17) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Data Collected" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    return;
                }                
            }else {
                DataCollectVC *viewController = [[DataCollectVC alloc] initWithNibName:nil bundle:nil];
                if (tagView == 18) {
                    [viewController getStudID:[[arrStud valueForKey:@"StudID"] componentsJoinedByString:@","]];
                }else if (tagView == 17) {
                    NSMutableArray *arr = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings"]];
                    if ([arr count] == 0) {
                        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"No Saved Student" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert1 show];
                        return;
                    }
                    [viewController getStudIDFrmMain:[[arrStud valueForKey:@"StudID"] componentsJoinedByString:@","] :arrMVScore];
                }
                [self.navigationController pushViewController:viewController animated:YES];
            }    
            [arrMVScore removeAllObjects];
            [self loadDict];
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    if (alertView.tag==22) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark-
#pragma mark OptionVC delegate methods
-(void)groupSettingsDone
{
    [self clickLoadQuestion:nil];
}
#pragma
#pragma mark custom methods
-(void)getViewTag:(int)viewTag
{
    tagView = viewTag;
}
-(void)clickHome:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to Data Collection Chart?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
    alert.tag = 16;
    [alert show];
}
-(void)clickSettings
{
    self.viewGroupVC = [[GroupVC alloc] initWithNibName:@"GroupVC" bundle:nil];
    _viewGroupVC.delegate=self;
    _viewGroupVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _viewGroupVC.modalPresentationStyle = UIModalPresentationFormSheet;
	[self presentModalViewController:_viewGroupVC animated:YES];
}
-(void)clickLoadQuestion:(id)sender{
    if (selectStudent==0 && [arrStud count] >0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select the student to answer" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
        return;
    }
    if (tagSV == 1) {
        [btnPOS setTitle:@"Play Audio" forState:UIControlStateNormal];
        btnPOS.tag = 21;
        btnPOS.hidden=NO;
    }else {
        btnPOS.hidden=YES;
    }
    if (audioPlayer.playing) {
        [audioPlayer stop];
    }
    
    if (tagQue==0) {
        //tagQue = countQue;
    }else{
        if (selectedOption == 0) {
            if (selectStudent ==5) {
                
            }else {
                [self updateScore];
            }
        }else {
        }
        //tagQue = countQue;
    }
    tagQue = countQue;
    if (tagQue<[arrQue count]) {
        switch (tagQue) {
            case 1:
                imgView1.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 2:
                imgView2.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 3:
                imgView3.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 4:
                imgView4.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 5:
                imgView5.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 6:
                imgView6.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 7:
                imgView7.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 8:
                imgView8.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            case 9:
                imgView9.image = [UIImage imageNamed:@"checkTrue.png"];
                break;
            default:
                break;
        }
        selectedOption = 0;
        if (selectStudent == 5) {
            tagSOrN5=0;
        }else {
            [dicMQ removeAllObjects];
        }
        [self loadQuestion];
//        NSString *strQ = [self checkForCustomOrModify:@"QText" :[arrQue objectAtIndex:countQue]];
//        lblQue.text = [[arrQue objectAtIndex:countQue] valueForKey:@"QText"]; 
//        lblQue.text = strQ;
        if(tagSV == 1){
            //NSLog(@"(corOrNot) Out:3");
            if (tagQue==0) {
                if (tagView==17) {
                    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(clickPlayOrStop:) userInfo:btnPOS repeats:NO];
                }else {
                    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(clickPlayOrStop:) userInfo:btnPOS repeats:NO];
                }
            }else {
                [self clickPlayOrStop:btnPOS];
            }
        }
        countQue++;
    }else{
        if ([arrQue count] == 0) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select correct level or question type for the category you created." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You entered the incorrect level or question type. Go back to settings." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 22;
            [alert show];
            return;
        }else {
            if (tagQue==[arrQue count]) {
                switch (tagQue) {
                    case 1:
                    {
                        imgView1.image = [UIImage imageNamed:@"checkTrue.png"];                
                    }
                        break;
                    case 2:
                    {
                        imgView2.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 3:
                    {
                        imgView3.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 4:
                    {
                        imgView4.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 5:
                    {
                        imgView5.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 6:
                    {
                        imgView6.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 7:
                    {
                        imgView7.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 8:
                    {
                        imgView8.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 9:
                    {
                        imgView9.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                    case 10:
                    {
                        imgView10.image = [UIImage imageNamed:@"checkTrue.png"];                    
                    }
                        break;
                        
                    default:
                        break;
                }
            }
            UIButton *btn = sender;
            btn.hidden = YES;
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(loadAllBone) userInfo:nil repeats:NO];
            self.view.userInteractionEnabled =NO;
        }
    }
}

-(UIImage*)loadImage:(NSString *)imgN
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *imgPath = [docsDir stringByAppendingPathComponent:imgN];
    UIImage *imgGet = [UIImage imageWithData:[NSData dataWithContentsOfFile:imgPath]];
    return imgGet;
}
-(void)loadDict
{
    switch (selectLevel) {
        case 1:
        {
            //[dictQue removeAllObjects];
            NSMutableDictionary *dictQue = [NSMutableDictionary dictionary];
//            tagSOrN = 0;
            NSMutableDictionary *d1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            [dictQue setObject:d1 forKey:@"opt1"];
            [dictQue setObject:d2 forKey:@"opt2"];
            [dictQue setObject:d3 forKey:@"opt3"];

            NSMutableDictionary *ds = [NSMutableDictionary dictionary];
            if (tagVPrompt == 1) {
                    ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent],[NSNumber numberWithInt:0],@"tag1",[NSNumber numberWithInt:0],@"tag2",[NSNumber numberWithInt:0],@"tag3", nil];
            }else {
                    ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent], nil];
            }
            
    
            if ([dicMQ count]==0) {
                NSMutableArray *arr = [NSMutableArray array];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }else {
                NSMutableArray *arr = [NSMutableArray array];
                [arr setArray:[dicMQ valueForKey:@"sdic"]];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }
        }
            break;
        case 2:
        {
            //[dictQue removeAllObjects];
            NSMutableDictionary *dictQue = [NSMutableDictionary dictionary];
//            tagSOrN = 0;
            NSMutableDictionary *d1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d4 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            [dictQue setObject:d1 forKey:@"opt1"];
            [dictQue setObject:d2 forKey:@"opt2"];
            [dictQue setObject:d3 forKey:@"opt3"];
            [dictQue setObject:d4 forKey:@"opt4"];
            if (selectStudent==5) {
                [dictQue5 removeAllObjects];
                tagSOrN5=0;
                dictQue5 = [NSMutableDictionary dictionaryWithDictionary:dictQue];
                NSLog(@"dictQue5:%@",dictQue5);
            }else {
                NSMutableDictionary *ds = [NSMutableDictionary dictionary];
                if (tagVPrompt == 1) {
                    ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent],[NSNumber numberWithInt:0],@"tag1",[NSNumber numberWithInt:0],@"tag2",[NSNumber numberWithInt:0],@"tag3",[NSNumber numberWithInt:0],@"tag4", nil];
                }else {
                    ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent], nil];
                }
                
                if ([dicMQ count]==0) {
                    NSMutableArray *arr = [NSMutableArray array];
                    [arr addObject:ds];
                    dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
                }else {
                    NSMutableArray *arr = [NSMutableArray array];
                    [arr setArray:[dicMQ valueForKey:@"sdic"]];
                    [arr addObject:ds];
                    dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
                }
            }
        }
            break;
        case 3:
        {
            //[dictQue removeAllObjects];
            NSMutableDictionary *dictQue = [NSMutableDictionary dictionary];
//            tagSOrN = 0;
            NSMutableDictionary *d1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d3 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d4 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d5 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            [dictQue setObject:d1 forKey:@"opt1"];
            [dictQue setObject:d2 forKey:@"opt2"];
            [dictQue setObject:d3 forKey:@"opt3"];
            [dictQue setObject:d4 forKey:@"opt4"];
            [dictQue setObject:d5 forKey:@"opt5"];
            NSMutableDictionary *ds = [NSMutableDictionary dictionary];
            if (tagVPrompt == 1) {
                ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent],[NSNumber numberWithInt:0],@"tag1",[NSNumber numberWithInt:0],@"tag2",[NSNumber numberWithInt:0],@"tag3",[NSNumber numberWithInt:0],@"tag4",[NSNumber numberWithInt:0],@"tag5", nil];
            }else {
                ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent], nil];
            }
            
            if ([dicMQ count]==0) {
                NSMutableArray *arr = [NSMutableArray array];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }else {
                NSMutableArray *arr = [NSMutableArray array];
                [arr setArray:[dicMQ valueForKey:@"sdic"]];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }
        }
            break;
        case 4:
        {
            //[dictQue removeAllObjects];
//            tagSOrN = 0;
            NSMutableDictionary *dictQue = [NSMutableDictionary dictionary];
            NSMutableDictionary *d1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            NSMutableDictionary *d2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2],@"+",[NSNumber numberWithInt:0],@"attempt",[NSNumber numberWithInt:0],@"-", nil];
            [dictQue setObject:d1 forKey:@"opt1"];
            [dictQue setObject:d2 forKey:@"opt2"];
            
            NSMutableDictionary *ds = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",selectStudent],@"stud",dictQue,[NSString stringWithFormat:@"dic%d",selectStudent],[NSNumber numberWithInt:0],[NSString stringWithFormat:@"tagSOrN%d",selectStudent], nil];
            if ([dicMQ count]==0) {
                NSMutableArray *arr = [NSMutableArray array];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }else {
                NSMutableArray *arr = [NSMutableArray array];
                [arr setArray:[dicMQ valueForKey:@"sdic"]];
                [arr addObject:ds];
                dicMQ = [NSMutableDictionary dictionaryWithObject:arr forKey:@"sdic"];
            }
        }
            break;
            
        default:
            break;
    }
    //NSLog(@"dicMQ:%@",dicMQ);
}
-(void)clickSelectOption:(id)sender
{
    UIButton *btn = sender;
    selectedOption = btn.tag;
    switch (btn.tag) {
        case 1:
        {
            if ([self checkCorrectOrNotAnswer]) {
                if (tagReinSch == 1) {
                    cntCorrect1++;
                    if (cntCorrect1 == tagReinI) {
                        if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                            [self audioFeedback1];
                        }
                        cntCorrect1 = 0;
                    }
                }else if (tagReinSch == 2) {
                    if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                        [self audioFeedback1];
                    }                
                }

            }else {
                if (tagVPrompt == 1) {
                    if (selectStudent == 5 && tagSOrN5==0) {
                        btn.hidden = YES;
                        imgBG1.hidden=YES;
                    }else {
                        NSMutableArray *ars= [NSMutableArray array];
                        [ars setArray:[dicMQ valueForKey:@"sdic"]];
                        int tagSN=0;
                        for (int i=0; i<[ars count]; i++) {
                            if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                                tagSN = [[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]] intValue];
                            }
                        }
                        if (tagSN == 0) {
                            btn.hidden = YES;
                            imgBG1.hidden=YES;
                        }
                    }
                }
                if (tagAFB == 1) {
                    [self wrongSelectionReinforce];
                }
            }
        }
            break;
        case 2:
        {
            if ([self checkCorrectOrNotAnswer]) {
                if (tagReinSch == 1) {
                    cntCorrect1++;
                    if (cntCorrect1 == tagReinI) {
                        if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                            [self audioFeedback1];
                        }
                        cntCorrect1 = 0;
                    }
                    
                }else if (tagReinSch == 2) {
                    if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                        [self audioFeedback1];
                    }
                }
            }else {
                if (selectLevel == 4) {
                    selectedOption = 2;
                }else {
                    if (tagVPrompt == 1) {
                        if (selectStudent == 5 && tagSOrN5==0) {
                            btn.hidden = YES;
                            imgBG2.hidden=YES;
                        }else {
                            NSMutableArray *ars= [NSMutableArray array];
                            [ars setArray:[dicMQ valueForKey:@"sdic"]];
                            int tagSN=0;
                            for (int i=0; i<[ars count]; i++) {
                                if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                                    tagSN = [[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]] intValue];
                                }
                            }
                            if (tagSN == 0) {
                                btn.hidden = YES;
                                imgBG2.hidden=YES;
                            }
                        }
                    }
                }
                if (tagAFB == 1) {
                    [self wrongSelectionReinforce];
                }
            }      
        }
            break;
        case 3:
        {
            if ([self checkCorrectOrNotAnswer]) {
                if (tagReinSch == 1) {
                    cntCorrect1++;
                    if (cntCorrect1 == tagReinI) {
                        if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                            [self audioFeedback1];
                        }
                        cntCorrect1 = 0;
                    }
                    
                }else if (tagReinSch == 2) {
                    if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                        [self audioFeedback1];
                    }
                }
            }else {
                if (tagVPrompt == 1) {
                    if (selectStudent == 5 && tagSOrN5==0) {
                        btn.hidden = YES;
                        imgBG3.hidden=YES;
                    }else {
                        NSMutableArray *ars= [NSMutableArray array];
                        [ars setArray:[dicMQ valueForKey:@"sdic"]];
                        int tagSN=0;
                        for (int i=0; i<[ars count]; i++) {
                            if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                                tagSN = [[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]] intValue];
                            }
                        }
                        if (tagSN == 0) {
                            btn.hidden = YES;
                            imgBG3.hidden=YES;
                        }
                    }
                }
                if (tagAFB == 1) {
                    [self wrongSelectionReinforce];
                }
            }    
        }
            break;            
        case 4:
        {
            if ([self checkCorrectOrNotAnswer]) {
                if (tagReinSch == 1) {
                    cntCorrect1++;
                    if (cntCorrect1 == tagReinI) {
                        if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                            [self audioFeedback1];
                        }
                        cntCorrect1 = 0;
                    }
                    
                }else if (tagReinSch == 2) {
                    if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                        [self audioFeedback1];
                    }                    
                }
            }else {
                if (tagVPrompt == 1) {
                    if (selectStudent == 5 && tagSOrN5==0) {
                        btn.hidden = YES;
                        imgBG4.hidden=YES;
                    }else {
                        NSMutableArray *ars= [NSMutableArray array];
                        [ars setArray:[dicMQ valueForKey:@"sdic"]];
                        int tagSN=0;
                        for (int i=0; i<[ars count]; i++) {
                            if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                                tagSN = [[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]] intValue];
                            }
                        }
                        if (tagSN == 0) {
                            btn.hidden = YES;
                            imgBG4.hidden=YES;
                        }
                    }
                }
                if (tagAFB == 1) {
                    [self wrongSelectionReinforce];
                }
            }
        }
            break;
        case 5:
        {
            if ([self checkCorrectOrNotAnswer]) {
                if (tagReinSch == 1) {
                    cntCorrect1++;
                    if (cntCorrect1 == tagReinI) {
                        if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                            [self audioFeedback1];
                        }
                        cntCorrect1 = 0;
                    }
                    
                }else if (tagReinSch == 2) {
                    if (tagReinType == 1 || tagReinType == 2 || tagReinType == 3) {
                        [self audioFeedback1];
                    }                
                }
            }else {
                if (tagVPrompt == 1) {
                    if (selectStudent == 5 && tagSOrN5==0) {
                        btn.hidden = YES;
                        imgBG5.hidden=YES;
                    }else {
                        NSMutableArray *ars= [NSMutableArray array];
                        [ars setArray:[dicMQ valueForKey:@"sdic"]];
                        int tagSN=0;
                        for (int i=0; i<[ars count]; i++) {
                            if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                                tagSN = [[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]] intValue];
                            }
                        }
                        if (tagSN == 0) {
                            btn.hidden = YES;
                            imgBG5.hidden=YES;
                        }
                    }
                }
                if (tagAFB == 1) {
                    [self wrongSelectionReinforce];
                }
            }
        }
            break;            
        default:
            break;
    }
}
-(NSString*)checkForCustomOrModify:(NSString*)strField:(NSMutableDictionary*)dc
{
    NSString *strReqF=@"";
    if ([[dc valueForKey:@"id"] intValue]>260) {
        if ([strField isEqualToString:@"Ans1Img"]) {
            strReqF = [dc valueForKey:@"Ans1Img"];
        }else if ([strField isEqualToString:@"Ans2Img"]) {
            strReqF = [dc valueForKey:@"Ans2Img"];
        }else if ([strField isEqualToString:@"Ans3Img"]) {
            strReqF = [dc valueForKey:@"Ans3Img"];
        }else if ([strField isEqualToString:@"Ans4Img"]) {
            strReqF = [dc valueForKey:@"Ans4Img"];
        }else if ([strField isEqualToString:@"Ans5Img"]) {
            strReqF = [dc valueForKey:@"Ans5Img"];
        }else if ([strField isEqualToString:@"QText"]) {
            strReqF = [dc valueForKey:@"QText"];
        }else if ([strField isEqualToString:@"Modify"]) {
            strReqF = [dc valueForKey:@"Modify"];
        }else if ([strField isEqualToString:@"CorrectAns"]) {
            strReqF = [dc valueForKey:@"CorrectAns"];
        }else if ([strField isEqualToString:@"AudioRein"]) {
            strReqF = [dc valueForKey:@"AudioRein"];
        }else if ([strField isEqualToString:@"QAFeedback"]) {
            strReqF = [dc valueForKey:@"QAFeedback"];
        }else if ([strField isEqualToString:@"QAudio"]) {
            strReqF = [dc valueForKey:@"QAudio"];
        }else if ([strField isEqualToString:@"VisualRein"]) {
            strReqF = [dc valueForKey:@"VisualRein"];
        }else if ([strField isEqualToString:@"QCat"]) {
            strReqF = [dc valueForKey:@"QCat"];
        }else if ([strField isEqualToString:@"QType"]) {
            strReqF = [dc valueForKey:@"QType"];
        }      
    }else {
        if  ([[dc valueForKey:@"tagM"] isKindOfClass:[NSMutableArray class]]) {
            //NSLog(@"dc:%@",[dc valueForKey:@"tagM"]);
            NSMutableArray *arrTM = [NSMutableArray array];
            [arrTM setArray:[dc valueForKey:@"tagM"]];
            if ([[arrTM valueForKey:@"StudID"] containsObject:[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"]] && [[arrTM valueForKey:@"id"] containsObject:[dc valueForKey:@"id"]]) {
                NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[[arrTM objectAtIndex:0] valueForKey:@"dic"]];
                NSLog(@"d:%@",d);
                if ([strField isEqualToString:@"Ans1Img"]) {
                    strReqF = [d valueForKey:@"Ans1Img"];
                }else if ([strField isEqualToString:@"Ans2Img"]) {
                    strReqF = [d valueForKey:@"Ans2Img"];
                }else if ([strField isEqualToString:@"Ans3Img"]) {
                    strReqF = [d valueForKey:@"Ans3Img"];
                }else if ([strField isEqualToString:@"Ans4Img"]) {
                    strReqF = [d valueForKey:@"Ans4Img"];
                }else if ([strField isEqualToString:@"Ans5Img"]) {
                    strReqF = [d valueForKey:@"Ans5Img"];
                }else if ([strField isEqualToString:@"QText"]) {
                    strReqF = [d valueForKey:@"QText"];
                }else if ([strField isEqualToString:@"Modify"]) {
                    strReqF = [d valueForKey:@"Modify"];
                }else if ([strField isEqualToString:@"CorrectAns"]) {
                    strReqF = [d valueForKey:@"CorrectAns"];
                }
            }
        }else {
            if ([strField isEqualToString:@"Ans1Img"]) {
                strReqF = [dc valueForKey:@"Ans1Img"];
            }else if ([strField isEqualToString:@"Ans2Img"]) {
                strReqF = [dc valueForKey:@"Ans2Img"];
            }else if ([strField isEqualToString:@"Ans3Img"]) {
                strReqF = [dc valueForKey:@"Ans3Img"];
            }else if ([strField isEqualToString:@"Ans4Img"]) {
                strReqF = [dc valueForKey:@"Ans4Img"];
            }else if ([strField isEqualToString:@"Ans5Img"]) {
                strReqF = [dc valueForKey:@"Ans5Img"];
            }else if ([strField isEqualToString:@"QText"]) {
                strReqF = [dc valueForKey:@"QText"];
            }else if ([strField isEqualToString:@"Modify"]) {
                strReqF = [dc valueForKey:@"Modify"];
            }else if ([strField isEqualToString:@"CorrectAns"]) {
                strReqF = [dc valueForKey:@"CorrectAns"];
            }        
        }
        if ([strField isEqualToString:@"AudioRein"]) {
            strReqF = [dc valueForKey:@"AudioRein"];
        }else if ([strField isEqualToString:@"QAFeedback"]) {
            strReqF = [dc valueForKey:@"QAFeedback"];
        }else if ([strField isEqualToString:@"QAudio"]) {
            strReqF = [dc valueForKey:@"QAudio"];
        }else if ([strField isEqualToString:@"VisualRein"]) {
            strReqF = [dc valueForKey:@"VisualRein"];
        }else if ([strField isEqualToString:@"QCat"]) {
            strReqF = [dc valueForKey:@"QCat"];
        }else if ([strField isEqualToString:@"QType"]) {
            strReqF = [dc valueForKey:@"QType"];
        }
    }
    return strReqF;
}
-(void)setNilHighLighedImage
{
    int op = [self checkForCorrectAnswer];
    UIButton *btn = (UIButton*)[self.view viewWithTag:op];
    [btn setImage:nil forState:UIControlStateHighlighted];
}
-(IBAction)chooseStudent:(id)sender
{
    if (selectStudent == 0) {
        int noOT = 0;
        int vOrNot;
        NSString *strCats=@"";
        NSString *strCT=@"";
        if (tagView == 17) {
            selectLevel = 2;
            vOrNot = 1;
            noOT= 0;
            tagReinType = 3;
            tagReinSch = 2; 
            tagVPrompt = 2;
            tagSV = 1;
            tagAFB = 1;//
            strCats = @"School";
            strCT = @"all";
            UIButton *btnA = (UIButton*)[self.view viewWithTag:21];
            btnA.hidden = NO;
        }else {
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblGroup where GroupID='1'"]]];
                selectLevel = [[[[[dict valueForKey:@"Table1"] valueForKey:@"Level"] componentsSeparatedByString:@" "] objectAtIndex:1] intValue];
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"VisualTrials"] isEqualToString:@"ON"]) {
                    vOrNot = 2;
                    noOT = 10;
                }else {
                    vOrNot = 1;
                    noOT = 0;
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] isEqualToString:@"None"]) {
                    tagReinSch = 3;
                    tagReinType = 4;
                }else {
                    if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] isEqualToString:@"Auditory"]){
                        tagReinType = 1;
                    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] isEqualToString:@"Visual"]){
                        tagReinType = 2;
                    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] isEqualToString:@"Both"]){
                        tagReinType = 3;
                    }
                    if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinSchedule"] isEqualToString:@"Intermittent"]) {
                        tagReinSch = 1;
                        if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinInterval"] isEqualToString:@"3"]) {
                            tagReinI = 3;
                        }else {
                            tagReinI = 2;
                        }
                    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"ReinSchedule"] isEqualToString:@"Continuous"]) {
                        tagReinSch = 2;
                    }
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"VPrompt"] isEqualToString:@"ON"]) {
                    tagVPrompt = 1;
                }else {
                    tagVPrompt = 2;
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"SpokenVoice"] isEqualToString:@"ON"]) {
                    tagSV = 1;
                    UIButton *btnA = (UIButton*)[self.view viewWithTag:21];
                    btnA.hidden = NO;
                }else {
                    tagSV = 2;
                    UIButton *btnA = (UIButton*)[self.view viewWithTag:21];
                    btnA.hidden = YES;
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"AFeedback"] isEqualToString:@"ON"]) {
                    tagAFB = 1;
                }else {
                    tagAFB = 2;
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"QueCat"] isEqualToString:@"all"]) {
                    strCats = @"all";
                }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"QueCat"] isEqualToString:@""]){
                    strCats = @"all";
                }else {
                    strCats = [[dict valueForKey:@"Table1"] valueForKey:@"QueCat"];
                }
                if ([[[dict valueForKey:@"Table1"] valueForKey:@"QueType"] isEqualToString:@"all"]) {
                    strCT = @"all";
                }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"QueType"] isEqualToString:@""]){
                    strCT = @"all";
                }else {
                    strCT = [[dict valueForKey:@"Table1"] valueForKey:@"QueType"];
                }
            }else{
                selectLevel = [[[[[arrStud objectAtIndex:0] valueForKey:@"Level"] componentsSeparatedByString:@" "] objectAtIndex:1] intValue];
                if ([[[arrStud objectAtIndex:0] valueForKey:@"VisualTrials"] isEqualToString:@"ON"]) {
                    vOrNot = 2;
                    noOT = [[[arrStud objectAtIndex:0] valueForKey:@"NoOfTrials"] intValue];
                }else {
                    vOrNot = 1;
                    noOT = 0;
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinType"] isEqualToString:@"None"]) {
                    tagReinSch = 3;
                    tagReinType = 4;
                }else {
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinType"] isEqualToString:@"Auditory"]){
                        tagReinType = 1;
                    }else if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinType"] isEqualToString:@"Visual"]){
                        tagReinType = 2;
                    }else if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinType"] isEqualToString:@"Both"]){
                        tagReinType = 3;
                    }
                    if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinSchedule"] isEqualToString:@"Intermittent"]) {
                        tagReinSch = 1;
                        if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinInterval"] isEqualToString:@"3"]) {
                            tagReinI = 3;
                        }else {
                            tagReinI = 2;
                        }
                    }else if ([[[arrStud objectAtIndex:0] valueForKey:@"ReinSchedule"] isEqualToString:@"Continuous"]) {
                        tagReinSch = 2;
                    }
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"VPrompt"] isEqualToString:@"ON"]) {
                    tagVPrompt = 1;
                }else {
                    tagVPrompt = 2;
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"SpokenVoice"] isEqualToString:@"ON"]) {
                    tagSV = 1;
                    UIButton *btnA = (UIButton*)[self.view viewWithTag:21];
                    btnA.hidden = NO;
                }else {
                    tagSV = 2;
                    UIButton *btnA = (UIButton*)[self.view viewWithTag:21];
                    btnA.hidden = YES;
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"AFeedback"] isEqualToString:@"ON"]) {
                    tagAFB = 1;
                }else {
                    tagAFB = 2;
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"QueCat"] isEqualToString:@"all"]) {
                    strCats = @"all";
                }else if ([[[arrStud objectAtIndex:0] valueForKey:@"QueCat"] isEqualToString:@""]){
                    strCats = @"all";
                }else {
                    strCats = [[arrStud objectAtIndex:0] valueForKey:@"QueCat"];
                }
                if ([[[arrStud objectAtIndex:0] valueForKey:@"QueType"] isEqualToString:@"all"]) {
                    strCT = @"all";
                }else if ([[[arrStud objectAtIndex:0] valueForKey:@"QueType"] isEqualToString:@""]){
                    strCT = @"all";
                }else {
                    strCT = [[arrStud objectAtIndex:0] valueForKey:@"QueType"];
                }
            }        
        }
        switch (selectLevel) {
            case 1:
            {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:[self getSelectQue:strCats :strCT:1:0]];
                NSMutableArray *arr1 = [NSMutableArray array];
                for (int i=0; i<[arrStud count]; i++) {
                    NSMutableArray *ar = [NSMutableArray array];
                    [ar setArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"Level='%d' and StudID='%@'",selectLevel,[[arrStud objectAtIndex:i] valueForKey:@"StudID"]] withColumnValue:@""]];
                    if ([ar count]>0) {
                        for (int j=0; j<[arr count]; j++) {
                            NSMutableDictionary *d = [arr objectAtIndex:j];
                            if ([[ar valueForKey:@"id"] containsObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]]) {
                                int indA2D = [[ar valueForKey:@"id"] indexOfObject:[d valueForKey:@"id"]];
                                
                                NSMutableDictionary *di = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[ar objectAtIndex:indA2D] valueForKey:@"Ans1Img"],@"Ans1Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans2Img"],@"Ans2Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans3Img"],@"Ans3Img",[[ar objectAtIndex:indA2D] valueForKey:@"QText"],@"QText",[[ar objectAtIndex:indA2D] valueForKey:@"Modify"],@"Modify",[[ar objectAtIndex:indA2D] valueForKey:@"CorrectAns"],@"CorrectAns", nil];
                                NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrStud objectAtIndex:i] valueForKey:@"StudID"],@"StudID",[[ar objectAtIndex:indA2D] valueForKey:@"id"],@"id",di,@"dic", nil];
                                if ([[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"] isKindOfClass:[NSMutableArray class]]) {
                                    NSMutableArray*arrA = [NSMutableArray arrayWithArray:[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"]];
                                    [arrA addObject:dM];
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:arrA forKey:@"tagM"];
                                }else {
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[NSMutableArray arrayWithObject:dM] forKey:@"tagM"];
                                }
                            }
                        }                 
                    }
                    //.....
                    NSMutableArray *arrr = [NSMutableArray array];
                    [arrr setArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                    for (int j=0; j<[arrr count]; j++) {
                        NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrr objectAtIndex:j] valueForKey:@"Level"],@"Level",[[arrr objectAtIndex:j] valueForKey:@"Ans1Img"],@"Ans1Img",[[arrr objectAtIndex:j] valueForKey:@"Ans2Img"],@"Ans2Img",[[arrr objectAtIndex:j] valueForKey:@"Ans3Img"],@"Ans3Img",[[arrr objectAtIndex:j] valueForKey:@"Ans4Img"],@"Ans4Img",[[arrr objectAtIndex:j] valueForKey:@"Ans5Img"],@"Ans5Img",[[arrr objectAtIndex:j] valueForKey:@"AudioRein"],@"AudioRein",[[arrr objectAtIndex:j] valueForKey:@"CorrectAns"],@"CorrectAns",[[arrr objectAtIndex:j] valueForKey:@"Modify"],@"Modify",[[arrr objectAtIndex:j] valueForKey:@"QAFeedback"],@"QAFeedback",[[arrr objectAtIndex:j] valueForKey:@"QAudio"],@"QAudio",[[arrr objectAtIndex:j] valueForKey:@"QCat"],@"QCat",[[arrr objectAtIndex:j] valueForKey:@"QText"],@"QText",[[arrr objectAtIndex:j] valueForKey:@"QType"],@"QType",[[arrr objectAtIndex:j] valueForKey:@"VisualRein"],@"VisualRein", nil];
                        if ([arr1 containsObject:dM]) {
                            
                        }else {
                            [arr1 addObject:dM];
                        }
                    }
                }
                NSLog(@"arr1:%@",arr1);
                for (int i=0; i<[arr1 count]; i++) {
                    [[arr1 objectAtIndex:i] setObject:[NSNumber numberWithInt:261+i] forKey:@"id"];
                    [arr addObject:[arr1 objectAtIndex:i]];
                }
                //NSLog(@"arr:%@",arr);
                if (noOT==5 || noOT==10) {
                    if ([arr count]<noOT) {
                        noOT = [arr count];   
                    }
                }else {
                    noOT = [arr count];
                }
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                    
                }else{
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                }
            }
                break;
            case 2:
            {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:[self getSelectQue:strCats :strCT:1:0]];
                NSMutableArray *arr1 = [NSMutableArray array];
//                NSMutableArray *arr1 = [NSMutableArray array];
                for (int i=0; i<[arrStud count]; i++) {
                    NSMutableArray *ar = [NSMutableArray array];
                    [ar setArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"Level='%d' and StudID='%@'",selectLevel,[[arrStud objectAtIndex:i] valueForKey:@"StudID"]] withColumnValue:@""]];
                    if ([ar count]>0) {
                        for (int j=0; j<[arr count]; j++) {
                            NSMutableDictionary *d = [arr objectAtIndex:j];
                            if ([[ar valueForKey:@"id"] containsObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]]) {
                                int indA2D = [[ar valueForKey:@"id"] indexOfObject:[d valueForKey:@"id"]];
                                
                                NSMutableDictionary *di = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[ar objectAtIndex:indA2D] valueForKey:@"Ans1Img"],@"Ans1Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans2Img"],@"Ans2Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans3Img"],@"Ans3Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans4Img"],@"Ans4Img",[[ar objectAtIndex:indA2D] valueForKey:@"QText"],@"QText",[[ar objectAtIndex:indA2D] valueForKey:@"Modify"],@"Modify",[[ar objectAtIndex:indA2D] valueForKey:@"CorrectAns"],@"CorrectAns", nil];
                                NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrStud objectAtIndex:i] valueForKey:@"StudID"],@"StudID",[[ar objectAtIndex:indA2D] valueForKey:@"id"],@"id",di,@"dic", nil];
                                if ([[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"] isKindOfClass:[NSMutableArray class]]) {
                                    NSMutableArray*arrA = [NSMutableArray arrayWithArray:[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"]];
                                    [arrA addObject:dM];
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:arrA forKey:@"tagM"];
                                }else {
                                    
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[NSMutableArray arrayWithObject:dM] forKey:@"tagM"];
                                }
                                
                            }
                        }                 
                    }
                    //.....
                    NSMutableArray *arrr = [NSMutableArray array];
                    [arrr setArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                    for (int j=0; j<[arrr count]; j++) {
                        NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrr objectAtIndex:j] valueForKey:@"Level"],@"Level",[[arrr objectAtIndex:j] valueForKey:@"Ans1Img"],@"Ans1Img",[[arrr objectAtIndex:j] valueForKey:@"Ans2Img"],@"Ans2Img",[[arrr objectAtIndex:j] valueForKey:@"Ans3Img"],@"Ans3Img",[[arrr objectAtIndex:j] valueForKey:@"Ans4Img"],@"Ans4Img",[[arrr objectAtIndex:j] valueForKey:@"Ans5Img"],@"Ans5Img",[[arrr objectAtIndex:j] valueForKey:@"AudioRein"],@"AudioRein",[[arrr objectAtIndex:j] valueForKey:@"CorrectAns"],@"CorrectAns",[[arrr objectAtIndex:j] valueForKey:@"Modify"],@"Modify",[[arrr objectAtIndex:j] valueForKey:@"QAFeedback"],@"QAFeedback",[[arrr objectAtIndex:j] valueForKey:@"QAudio"],@"QAudio",[[arrr objectAtIndex:j] valueForKey:@"QCat"],@"QCat",[[arrr objectAtIndex:j] valueForKey:@"QText"],@"QText",[[arrr objectAtIndex:j] valueForKey:@"QType"],@"QType",[[arrr objectAtIndex:j] valueForKey:@"VisualRein"],@"VisualRein", nil];
                        if ([arr1 containsObject:dM]) {
                            
                        }else {
                            [arr1 addObject:dM];
                        }
                    }
                }
                /*
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"]==2 || [arrStud count]==0){
                    
                }else {
                    [arr2 addObjectsFromArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"Level='%d' and StudID='%@'",selectLevel,[[arrStud objectAtIndex:0] valueForKey:@"StudID"]] withColumnValue:@""]];
                    if ([arr2 count]>0) {
                        for (int i=0; i<[arr count]; i++) {
                            NSMutableDictionary *d = [arr objectAtIndex:i];
                            if ([[arr2 valueForKey:@"id"] containsObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]]) {
                                int indA2D = [[arr2 valueForKey:@"id"] indexOfObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans1Img"] forKey:@"Ans1Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans2Img"] forKey:@"Ans2Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans3Img"] forKey:@"Ans3Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans4Img"] forKey:@"Ans4Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"QText"] forKey:@"QText"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Modify"] forKey:@"Modify"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"CorrectAns"] forKey:@"CorrectAns"];
                                NSLog(@"arrMatch:%@",[arr objectAtIndex:[arr indexOfObject:d]]);
                            }
                        }                  
                    }
                }
                for (int i=0; i<[arrStud count]; i++) {
                    [arr1 addObjectsFromArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                }*/
                for (int i=0; i<[arr1 count]; i++) {
                    [[arr1 objectAtIndex:i] setObject:[NSNumber numberWithInt:261+i] forKey:@"id"];
                    [arr addObject:[arr1 objectAtIndex:i]];
                }
                //NSLog(@"arr11:%@",arr1);
                //NSLog(@"arr:%@",arr);
                if (noOT==5 || noOT==10) {
                    if ([arr count]<noOT) {
                        noOT = [arr count];   
                    }
                }else {
                    noOT = [arr count];
                }
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                    
                }else{
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                }        
            }
                break;
            case 3:
            {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:[self getSelectQue:strCats :strCT:1:0]];
                NSMutableArray *arr1 = [NSMutableArray array];
                for (int i=0; i<[arrStud count]; i++) {
                    NSMutableArray *ar = [NSMutableArray array];
                    [ar setArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"Level='%d' and StudID='%@'",selectLevel,[[arrStud objectAtIndex:i] valueForKey:@"StudID"]] withColumnValue:@""]];
                    if ([ar count]>0) {
                        for (int j=0; j<[arr count]; j++) {
                            NSMutableDictionary *d = [arr objectAtIndex:j];
                            if ([[ar valueForKey:@"id"] containsObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]]) {
                                int indA2D = [[ar valueForKey:@"id"] indexOfObject:[d valueForKey:@"id"]];
                                
                                NSMutableDictionary *di = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[ar objectAtIndex:indA2D] valueForKey:@"Ans1Img"],@"Ans1Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans2Img"],@"Ans2Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans3Img"],@"Ans3Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans4Img"],@"Ans4Img",[[ar objectAtIndex:indA2D] valueForKey:@"Ans5Img"],@"Ans5Img",[[ar objectAtIndex:indA2D] valueForKey:@"QText"],@"QText",[[ar objectAtIndex:indA2D] valueForKey:@"Modify"],@"Modify",[[ar objectAtIndex:indA2D] valueForKey:@"CorrectAns"],@"CorrectAns", nil];
                                NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrStud objectAtIndex:i] valueForKey:@"StudID"],@"StudID",[[ar objectAtIndex:indA2D] valueForKey:@"id"],@"id",di,@"dic", nil];
                                if ([[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"] isKindOfClass:[NSMutableArray class]]) {
                                    NSMutableArray*arrA = [NSMutableArray arrayWithArray:[[arr objectAtIndex:[arr indexOfObject:d]] valueForKey:@"tagM"]];
                                    [arrA addObject:dM];
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:arrA forKey:@"tagM"];
                                }else {
                                    
                                    [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[NSMutableArray arrayWithObject:dM] forKey:@"tagM"];
                                }
                                
                            }
                        }                 
                    }
                    //.....
                    NSMutableArray *arrr = [NSMutableArray array];
                    [arrr setArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                    for (int j=0; j<[arrr count]; j++) {
                        NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrr objectAtIndex:j] valueForKey:@"Level"],@"Level",[[arrr objectAtIndex:j] valueForKey:@"Ans1Img"],@"Ans1Img",[[arrr objectAtIndex:j] valueForKey:@"Ans2Img"],@"Ans2Img",[[arrr objectAtIndex:j] valueForKey:@"Ans3Img"],@"Ans3Img",[[arrr objectAtIndex:j] valueForKey:@"Ans4Img"],@"Ans4Img",[[arrr objectAtIndex:j] valueForKey:@"Ans5Img"],@"Ans5Img",[[arrr objectAtIndex:j] valueForKey:@"AudioRein"],@"AudioRein",[[arrr objectAtIndex:j] valueForKey:@"CorrectAns"],@"CorrectAns",[[arrr objectAtIndex:j] valueForKey:@"Modify"],@"Modify",[[arrr objectAtIndex:j] valueForKey:@"QAFeedback"],@"QAFeedback",[[arrr objectAtIndex:j] valueForKey:@"QAudio"],@"QAudio",[[arrr objectAtIndex:j] valueForKey:@"QCat"],@"QCat",[[arrr objectAtIndex:j] valueForKey:@"QText"],@"QText",[[arrr objectAtIndex:j] valueForKey:@"QType"],@"QType",[[arrr objectAtIndex:j] valueForKey:@"VisualRein"],@"VisualRein", nil];
                        if ([arr1 containsObject:dM]) {
                            
                        }else {
                            [arr1 addObject:dM];
                        }
                    }
                }
                /*
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"]==2 || [arrStud count]==0){
                    
                }else {
                    [arr2 addObjectsFromArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"Level='%d' and StudID='%@'",selectLevel,[[arrStud objectAtIndex:0] valueForKey:@"StudID"]] withColumnValue:@""]];
                    if ([arr2 count]>0) {
                        for (int i=0; i<[arr count]; i++) {
                            NSMutableDictionary *d = [arr objectAtIndex:i];
                            if ([[arr2 valueForKey:@"id"] containsObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]]) {
                                int indA2D = [[arr2 valueForKey:@"id"] indexOfObject:[NSNumber numberWithInt:[[d valueForKey:@"id"] intValue]]];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans1Img"] forKey:@"Ans1Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans2Img"] forKey:@"Ans2Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans3Img"] forKey:@"Ans3Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans4Img"] forKey:@"Ans4Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Ans5Img"] forKey:@"Ans5Img"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"QText"] forKey:@"QText"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"Modify"] forKey:@"Modify"];
                                [[arr objectAtIndex:[arr indexOfObject:d]] setObject:[[arr2 objectAtIndex:indA2D] valueForKey:@"CorrectAns"] forKey:@"CorrectAns"];
                                NSLog(@"arrMatch:%@",[arr objectAtIndex:[arr indexOfObject:d]]);
                            }
                        }
                    }
                }
                for (int i=0; i<[arrStud count]; i++) {
                    [arr1 addObjectsFromArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                }*/
                for (int i=0; i<[arr1 count]; i++) {
                    [[arr1 objectAtIndex:i] setObject:[NSNumber numberWithInt:261+i] forKey:@"id"];
                    [arr addObject:[arr1 objectAtIndex:i]];
                }
                //NSLog(@"arr11:%@",arr1);
                //NSLog(@"arr:%@",arr);
                if (noOT==5 || noOT==10) {
                    if ([arr count]<noOT) {
                        noOT = [arr count];   
                    }
                }else {
                    noOT = [arr count];
                }
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                    
                }else{
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                }           
            }
                break;
            case 4:
            {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:[self getSelectQue:strCats :strCT:1:0]];
                NSMutableArray *arr1 = [NSMutableArray array];
                for (int i=0; i<[arrStud count]; i++) {
                    NSMutableArray *arrr = [NSMutableArray array];
                    [arrr setArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                    for (int j=0; j<[arrr count]; j++) {
                        NSMutableDictionary *dM = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrr objectAtIndex:j] valueForKey:@"Level"],@"Level",[[arrr objectAtIndex:j] valueForKey:@"AudioRein"],@"AudioRein",[[arrr objectAtIndex:j] valueForKey:@"QAFeedback"],@"QAFeedback",[[arrr objectAtIndex:j] valueForKey:@"QAudio"],@"QAudio",[[arrr objectAtIndex:j] valueForKey:@"QCat"],@"QCat",[[arrr objectAtIndex:j] valueForKey:@"QText"],@"QText",[[arrr objectAtIndex:j] valueForKey:@"QType"],@"QType",[[arrr objectAtIndex:j] valueForKey:@"VisualRein"],@"VisualRein", nil];
                        if ([arr1 containsObject:dM]) {
                            
                        }else {
                            [arr1 addObject:dM];
                        }
                    }
                }
                /*
                for (int i=0; i<[arrStud count]; i++) {
                    [arr1 addObjectsFromArray:[self getSelectQue:strCats :strCT:2:[[[arrStud objectAtIndex:i] valueForKey:@"StudID"] intValue]]];
                }*/
                for (int i=0; i<[arr1 count]; i++) {
                    [[arr1 objectAtIndex:i] setObject:[NSNumber numberWithInt:261+i] forKey:@"id"];
                    [arr addObject:[arr1 objectAtIndex:i]];
                }
                //NSLog(@"arr11:%@",arr1);
                //NSLog(@"arr:%@",arr);
                if (noOT==5 || noOT==10) {
                    if ([arr count]<noOT) {
                        noOT = [arr count];   
                    }
                }else {
                    noOT = [arr count];
                }
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                }else{
                    arrQue = [NSMutableArray arrayWithArray:[self generateQue:arr :noOT]];
                }
            }
                break;
            default:
                break;
        }
        UIButton *btn = sender;
        if (btn == btnStudent1) {
            selectStudent = 1;
            btnStudent1.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
        }else if (btn == btnStudent2){
            selectStudent = 2;
            btnStudent2.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
        }else if (btn == btnStudent3){
            selectStudent = 3;
            btnStudent3.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
        }else if (btn == btnStudent4){
            selectStudent = 4;
            btnStudent4.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor blueColor].CGColor;
        }else if (btn.tag == 19){
            selectStudent = 5;
        }
        if (vOrNot == 1) {
            imgView1.hidden=YES;
            imgView2.hidden=YES;
            imgView3.hidden=YES;
            imgView4.hidden=YES;
            imgView5.hidden=YES;            
            imgView6.hidden=YES;
            imgView7.hidden=YES;
            imgView8.hidden=YES;
            imgView9.hidden=YES;
            imgView10.hidden=YES;
        }else {
            if ([arrQue count]==5) {
                imgView1.frame = CGRectMake(394, 20, 33, 23);
                imgView2.frame = CGRectMake(443, 20, 33, 23);
                imgView3.frame = CGRectMake(492, 20, 33, 23);
                imgView4.frame = CGRectMake(541, 20, 33, 23);
                imgView5.frame = CGRectMake(590, 20, 33, 23);
                imgView1.hidden=NO;
                imgView2.hidden=NO;
                imgView3.hidden=NO;
                imgView4.hidden=NO;
                imgView5.hidden=NO;
            }else if ([arrQue count]==10){
                imgView6.hidden=NO;
                imgView7.hidden=NO;
                imgView8.hidden=NO;
                imgView9.hidden=NO;
                imgView10.hidden=NO;
                imgView1.hidden=NO;
                imgView2.hidden=NO;
                imgView3.hidden=NO;
                imgView4.hidden=NO;
                imgView5.hidden=NO;
            }else {
                switch ([arrQue count]) {
                    case 1:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=YES;
                        imgView3.hidden=YES;
                        imgView4.hidden=YES;
                        imgView5.hidden=YES;            
                        imgView6.hidden=YES;
                        imgView7.hidden=YES;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                    
                    }
                        break;
                    case 2:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=YES;
                        imgView4.hidden=YES;
                        imgView5.hidden=YES;            
                        imgView6.hidden=YES;
                        imgView7.hidden=YES;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 3:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=YES;
                        imgView5.hidden=YES;            
                        imgView6.hidden=YES;
                        imgView7.hidden=YES;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 4:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=NO;
                        imgView5.hidden=YES;            
                        imgView6.hidden=YES;
                        imgView7.hidden=YES;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 6:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=NO;
                        imgView5.hidden=NO;            
                        imgView6.hidden=NO;
                        imgView7.hidden=YES;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 7:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=NO;
                        imgView5.hidden=NO;            
                        imgView6.hidden=NO;
                        imgView7.hidden=NO;
                        imgView8.hidden=YES;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 8:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=NO;
                        imgView5.hidden=NO;            
                        imgView6.hidden=NO;
                        imgView7.hidden=NO;
                        imgView8.hidden=NO;
                        imgView9.hidden=YES;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    case 9:
                    {
                        imgView1.hidden=NO;
                        imgView2.hidden=NO;
                        imgView3.hidden=NO;
                        imgView4.hidden=NO;
                        imgView5.hidden=NO;            
                        imgView6.hidden=NO;
                        imgView7.hidden=NO;
                        imgView8.hidden=NO;
                        imgView9.hidden=NO;
                        imgView10.hidden=YES;                        
                    }
                        break;
                    default:
                        break;
                }
            }

        }
        [self clickLoadQuestion:nil];
    }else {
        UIButton *btn = sender;
        if (btn == btnStudent1) {
            selectStudent = 1;
            btnStudent1.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
            [self loadQuestion];
        }else if (btn == btnStudent2){
            selectStudent = 2;
            btnStudent2.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
            [self loadQuestion];
            
        }else if (btn == btnStudent3){
            selectStudent = 3;
            btnStudent3.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor blueColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor clearColor].CGColor;
            [self loadQuestion];
        }else if (btn == btnStudent4){
            selectStudent = 4;
            btnStudent4.layer.borderWidth = 3;
            btnStudent1.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent2.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent3.layer.borderColor = [UIColor clearColor].CGColor;
            btnStudent4.layer.borderColor = [UIColor blueColor].CGColor;
            [self loadQuestion];
        }
    }

}
-(void)loadQuestion
{
    switch (selectLevel) {
        case 1:
        {
            NSMutableArray *arr = [NSMutableArray array];
            [arr setArray:[dicMQ valueForKey:@"sdic"]];
            int kM=0;
            int tag1=0;
            int tag2=0;
            int tag3=0;
            for (int i=0; i<[arr count]; i++) {
                if ([[[arr objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                    kM=1;
                    if (tagVPrompt == 1) {
                        tag1 = [[[arr objectAtIndex:i] valueForKey:@"tag1"] intValue];
                        tag2 = [[[arr objectAtIndex:i] valueForKey:@"tag2"] intValue];
                        tag3 = [[[arr objectAtIndex:i] valueForKey:@"tag3"] intValue];
                    }
                }
            }
            if (kM == 1) {
            }else {
                [self loadDict];
            }
            UIButton *btn = (UIButton*)[self.view viewWithTag:1];

            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];

            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
            NSLog(@"tagQue Dict:%d",tagQue);
            NSString *strMdf = [self checkForCustomOrModify:@"Modify" :[arrQue objectAtIndex:tagQue]];
            NSLog(@"strMdf:%@",strMdf);
            //17
            if ([strMdf isEqualToString:@""]) {
                if (tag1==0) {
                    btn.hidden=NO;
                }else {
                    btn.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                }else {
                    btn1.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                }else {
                    btn2.hidden=YES;
                }
                imgBG1.hidden=YES;
                imgBG2.hidden=YES;
                imgBG3.hidden=YES;
                btn.frame =CGRectMake(156, 226, 209, 204);
                //17
                NSString *strImg = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                [btn setImage:[UIImage imageNamed:strImg] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:strImg] forState:UIControlStateHighlighted];
                strImg=@"";
                strImg = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                btn1.frame =CGRectMake(408, 226, 209, 204);
                [btn1 setImage:[UIImage imageNamed:strImg] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:strImg] forState:UIControlStateHighlighted];
                strImg=@"";
                strImg = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                btn2.frame =CGRectMake(660, 226, 209, 204);
                [btn2 setImage:[UIImage imageNamed:strImg] forState:UIControlStateNormal];
                [btn2 setImage:[UIImage imageNamed:strImg] forState:UIControlStateHighlighted];
            }else {
                if (tag1==0) {
                    btn.hidden=NO;
                    imgBG1.hidden=NO;
                }else {
                    btn.hidden=YES;
                    imgBG1.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                    imgBG2.hidden=NO;
                }else {
                    btn1.hidden=YES;
                    imgBG2.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                    imgBG3.hidden=NO;
                }else {
                    btn2.hidden=YES;
                    imgBG3.hidden=YES;
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"1"]) { 
                    //imgBG1.hidden=NO;
                    imgBG1.frame =CGRectMake(156, 226, 209, 204);
                    btn.frame =CGRectMake(156+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG1.hidden=YES;
                    btn.frame =CGRectMake(156, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"1"]) {
                    //imgBG2.hidden=NO;
                    imgBG2.frame =CGRectMake(408, 226, 209, 204);
                    btn1.frame =CGRectMake(408+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG2.hidden=YES;
                    btn1.frame =CGRectMake(408, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                    
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"1"]) {
                    //imgBG3.hidden=NO;
                    imgBG3.frame =CGRectMake(660, 226, 209, 204);
                    btn2.frame =CGRectMake(660+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG3.hidden=YES;
                    btn2.frame =CGRectMake(660, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                
            }     
            imgBG4.hidden=YES;
            imgBG5.hidden=YES;
            [self.view viewWithTag:4].hidden=YES;
            [self.view viewWithTag:5].hidden=YES;
        }
            break;
        case 2:
        {
            int tag1=0;
            int tag2=0;
            int tag3=0;
            int tag4=0;
            if (selectStudent!=5) {
                NSMutableArray *arr = [NSMutableArray array];
                [arr setArray:[dicMQ valueForKey:@"sdic"]];
                int kM=0;
                for (int i=0; i<[arr count]; i++) {
                    if ([[[arr objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                        kM=1;
                        if (tagVPrompt == 1) {
                            tag1 = [[[arr objectAtIndex:i] valueForKey:@"tag1"] intValue];
                            tag2 = [[[arr objectAtIndex:i] valueForKey:@"tag2"] intValue];
                            tag3 = [[[arr objectAtIndex:i] valueForKey:@"tag3"] intValue];
                            tag4 = [[[arr objectAtIndex:i] valueForKey:@"tag4"] intValue];
                        }
                    }
                }
                if (kM == 1) {
                }else {
                    [self loadDict];
                }  
            }else {
                if ([dicMQ count]>0) {
                }else if ([dicMQ count]==0){
                    [self loadDict];
                }
            }
            UIButton *btn = (UIButton*)[self.view viewWithTag:1];
            btn.hidden=NO;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
            btn1.hidden=NO;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
            btn2.hidden=NO;
            UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
            btn3.hidden=NO;
            NSString *strMdf = [self checkForCustomOrModify:@"Modify" :[arrQue objectAtIndex:tagQue]];
            if ([strMdf isEqualToString:@""]) {
                if (tag1==0) {
                    btn.hidden=NO;
                }else {
                    btn.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                }else {
                    btn1.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                }else {
                    btn2.hidden=YES;
                }
                if (tag4==0) {
                    btn3.hidden=NO;
                }else {
                    btn3.hidden=YES;
                }
                imgBG1.hidden=YES;
                imgBG2.hidden=YES;
                imgBG3.hidden=YES;
                imgBG4.hidden=YES;
                btn.frame =CGRectMake(278, 226, 209, 204);
                btn1.frame =CGRectMake(531, 226, 209, 204);
                btn2.frame =CGRectMake(278, 443, 209, 204);
                btn3.frame =CGRectMake(531, 443, 209, 204);
                NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];                    
                [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
            }else {
                if (tag1==0) {
                    btn.hidden=NO;
                    imgBG1.hidden=NO;
                }else {
                    btn.hidden=YES;
                    imgBG1.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                    imgBG2.hidden=NO;
                }else {
                    btn1.hidden=YES;
                    imgBG2.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                    imgBG3.hidden=NO;
                }else {
                    btn2.hidden=YES;
                    imgBG3.hidden=YES;
                }
                if (tag4==0) {
                    btn3.hidden=NO;
                    imgBG4.hidden=NO;
                }else {
                    btn3.hidden=YES;
                    imgBG4.hidden=YES;
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"1"]) {
                    //imgBG1.hidden=NO;
                    imgBG1.frame =CGRectMake(278, 226, 209, 204);
                    btn.frame =CGRectMake(278+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG1.hidden=YES;
                    btn.frame =CGRectMake(278, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"1"]) {
                    //imgBG2.hidden=NO;
                    imgBG2.frame =CGRectMake(531, 226, 209, 204);
                    btn1.frame =CGRectMake(531+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG2.hidden=YES;
                    btn1.frame =CGRectMake(531, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                    
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"1"]) {
                    //imgBG3.hidden=NO;
                    imgBG3.frame =CGRectMake(278, 443, 209, 204);
                    btn2.frame =CGRectMake(278+14, 443+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG3.hidden=YES;
                    btn2.frame =CGRectMake(278, 443, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                    
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:3] isEqualToString:@"1"]) {
                    //imgBG4.hidden=NO;
                    imgBG4.frame =CGRectMake(531, 443, 209, 204);
                    btn3.frame =CGRectMake(531+14, 443+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                    [btn3 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn3 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG4.hidden=YES;
                    btn3.frame =CGRectMake(531, 443, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                    [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
            }
            imgBG5.hidden=YES;
            [self.view viewWithTag:5].hidden=YES; 
        }
            break;
        case 3:
        {
            NSMutableArray *arr = [NSMutableArray array];
            [arr setArray:[dicMQ valueForKey:@"sdic"]];
            int kM=0;
            int tag1=0;
            int tag2=0;
            int tag3=0;
            int tag4=0;
            int tag5=0;
            for (int i=0; i<[arr count]; i++) {
                if ([[[arr objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                    kM=1;
                    if (tagVPrompt == 1) {
                        tag1 = [[[arr objectAtIndex:i] valueForKey:@"tag1"] intValue];
                        tag2 = [[[arr objectAtIndex:i] valueForKey:@"tag2"] intValue];
                        tag3 = [[[arr objectAtIndex:i] valueForKey:@"tag3"] intValue];
                        tag4 = [[[arr objectAtIndex:i] valueForKey:@"tag4"] intValue];
                        tag5 = [[[arr objectAtIndex:i] valueForKey:@"tag5"] intValue];
                    }
                }
            }
            if (kM == 1) {
            }else {
                [self loadDict];
            }
            UIButton *btn = (UIButton*)[self.view viewWithTag:1];
            btn.hidden=NO;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
            btn1.hidden=NO;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
            btn2.hidden=NO;
            UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
            btn3.hidden=NO;
            UIButton *btn4 = (UIButton*)[self.view viewWithTag:5];
            btn4.hidden=NO; 
            NSString *strMdf = [self checkForCustomOrModify:@"Modify" :[arrQue objectAtIndex:tagQue]];
            if ([strMdf isEqualToString:@""]) {
                if (tag1==0) {
                    btn.hidden=NO;
                }else {
                    btn.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                }else {
                    btn1.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                }else {
                    btn2.hidden=YES;
                }
                if (tag4==0) {
                    btn3.hidden=NO;
                }else {
                    btn3.hidden=YES;
                }
                if (tag5==0) {
                    btn4.hidden=NO;
                }else {
                    btn4.hidden=YES;
                }
                imgBG1.hidden=YES;
                imgBG2.hidden=YES;
                imgBG3.hidden=YES;
                imgBG4.hidden=YES;
                imgBG5.hidden=YES;
                btn.frame =CGRectMake(156, 226, 209, 204);
                btn1.frame =CGRectMake(408, 226, 209, 204);
                btn2.frame =CGRectMake(660, 226, 209, 204);
                btn3.frame =CGRectMake(278, 443, 209, 204);
                btn4.frame =CGRectMake(531, 443, 209, 204);
                NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                strI=@"";
                strI = [self checkForCustomOrModify:@"Ans5Img" :[arrQue objectAtIndex:tagQue]];
                [btn4 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];                    
                [btn4 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
            }else {
                if (tag1==0) {
                    btn.hidden=NO;
                    imgBG1.hidden=NO;
                }else {
                    btn.hidden=YES;
                    imgBG1.hidden=YES;
                }
                if (tag2==0) {
                    btn1.hidden=NO;
                    imgBG2.hidden=NO;
                }else {
                    btn1.hidden=YES;
                    imgBG2.hidden=YES;
                }
                if (tag3==0) {
                    btn2.hidden=NO;
                    imgBG3.hidden=NO;
                }else {
                    btn2.hidden=YES;
                    imgBG3.hidden=YES;
                }
                if (tag4==0) {
                    btn3.hidden=NO;
                    imgBG4.hidden=NO;
                }else {
                    btn3.hidden=YES;
                    imgBG4.hidden=YES;
                }
                if (tag5==0) {
                    btn4.hidden=NO;
                    imgBG5.hidden=NO;
                }else {
                    btn4.hidden=YES;
                    imgBG5.hidden=YES;
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:0] isEqualToString:@"1"]) {
                    //imgBG1.hidden=NO;
                    imgBG1.frame =CGRectMake(156, 226, 209, 204);
                    btn.frame =CGRectMake(156+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG1.hidden=YES;
                    btn.frame =CGRectMake(156, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans1Img" :[arrQue objectAtIndex:tagQue]];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:1] isEqualToString:@"1"]) {
                    //imgBG2.hidden=NO;
                    imgBG2.frame =CGRectMake(408, 226, 209, 204);
                    btn1.frame =CGRectMake(408+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG2.hidden=YES;
                    btn1.frame =CGRectMake(408, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans2Img" :[arrQue objectAtIndex:tagQue]];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn1 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                    
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:2] isEqualToString:@"1"]) {
                    //imgBG3.hidden=NO;
                    imgBG3.frame =CGRectMake(660, 226, 209, 204);
                    btn2.frame =CGRectMake(660+14, 226+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG3.hidden=YES;
                    btn2.frame =CGRectMake(660, 226, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans3Img" :[arrQue objectAtIndex:tagQue]];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn2 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }                    
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:3] isEqualToString:@"1"]) {
                    //imgBG4.hidden=NO;
                    imgBG4.frame =CGRectMake(278, 443, 209, 204);
                    btn3.frame =CGRectMake(278+14, 443+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                    [btn3 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn3 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG4.hidden=YES;
                    btn3.frame =CGRectMake(278, 443, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans4Img" :[arrQue objectAtIndex:tagQue]];
                    [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn3 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
                if ([[[strMdf componentsSeparatedByString:@","] objectAtIndex:4] isEqualToString:@"1"]) {
                    //imgBG5.hidden=NO;
                    imgBG5.frame =CGRectMake(531, 443, 209, 204);
                    btn4.frame =CGRectMake(531+14, 443+13, 175, 176);
                    NSString *strI = [self checkForCustomOrModify:@"Ans5Img" :[arrQue objectAtIndex:tagQue]];
                    [btn4 setImage:[self loadImage:strI] forState:UIControlStateNormal];
                    [btn4 setImage:[self loadImage:strI] forState:UIControlStateHighlighted];
                }else {
                    imgBG5.hidden=YES;
                    btn4.frame =CGRectMake(531, 443, 209, 204);
                    NSString *strI = [self checkForCustomOrModify:@"Ans5Img" :[arrQue objectAtIndex:tagQue]];
                    [btn4 setImage:[UIImage imageNamed:strI] forState:UIControlStateNormal];
                    [btn4 setImage:[UIImage imageNamed:strI] forState:UIControlStateHighlighted];
                }
            }
        }
            break;
        case 4:
        {
            NSMutableArray *arr = [NSMutableArray array];
            [arr setArray:[dicMQ valueForKey:@"sdic"]];
            int kM=0;
            for (int i=0; i<[arr count]; i++) {
                if ([[[arr objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                    kM=1;
                }
            }
            if (kM == 1) {
            }else {
                [self loadDict];
            }
            UIButton *btn = (UIButton*)[self.view viewWithTag:1];
            btn.hidden=NO;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
            btn1.hidden=NO;
            imgBG1.hidden=YES;
            imgBG2.hidden=YES;
            imgBG3.hidden=YES;
            imgBG4.hidden=YES;
            imgBG5.hidden=YES;
            btn.frame =CGRectMake(101, 200, 368, 276);
            btn1.frame =CGRectMake(588, 200, 340, 258);
            [btn setImage:[UIImage imageNamed:@"BtnWellDone.png"] forState:UIControlStateNormal];
            [btn1 setImage:[UIImage imageNamed:@"BtnKeepTrying.png"] forState:UIControlStateNormal];
            [btn1 setImage:[UIImage imageNamed:@"BtnKeepTrying.png"] forState:UIControlStateHighlighted];
            [self.view viewWithTag:3].hidden=YES;
            [self.view viewWithTag:4].hidden=YES;
            [self.view viewWithTag:5].hidden=YES;
        }
            break;
            
        default:
            break;
    }
    if (selectLevel==4) {
        
    }else
    {
        [self setNilHighLighedImage];
    }
    
    NSString *strQ = [self checkForCustomOrModify:@"QText" :[arrQue objectAtIndex:tagQue]];
    lblQue.text = strQ;
}
-(void)showReinforce
{
    [self.view sendSubviewToBack:viewFinsh];
    viewFinsh.hidden = YES;
    if(tagSV == 1){
        NSLog(@"showReinforce:4");
        [self clickPlayOrStop:btnPOS];
    }

}
-(NSMutableArray*)generateQue:(NSMutableArray*)arrAllQue:(int)noOfQue
{
    NSMutableArray *arrQ = [NSMutableArray array];
    for (int i=0; i<noOfQue; i++) {
        int numberQue = [self generateRandomNumber:[arrAllQue count]];
        [arrRecordQueNo addObject:[NSNumber numberWithInt:numberQue]];   
    }
    for (int j=0; j<[arrRecordQueNo count]; j++) {
        int cnt = [[arrRecordQueNo objectAtIndex:j] intValue]-1;
        [arrQ addObject:[arrAllQue objectAtIndex:cnt]];
    }
    [arrRecordQueNo removeAllObjects];
    return arrQ;
}
-(NSMutableArray*)getSelectQue:(NSString*)catStr:(NSString*)typeStr :(int)tag:(int)nmStr
{
    NSMutableArray *arrTmp = [NSMutableArray array];
    if ([catStr isEqualToString:@"all"] && [typeStr isEqualToString:@"all"]) {
        if (tag==1) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",selectLevel]]];
        }else if (tag==2) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"tblCustom" withCondition:[NSString stringWithFormat:@"Level='Level %d' and StudID='%d'",selectLevel,nmStr] withColumnValue:@""]];
        }
        
        NSLog(@"1");
    }else if ([catStr isEqualToString:@"all"]) {
        //QType LIKE 'What'or QType LIKE 'who'
        NSArray *arr1 = [typeStr componentsSeparatedByString:@","];
        NSMutableArray *arr2 = [NSMutableArray array];
        for (int i=0; i<[arr1 count]; i++) {
            [arr2 addObject:[NSString stringWithFormat:@" QType LIKE '%@'",[arr1 objectAtIndex:i]]]
            ;            
        }
        NSString *strMK = [arr2 componentsJoinedByString:@"or"];
        if (tag==1) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",selectLevel] withCondition:strMK withColumnValue:@""]];
        }else if (tag==2) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"tblCustom" withCondition:[NSString stringWithFormat:@"(%@ ) and Level='Level %d' and StudID='%d'",strMK,selectLevel,nmStr] withColumnValue:@""]];
        }
        NSLog(@"2");
    }else if ([typeStr isEqualToString:@"all"]) {
        NSArray *arr1 = [catStr componentsSeparatedByString:@","];
        NSMutableArray *arr2 = [NSMutableArray array];
        for (int i=0; i<[arr1 count]; i++) {
            [arr2 addObject:[NSString stringWithFormat:@" QCat='%@'",[arr1 objectAtIndex:i]]]
            ;            
        }
        NSString *strMK = [arr2 componentsJoinedByString:@"or"];
        if (tag==1) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",selectLevel] withCondition:strMK withColumnValue:@""]];
        }else if (tag==2) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"tblCustom" withCondition:[NSString stringWithFormat:@"(%@ ) and Level='Level %d' and StudID='%d'",strMK,selectLevel,nmStr] withColumnValue:@""]];
        }
        NSLog(@"3");
    }else {
        NSArray *arr1 = [typeStr componentsSeparatedByString:@","];
        NSMutableArray *arr11 = [NSMutableArray array];
        for (int i=0; i<[arr1 count]; i++) {
            [arr11 addObject:[NSString stringWithFormat:@" QType LIKE '%@'",[arr1 objectAtIndex:i]]]
            ;            
        }
        NSString *strMK1 = [arr11 componentsJoinedByString:@"or"];
        NSArray *arr2 = [catStr componentsSeparatedByString:@","];
        NSMutableArray *arr22 = [NSMutableArray array];
        for (int i=0; i<[arr2 count]; i++) {
            [arr22 addObject:[NSString stringWithFormat:@" QCat='%@'",[arr2 objectAtIndex:i],selectLevel]]
            ;            
        }
        NSString *strMK2 = [arr22 componentsJoinedByString:@"or"];
        if (tag==1) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",selectLevel] withCondition:[NSString stringWithFormat:@"(%@ ) and (%@ )",strMK1,strMK2] withColumnValue:@""]];
        }else if (tag==2) {
            arrTmp = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"tblCustom" withCondition:[NSString stringWithFormat:@"(%@ ) and (%@ ) and Level='Level %d' and StudID='%d'",strMK1,strMK2,selectLevel,nmStr] withColumnValue:@""]];
        }
        //NSLog(@"4");
    }
//    NSLog(@"arrTmp:%d",[arrTmp count]);
    return arrTmp;
}
-(int)generateRandomNumber:(int)cntA
{
	int randomNo = (arc4random() % cntA)+1;
	if ([arrRecordQueNo count] != 0) {
		int i;
		for (i = 0; i <[arrRecordQueNo count]; i++) {
			int addedNo = [[arrRecordQueNo objectAtIndex:i] intValue];
			if (randomNo == addedNo) {
				int randomNoNew = [self generateRandomNumber:cntA];
				return randomNoNew;
			}
		}
	}else if ([arrRecordQueNo count] == 0) {
		return randomNo;
	}
	return randomNo;
}
-(void)clickPlayOrStop:(id)sender
{
    if (btnPOS.tag == 20) {
        [btnPOS setTitle:@"Play Audio" forState:UIControlStateNormal];
        btnPOS.tag = 21;
        if (audioPlayer.playing) {
            [audioPlayer stop];
        }
    }else if (btnPOS.tag == 21){
        if (tagQue == [arrQue count]){
            tagQue = [arrQue count]-1;
//            NSLog(@"tagQue == [arrQue count] tagQue1:%d",tagQue);
        }
        NSLog(@"tagQue1:%d",tagQue);
        NSString *strTrack = [self checkForCustomOrModify:@"QAudio" :[arrQue objectAtIndex:tagQue]];
//        NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"QAudio"];
        NSLog(@"TrackeName:%@",strTrack);
        NSError *error;
        NSString *strMdf = [self checkForCustomOrModify:@"Modify" :[arrQue objectAtIndex:tagQue]];
        if ([strMdf isEqualToString:@""]) {
            audioPlayer = [[AVAudioPlayer alloc] 
                           initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
        }else {
            if ([[[arrQue objectAtIndex:tagQue] valueForKey:@"id"] intValue]>260) {
                NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docsDir = [dirPaths objectAtIndex:0];
                NSString *imgM4aPath = [docsDir stringByAppendingPathComponent:strTrack];
                audioPlayer = [[AVAudioPlayer alloc] 
                               initWithData:[NSData dataWithContentsOfFile:imgM4aPath] error:&error];
            }else {
                audioPlayer = [[AVAudioPlayer alloc] 
                               initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
            } 
        }
       
        audioPlayer.delegate = self;
        
        if (error){ 
            NSLog(@"Error: %@", 
                  [error localizedDescription]);
        }else {
            btnPOS.tag = 20;
            [btnPOS setTitle:@"Stop Audio" forState:UIControlStateNormal];
            [audioPlayer play];
        }
    }
}
-(int)checkForCorrectAnswer
{   
    NSString *strCorAn = [self checkForCustomOrModify:@"CorrectAns" :[arrQue objectAtIndex:tagQue]];
    int opt;
    NSArray *arrSepAns = [[strCorAn stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@"&"];
        switch (selectLevel) {
            case 1:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    opt=1;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    opt=2;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    opt=3;
                }
            }
                break;
            case 2:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    opt=1;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    opt=2;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    opt=3;
                }
                else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                    opt=4;
                }
            }
                break;
            case 3:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    opt=1;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    opt=2;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    opt=3;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                    opt=4;
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"I"]) {
                    opt=5;
                }                
            }
                break;
            case 4:
            {
            
            }
                break;
                
            default:
                break;
        }
    return opt;
}
-(BOOL)checkCorrectOrNotAnswer
{   
    NSLog(@"tagQue CFCON:%d",tagQue);
    NSString *strCorAn = [self checkForCustomOrModify:@"CorrectAns" :[arrQue objectAtIndex:tagQue]];
    NSArray *arrSepAns = [[strCorAn stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@"&"];
//    if (tagVPrompt == 1) {
        switch (selectLevel) {
            case 1:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    if (selectedOption == 1) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    if (selectedOption == 2) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    if (selectedOption == 3) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                    
                }
            }
                break;
            case 2:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    if (selectedOption == 1) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    if (selectedOption == 2) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    if (selectedOption == 3) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                    
                }
                else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                    if (selectedOption == 4) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                    
                }
            }
                break;
            case 3:
            {
                if ([[arrSepAns objectAtIndex:0] isEqualToString:@"E"]) {
                    if (selectedOption == 1) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"F"]) {
                    if (selectedOption == 2) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"G"]) {
                    if (selectedOption == 3) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    } else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                   
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"H"]) {
                    if (selectedOption == 4) {
                        [self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                    
                }else if ([[arrSepAns objectAtIndex:0] isEqualToString:@"I"]) {
                    if (selectedOption == 5) {[self manageAttemptCount:selectedOption :YES];
                        return YES;
                    }else {
                        [self manageAttemptCount:selectedOption :NO];
                        return NO;
                    }                    
                }                
            }
                break;
            case 4:
            {
                if (selectedOption == 1) {
                    [self manageAttemptCount:selectedOption :YES];
                    return YES;
                }else if (selectedOption == 2) {
                    [self manageAttemptCount:selectedOption :NO];
                    return NO;
                }
            }
                break;
                
            default:
                break;
        }
//    }
    /*else if (tagVPrompt == 2){
        switch (selectLevel) {
            case 1:
            {
                if ([[arrSepAns objectAtIndex:1] isEqualToString:@"J"]) {
                    if (selectedOption == 1) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"K"]) {
                    if (selectedOption == 2) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"L"]) {
                    if (selectedOption == 3) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }
            }
                break;
            case 2:
            {
                if ([[arrSepAns objectAtIndex:1] isEqualToString:@"K"]) {
                    if (selectedOption == 1) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"L"]) {
                    if (selectedOption == 2) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"M"]) {
                    if (selectedOption == 3) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }
                else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"N"]) {
                    if (selectedOption == 4) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }
            }
                break;
            case 3:
            {
                if ([[arrSepAns objectAtIndex:1] isEqualToString:@"L"]) {
                    if (selectedOption == 1) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"M"]) {
                    if (selectedOption == 2) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"N"]) {
                    if (selectedOption == 3) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"O"]) {
                    if (selectedOption == 4) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }else if ([[arrSepAns objectAtIndex:1] isEqualToString:@"P"]) {
                    if (selectedOption == 5) {
                        //[self saveScore];
                        return YES;
                    }else {
                        return NO;
                    }                    
                }                
            }
                break;
            case 4:
            {
                if (selectedOption == 1) {
                    return YES;
                }else if (selectedOption == 2) {
                    return NO;
                }                
            }
                break;
                
            default:
                break;
        }        
    }*/
    return NO;
}
-(void)manageAttemptCount:(int)optSelect:(BOOL)yOrN
{
    int cntP=0;
    int cntN=0;
    int saveOrNot = 0;
    int ind=0;
    NSMutableDictionary *dictQue = [NSMutableDictionary dictionary];
    NSMutableArray *ars= [NSMutableArray array];
    if (selectStudent == 5) {
        dictQue = [NSMutableDictionary dictionaryWithDictionary:dictQue5];
    }else {
        
        [ars setArray:[dicMQ valueForKey:@"sdic"]];
        for (int i=0; i<[ars count]; i++) {
            if ([[[ars objectAtIndex:i] valueForKey:@"stud"] isEqualToString:[NSString stringWithFormat:@"%d",selectStudent]]) {
                dictQue = [NSMutableDictionary dictionaryWithDictionary:[[ars objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"dic%d",selectStudent]]];
                ind = i+1;
            }
        }
    }
    if ([[[dictQue valueForKey:@"opt1"] valueForKey:@"+"] intValue] == 1) {
        cntP++;
    }else if ([[[dictQue valueForKey:@"opt2"] valueForKey:@"+"] intValue] == 1) {
        cntP++;
    }
    switch (selectLevel) {
        case 1:
        {
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            } 
            if ([[[dictQue valueForKey:@"opt1"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt2"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if (yOrN) {
                if (cntP == 1 && cntN < 3){
                    NSLog(@"y1");
                }else if (cntP == 0 && cntN > 1) {
                    NSLog(@"y2");
                    
                    [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]];
                    
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];     
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:1] forKey:@"+"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 1;
                }else {
                     NSLog(@"y3");
                }  
            }else {
                if (cntP == 1 && cntN == 1) {
                    NSLog(@"n1");
                }else if (cntP == 0 && cntN>=1){
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];
                    int cnt1 = [[[dictQue valueForKey:strO] valueForKey:@"-"] intValue];
                    cnt1++;
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:cnt1] forKey:@"-"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 2;
                    if (tagVPrompt == 1) {
                        [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tag%d",optSelect]];
                    }
                }else {
                    NSLog(@"n3");
                }
            }
        }
            break;
        case 2:
        {
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            }else if ([[[dictQue valueForKey:@"opt4"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            } 
            if ([[[dictQue valueForKey:@"opt1"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt2"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt4"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if (yOrN) {
                if (cntP == 1 && cntN < 4) {
                    NSLog(@"y1");
                }else if (cntP == 0 && cntN > 1){
                    NSLog(@"y2");
                    
                    if (selectStudent == 5) {
                        tagSOrN5 = 1;
                    }else {
                        [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]];
                    }
                    
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];     
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:1] forKey:@"+"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt4"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 1;
                }else {
                    NSLog(@"y3");
                }  
            }else {
                if (cntP == 1 && cntN == 1) {
                    NSLog(@"n1");
                }else if (cntP == 0 && cntN>=1){
                    NSLog(@"n2");
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];
                    int cnt1 = [[[dictQue valueForKey:strO] valueForKey:@"-"] intValue];
                    cnt1++;
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:cnt1] forKey:@"-"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt4"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 2;
                    if (tagVPrompt == 1) {
                        [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tag%d",optSelect]];
                    }
                }else {
                    NSLog(@"n3");
                }
            }
        }
            break;
        case 3:
        {
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            }else if ([[[dictQue valueForKey:@"opt4"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            }else if ([[[dictQue valueForKey:@"opt5"] valueForKey:@"+"] intValue] == 1) {
                cntP++;
            } 
            if ([[[dictQue valueForKey:@"opt1"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt2"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt3"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt4"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt5"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if (yOrN) {
                if (cntP == 1 && cntN < 5) {
                    NSLog(@"y1");
                }else if (cntP == 0 && cntN > 1){
                    NSLog(@"y2");
                    
                    [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]];
                    
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];     
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:1] forKey:@"+"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt4"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt5"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 1;
                }else {
                    NSLog(@"y3");
                }  
            }else {
                if (cntP == 1 && cntN == 1) {
                    NSLog(@"n1");
                }else if (cntP == 0 && cntN>=1){
                    NSLog(@"n2");
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];
                    int cnt1 = [[[dictQue valueForKey:strO] valueForKey:@"-"] intValue];
                    cnt1++;
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:cnt1] forKey:@"-"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++; 
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt3"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt4"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt5"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 2;
                    if (tagVPrompt == 1) {
                        [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tag%d",optSelect]];
                    }
                }else {
                    NSLog(@"n3");
                }
            }
        }
            break;
        case 4:
        {
            if ([[[dictQue valueForKey:@"opt1"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }
            if ([[[dictQue valueForKey:@"opt2"] valueForKey:@"-"] intValue] == 0) {
                cntN++;
            }   
            if (yOrN) {
                if (cntP == 1 && cntN < 2) {
                    NSLog(@"y1");
                }else if (cntP == 0 && cntN > 1){
                    NSLog(@"y2");
                    
                    [[ars objectAtIndex:ind-1] setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"tagSOrN%d",selectStudent]];
                    
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];     
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:1] forKey:@"+"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 1;
                }else {
                    NSLog(@"y3");
                }  
            }else {
                if (cntP == 1 && cntN == 1) {
                    NSLog(@"n1");
                }else if (cntP == 0 && cntN>=1){
                    NSLog(@"n2");
                    NSString *strO = [NSString stringWithFormat:@"opt%d",optSelect];
                    int cnt1 = [[[dictQue valueForKey:strO] valueForKey:@"-"] intValue];
                    cnt1++;
                    [[dictQue valueForKey:strO] setObject:[NSNumber numberWithInt:cnt1] forKey:@"-"];
                    int cnt = [[[dictQue valueForKey:strO] valueForKey:@"attempt"] intValue]; 
                    cnt++;
                    [[dictQue valueForKey:@"opt1"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    [[dictQue valueForKey:@"opt2"] setObject:[NSNumber numberWithInt:cnt] forKey:@"attempt"];
                    saveOrNot = 2;
                }else {
                    NSLog(@"n3");
                }
            }
        }
            break;
        default:
            break;
    }
//    NSLog(@"dictQueSelect:%@",dictQue);
    if (saveOrNot == 0) {
        
    }else if (saveOrNot == 1) {
        if (selectStudent == 5) {
            NSMutableDictionary *di = [NSMutableDictionary dictionaryWithDictionary:dictQue];
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[[self makeRecordString] componentsJoinedByString:@","]],@"condition",di,@"record",[NSNumber numberWithInt:saveOrNot],@"bool", nil];
            NSMutableDictionary *dicTrack = [NSMutableDictionary dictionaryWithObjectsAndKeys:dic,@"dic",[NSString stringWithFormat:@"%d",tagQue],@"que", nil];
            if ([[[arrMVScore valueForKey:@"root"] valueForKey:@"que"] containsObject:[NSString stringWithFormat:@"%d",tagQue]]) {
                [arrMVScore replaceObjectAtIndex:[[[arrMVScore valueForKey:@"root"] valueForKey:@"que"] indexOfObject:[NSString stringWithFormat:@"%d",tagQue]] withObject:[NSMutableDictionary dictionaryWithObject:dicTrack forKey:@"root"]];
            }else {
                [arrMVScore addObject:[NSMutableDictionary dictionaryWithObject:dicTrack forKey:@"root"]];            
            }
            NSLog(@"arrMVScore:%@",arrMVScore);
        }else {
            [[ars objectAtIndex:ind-1] setObject:dictQue forKey:[NSString stringWithFormat:@"dic%d",selectStudent]];
            [dicMQ setObject:ars forKey:@"sdic"];
            NSLog(@"dictMQ:%@",dicMQ);
            [self saveScore];
            [self updateScore];
        }    
    }else if (saveOrNot == 2) {
        if (selectStudent == 5) {
            NSMutableDictionary *di = [NSMutableDictionary dictionaryWithDictionary:dictQue];
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[[self makeRecordString] componentsJoinedByString:@","]],@"condition",di,@"record",[NSNumber numberWithInt:saveOrNot],@"bool", nil];
            NSMutableDictionary *dicTrack = [NSMutableDictionary dictionaryWithObjectsAndKeys:dic,@"dic",[NSString stringWithFormat:@"%d",tagQue],@"que", nil];
            if ([[[arrMVScore valueForKey:@"root"] valueForKey:@"que"] containsObject:[NSString stringWithFormat:@"%d",tagQue]]) {
                [arrMVScore replaceObjectAtIndex:[[[arrMVScore valueForKey:@"root"] valueForKey:@"que"] indexOfObject:[NSString stringWithFormat:@"%d",tagQue]] withObject:[NSMutableDictionary dictionaryWithObject:dicTrack forKey:@"root"]];
            }else {
                [arrMVScore addObject:[NSMutableDictionary dictionaryWithObject:dicTrack forKey:@"root"]];
            }            
            NSLog(@"arrMVScore:%@",arrMVScore);
        }else {
            [[ars objectAtIndex:ind-1] setObject:dictQue forKey:[NSString stringWithFormat:@"dic%d",selectStudent]];
            [dicMQ setObject:ars forKey:@"sdic"];
            NSLog(@"dictMQ:%@",dicMQ);
            [self updateScore];
        }    
    }
}
-(NSMutableArray *)makeRecordString
{    
    NSString *strQType = @"";
    NSString *strCnt = @"";
    NSString *strQt = [self checkForCustomOrModify:@"QType" :[arrQue objectAtIndex:tagQue]];
    if ([strQt isEqualToString:@"What"] || [strQt isEqualToString:@"what"] || [strQt isEqualToString:@"What "]) {
        strQType = @"ScoreWhat";
        strCnt = @"CntWhat";
    }else if ([strQt isEqualToString:@"Where"] || [strQt isEqualToString:@"where"] || [strQt isEqualToString:@"Where "]) {
        strQType = @"ScoreWhere";
        strCnt = @"CntWhere";
    }else if ([strQt isEqualToString:@"When"] || [strQt isEqualToString:@"when"] || [strQt isEqualToString:@"When "]) {
        strQType = @"ScoreWhen";
        strCnt = @"CntWhen";
    }else if ([strQt isEqualToString:@"who"] || [strQt isEqualToString:@"Who"] || [strQt isEqualToString:@"Who "]) {
        strQType = @"ScoreWho";
        strCnt = @"CntWho";
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/YYYY"];
    NSString *str = [df stringFromDate:[NSDate date]];
    NSString *strLevel = [NSString stringWithFormat:@"%d",selectLevel];
    NSString *strVPro = [NSString stringWithFormat:@"%d",tagVPrompt];
    NSString *strQCat = [self checkForCustomOrModify:@"QCat" :[arrQue objectAtIndex:tagQue]];
    //NSString *strQCat = [[arrQue objectAtIndex:tagQue] valueForKey:@"QCat"];
    NSMutableArray *arr = [NSMutableArray array];
    [arr addObject:str];
    [arr addObject:strLevel];
    [arr addObject:strVPro];
    [arr addObject:strQCat];
    [arr addObject:strQType];
    [arr addObject:strCnt];
    return arr;
}
-(void)saveScore
{
    if (tagView == 18) {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/YYYY"];
        NSString *str = [df stringFromDate:[NSDate date]];

        NSString *strQType = @"";
        NSString *strCnt = @"";
//        if (tagQue == [arrQue count]){
//            tagQue = [arrQue count]-1;
//        }
        NSLog(@"tagQue save:%d",tagQue);
        NSString *strQt = [self checkForCustomOrModify:@"QType" :[arrQue objectAtIndex:tagQue]];
        if ([strQt isEqualToString:@"What"] || [strQt isEqualToString:@"what"] || [strQt isEqualToString:@"What "]) {
            strQType = @"ScoreWhat";
            strCnt = @"CntWhat";
        }else if ([strQt isEqualToString:@"Where"] || [strQt isEqualToString:@"where"] || [strQt isEqualToString:@"Where "]) {
            strQType = @"ScoreWhere";
            strCnt = @"CntWhere";
        }else if ([strQt isEqualToString:@"When"] || [strQt isEqualToString:@"when"] || [strQt isEqualToString:@"When "]) {
            strQType = @"ScoreWhen";
            strCnt = @"CntWhen";
        }else if ([strQt isEqualToString:@"who"] || [strQt isEqualToString:@"Who"] || [strQt isEqualToString:@"Who "]) {
            strQType = @"ScoreWho";
            strCnt = @"CntWho";
        }
        NSString *strQc = [self checkForCustomOrModify:@"QCat" :[arrQue objectAtIndex:tagQue]];
        //NSLog(@"strQType:%@\nstrQc:%@",strQType,strQc);
        if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(StudID) FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,[NSString stringWithFormat:@"%d",selectLevel],[NSString stringWithFormat:@"%d",tagVPrompt],strQc]] valueForKey:@"Table1"] valueForKey:@"COUNT(StudID)"] isEqualToString:@"0"]) {

            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],@"StudID",str,@"Date",[NSString stringWithFormat:@"%d",selectLevel],@"Level",strQc,@"Category",[NSString stringWithFormat:@"%d",tagVPrompt],@"VPrompt",@"1",strQType, nil];
            [objDAL insertRecord:dict inTable:@"tblScore"];
        }else {
            int cstPre = [[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT %@ FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strQType,[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,[NSString stringWithFormat:@"%d",selectLevel],[NSString stringWithFormat:@"%d",tagVPrompt],strQc]] valueForKey:@"Table1"] valueForKey:[NSString stringWithFormat:@"%@",strQType]] intValue];
            cstPre= cstPre+1;
            NSString *strM = [NSString stringWithFormat:@"UPDATE tblScore SET %@='%d' WHERE StudID=%@ and Date='%@' and Level=%d and VPrompt=%d and Category='%@'",strQType,cstPre,[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,selectLevel,tagVPrompt,strQc];
            NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:strM]];
            //NSLog(@"DiU:%@",d);
        }        
    }
}
-(void)audioFeedback 
{
    if (tagReinType==1) {
        NSLog(@"tagQue2:%d",tagQue);
        NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
        //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
        NSLog(@"TrackeName:%@",strTrack);
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] 
                       initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
        
        audioPlayer.delegate = self;
        if (error){ 
            NSLog(@"Error: %@", 
                  [error localizedDescription]);
        }else {
            [audioPlayer play];
        }
    }
    if (tagReinType==2) {
        NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
//        NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
        if ([strIN isEqualToString:@"Fantastic.png"]) {
            strIN = @"fantastic.png";
        }else if ([strIN isEqualToString:@"waytogo.png"]){
            strIN = @"Waytogo.png";
        }
        else if ([strIN isEqualToString:@"Awesome.png"]){
            strIN = @"awesome.png";
        }
        imgReinS.image = [UIImage imageNamed:strIN];
        [self.view bringSubviewToFront:viewFinsh];
        viewFinsh.hidden = NO;    
    }
    if (tagReinType == 3) {
        NSLog(@"tagQue2:%d",tagQue);
        NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
//        NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
        NSLog(@"TrackeName:%@",strTrack);
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] 
                       initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
        
        audioPlayer.delegate = self;
        if (error){ 
            NSLog(@"Error: %@", 
                  [error localizedDescription]);
        }else {
            [audioPlayer play];
        }        
        NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
//        NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
        if ([strIN isEqualToString:@"Fantastic.png"]) {
            strIN = @"fantastic.png";
        }else if ([strIN isEqualToString:@"waytogo.png"]){
            strIN = @"Waytogo.png";
        }
        else if ([strIN isEqualToString:@"Awesome.png"]){
            strIN = @"awesome.png";
        }
        imgReinS.image = [UIImage imageNamed:strIN];
        [self.view bringSubviewToFront:viewFinsh];
        viewFinsh.hidden = NO; 
    }
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce) userInfo:nil repeats:NO];
}
-(void)loadAllBone
{
    [self loadBlankView];
    self.view.userInteractionEnabled =YES;
    imgReinS.frame = CGRectMake(302, 223, 421, 322);
    imgReinS.image = [UIImage imageNamed:@"Youfinished.png"];
    [self.view bringSubviewToFront:viewFinsh];
    viewFinsh.hidden = NO;
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce1) userInfo:nil repeats:NO];
}
-(void)showReinforce1
{
    [self.view sendSubviewToBack:viewFinsh];
    viewFinsh.hidden = YES;
}
-(void)audioFeedback1 
{
    if (tagReinSch == 1) {
        if (cntCorrect1 == tagReinI) {
            if (tagReinType==1) {
                if (tagQue == [arrQue count]){
                    tagQue = [arrQue count]-1;
                    //NSLog(@"tagQue == [arrQue count] tagQue6:%d",tagQue);
                }
                NSLog(@"tagQue2:%d",tagQue);
                NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
                //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
                NSLog(@"TrackeName:%@",strTrack);
                if ([strTrack isEqualToString:@"Nicework.m4a"]) {
                    strTrack = @"nicework.m4a";
                }else if ([strTrack isEqualToString:@"Youareright.m4a"]){
                    strTrack = @"youareright.m4a";
                }else if ([strTrack isEqualToString:@"WaytoGo.m4a"]){
                    strTrack = @"waytogo.m4a";
                }
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc] 
                               initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
                
                audioPlayer.delegate = self;
                if (error){ 
                    NSLog(@"Error: %@", 
                          [error localizedDescription]);
                }else {
                    [audioPlayer play];
                }
            }
            if (tagReinType==2) {
                NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
                //NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
                if ([strIN isEqualToString:@"Fantastic.png"]) {
                    strIN = @"fantastic.png";
                }else if ([strIN isEqualToString:@"waytogo.png"]){
                    strIN = @"Waytogo.png";
                }
                else if ([strIN isEqualToString:@"Awesome.png"]){
                    strIN = @"awesome.png";
                }else if ([strIN isEqualToString:@"Youareright.png"]){
                    strIN = @"youareright.png";
                }else if ([strIN isEqualToString:@"Fantastic.png"]){
                    strIN = @"fantastic.png";
                }
                imgReinS.image = [UIImage imageNamed:strIN];
                [self.view bringSubviewToFront:viewFinsh];
                viewFinsh.hidden = NO;
                [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce1) userInfo:nil repeats:NO];
            }
            if (tagReinType==3) {
                if (tagQue == [arrQue count]){
                    tagQue = [arrQue count]-1;
                    //NSLog(@"tagQue == [arrQue count] tagQue7:%d",tagQue);
                }
                NSLog(@"tagQue2:%d",tagQue);
                NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
                //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
                //NSLog(@"TrackeName:%@",strTrack);
                if ([strTrack isEqualToString:@"Nicework.m4a"]) {
                    strTrack = @"nicework.m4a";
                }else if ([strTrack isEqualToString:@"Youareright.m4a"]){
                    strTrack = @"youareright.m4a";
                }else if ([strTrack isEqualToString:@"WaytoGo.m4a"]){
                    strTrack = @"waytogo.m4a";
                }
                NSError *error;
                audioPlayer = [[AVAudioPlayer alloc] 
                               initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
                
                audioPlayer.delegate = self;
                if (error){ 
                    NSLog(@"Error: %@", 
                          [error localizedDescription]);
                }else {
                    [audioPlayer play];
                }
                NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
                //NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
                if ([strIN isEqualToString:@"Fantastic.png"]) {
                    strIN = @"fantastic.png";
                }else if ([strIN isEqualToString:@"waytogo.png"]){
                    strIN = @"Waytogo.png";
                }
                else if ([strIN isEqualToString:@"Awesome.png"]){//
                    strIN = @"awesome.png";
                }else if ([strIN isEqualToString:@"Youareright.png"]){
                    strIN = @"youareright.png";
                }else if ([strIN isEqualToString:@"Fantastic.png"]){
                    strIN = @"fantastic.png";
                }
                imgReinS.image = [UIImage imageNamed:strIN];
                [self.view bringSubviewToFront:viewFinsh];
                viewFinsh.hidden = NO;
                [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce1) userInfo:nil repeats:NO];
            }        
        }        
    }else if (tagReinSch == 2) {
        if (tagReinType==1) {
            if (tagQue == [arrQue count]){
                tagQue = [arrQue count]-1;
                //NSLog(@"tagQue == [arrQue count] tagQue6:%d",tagQue);
            }
            NSLog(@"tagQue2:%d",tagQue);
            NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
            //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
            //NSLog(@"TrackeName:%@",strTrack);
            if ([strTrack isEqualToString:@"Nicework.m4a"]) {
                strTrack = @"nicework.m4a";
            }else if ([strTrack isEqualToString:@"Youareright.m4a"]){
                strTrack = @"youareright.m4a";
            }else if ([strTrack isEqualToString:@"WaytoGo.m4a"]){
                strTrack = @"waytogo.m4a";
            }
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] 
                           initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
            
            audioPlayer.delegate = self;
            if (error){ 
                NSLog(@"Error: %@", 
                      [error localizedDescription]);
            }else {
                [audioPlayer play];
            }
        }
        if (tagReinType==2) {
            NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
            //NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
            if ([strIN isEqualToString:@"Fantastic.png"]) {
                strIN = @"fantastic.png";
            }else if ([strIN isEqualToString:@"waytogo.png"]){
                strIN = @"Waytogo.png";
            }
            else if ([strIN isEqualToString:@"Awesome.png"]){
                strIN = @"awesome.png";
            }else if ([strIN isEqualToString:@"Youareright.png"]){
                strIN = @"youareright.png";
            }else if ([strIN isEqualToString:@"Fantastic.png"]){
                strIN = @"fantastic.png";
            }
            imgReinS.image = [UIImage imageNamed:strIN];
            [self.view bringSubviewToFront:viewFinsh];
            viewFinsh.hidden = NO;
            [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce1) userInfo:nil repeats:NO];
        }
        if (tagReinType==3) {
            if (tagQue == [arrQue count]){
                tagQue = [arrQue count]-1;
                //NSLog(@"tagQue == [arrQue count] tagQue7:%d",tagQue);
            }
            NSLog(@"tagQue2:%d",tagQue);
            NSString *strTrack = [self checkForCustomOrModify:@"AudioRein" :[arrQue objectAtIndex:tagQue]];
            //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"AudioRein"];
            //NSLog(@"TrackeName:%@",strTrack);
            if ([strTrack isEqualToString:@"Nicework.m4a"]) {
                strTrack = @"nicework.m4a";
            }else if ([strTrack isEqualToString:@"Youareright.m4a"]){
                strTrack = @"youareright.m4a";
            }else if ([strTrack isEqualToString:@"WaytoGo.m4a"]){
                strTrack = @"waytogo.m4a";
            }
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] 
                           initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
            
            audioPlayer.delegate = self;
            if (error){ 
                NSLog(@"Error: %@", 
                      [error localizedDescription]);
            }else {
                [audioPlayer play];
            }
            
            NSString *strIN = [self checkForCustomOrModify:@"VisualRein" :[arrQue objectAtIndex:tagQue]];
            //NSString *strIN = [[arrQue objectAtIndex:tagQue] valueForKey:@"VisualRein"];
            if ([strIN isEqualToString:@"Fantastic.png"]) {
                strIN = @"fantastic.png";
            }else if ([strIN isEqualToString:@"waytogo.png"]){
                strIN = @"Waytogo.png";
            }
            else if ([strIN isEqualToString:@"Awesome.png"]){
                strIN = @"awesome.png";
            }else if ([strIN isEqualToString:@"Youareright.png"]){
                strIN = @"youareright.png";
            }else if ([strIN isEqualToString:@"Fantastic.png"]){
                strIN = @"fantastic.png";
            }
            imgReinS.image = [UIImage imageNamed:strIN];
            [self.view bringSubviewToFront:viewFinsh];
            viewFinsh.hidden = NO;
            [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showReinforce1) userInfo:nil repeats:NO];
        }       
    }
}
-(void)wrongSelectionReinforce
{
    if (tagReinType==1 || tagReinType ==3  || tagAFB == 1) {
        if (tagQue == [arrQue count]){
            tagQue = [arrQue count]-1;
            //NSLog(@"tagQue == [arrQue count] tagQue7:%d",tagQue);
        }
//        NSLog(@"tagQue8:%d",tagQue);
        NSString *strTrack = [self checkForCustomOrModify:@"QAFeedback" :[arrQue objectAtIndex:tagQue]];
        //NSString *strTrack = [[arrQue objectAtIndex:tagQue] valueForKey:@"QAFeedback"];
        //NSLog(@"AFeedback-Track:%@",strTrack);
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] 
                       initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:strTrack ofType:nil]] error:&error];
        
        audioPlayer.delegate = self;
        if (error){ 
            NSLog(@"Error: %@", 
                  [error localizedDescription]);
        }else {
            [audioPlayer play];
        }    
    }
}
-(void)updateScore {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/YYYY"];
    NSString *str = [df stringFromDate:[NSDate date]];
    
    NSString *strQType = @"";
    NSString *strCnt = @"";
//    if (tagQue == [arrQue count]){
//        tagQue = [arrQue count]-1;
//    }
    NSLog(@"tagQue up:%d",tagQue);
    NSString *strQt = [self checkForCustomOrModify:@"QType" :[arrQue objectAtIndex:tagQue]];
    if ([strQt isEqualToString:@"What"] || [strQt isEqualToString:@"what"] || [strQt isEqualToString:@"What "]) {
        strQType = @"ScoreWhat";
        strCnt = @"CntWhat";
    }else if ([strQt isEqualToString:@"Where"] || [strQt isEqualToString:@"where"] || [strQt isEqualToString:@"Where "]) {
        strQType = @"ScoreWhere";
        strCnt = @"CntWhere";
    }else if ([strQt isEqualToString:@"When"] || [strQt isEqualToString:@"when"] || [strQt isEqualToString:@"When "]) {
        strQType = @"ScoreWhen";
        strCnt = @"CntWhen";
    }else if ([strQt isEqualToString:@"who"] || [strQt isEqualToString:@"Who"] || [strQt isEqualToString:@"Who "]) {
        strQType = @"ScoreWho";
        strCnt = @"CntWho";
    }
    NSString *strQc = [self checkForCustomOrModify:@"QCat" :[arrQue objectAtIndex:tagQue]];
    if ([[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT COUNT(StudID) FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,[NSString stringWithFormat:@"%d",selectLevel],[NSString stringWithFormat:@"%d",tagVPrompt],strQc]] valueForKey:@"Table1"] valueForKey:@"COUNT(StudID)"] isEqualToString:@"0"]) {
        //            NSLog(@"strQType:%@",strQType);
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],@"StudID",str,@"Date",[NSString stringWithFormat:@"%d",selectLevel],@"Level",strQc,@"Category",[NSString stringWithFormat:@"%d",tagVPrompt],@"VPrompt",@"1",strCnt, nil];
        [objDAL insertRecord:dict inTable:@"tblScore"];
    }else {
        int cntPre = [[[[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT %@ FROM tblScore WHERE StudID='%@' and Date='%@' and Level='%@' and VPrompt='%@' and Category='%@'",strCnt,[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,[NSString stringWithFormat:@"%d",selectLevel],[NSString stringWithFormat:@"%d",tagVPrompt],strQc]] valueForKey:@"Table1"] valueForKey:[NSString stringWithFormat:@"%@",strCnt]] intValue];            
        cntPre = cntPre+1;
        
        NSString *strM = [NSString stringWithFormat:@"UPDATE tblScore SET %@='%d' WHERE StudID=%@ and Date='%@' and Level=%d and VPrompt=%d and Category='%@'",strCnt,cntPre,[[arrStud objectAtIndex:selectStudent-1] valueForKey:@"StudID"],str,selectLevel,tagVPrompt,strQc];
        NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:strM]];
        //NSLog(@"DiU:%@",d);
    }        
}
-(void)loadBlankView
{
    btnPOS.hidden = YES;
    btnStudent1.hidden = YES;
    lblStudent1.hidden = YES;
    btnStudent2.hidden = YES;
    lblStudent2.hidden = YES;
    btnStudent3.hidden = YES;
    lblStudent3.hidden = YES;
    btnStudent4.hidden = YES;
    lblStudent4.hidden = YES;    
    imgView1.hidden=YES;
    imgView2.hidden=YES;
    imgView3.hidden=YES;
    imgView4.hidden=YES;
    imgView5.hidden=YES;            
    imgView6.hidden=YES;
    imgView7.hidden=YES;
    imgView8.hidden=YES;
    imgView9.hidden=YES;
    imgView10.hidden=YES;
    [self.view viewWithTag:1].hidden=YES;
    [self.view viewWithTag:2].hidden=YES;
    [self.view viewWithTag:3].hidden=YES;
    [self.view viewWithTag:4].hidden=YES;
    [self.view viewWithTag:5].hidden=YES;
    imgBG1.hidden=YES;
    imgBG2.hidden=YES;
    imgBG3.hidden=YES;
    imgBG4.hidden=YES;
    imgBG5.hidden=YES;
    lblQue.hidden = YES;
}
#pragma
#pragma mark AVAudioPlayer delegate methods
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [btnPOS setTitle:@"Play Audio" forState:UIControlStateNormal];
    if (player.playing) {
        [player stop];
    }
    btnPOS.tag = 21;
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
//	NSLog(@"Decode Error occurred");
}
@end
