//
//  GroupVC.h
//  Ques2Learn
//
//  Created by apple on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsVC.h"
#import "OptionVC1.h"
@protocol GroupVCDelegate
- (void)groupSettingsDone;
@end
@interface GroupVC : UIViewController <UIPopoverControllerDelegate,OptionsVCDelegate,OptionsVC1Delegate>{
    OptionsVC *viewOptionsVC;
    OptionVC1 *viewOptionsVC1;
    id<GroupVCDelegate> delegate;
    int btnTag;
    UIPopoverController *popOverOVC;
    DAL *objDAL;
}
@property (nonatomic, retain) OptionsVC *viewOptionsVC;
@property (nonatomic, retain) OptionVC1 *viewOptionsVC1;
@property (nonatomic, retain) UIPopoverController *popOverOVC;
@property (nonatomic, retain) id<GroupVCDelegate> delegate;
-(IBAction)clickDone:(id)sender;
-(IBAction)clickSetOptions:(id)sender;
-(IBAction)clickSwitchOpt:(id)sender;
@end
