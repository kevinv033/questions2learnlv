//
//  GroupVC.m
//  Ques2Learn
//
//  Created by apple on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GroupVC.h"

@implementation GroupVC
@synthesize delegate = _delegate;
@synthesize viewOptionsVC = _viewOptionsVC;
@synthesize viewOptionsVC1 = _viewOptionsVC1;
@synthesize popOverOVC = _popOverOVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblGroup where GroupID='1'"]]];
    UIButton *btn = (UIButton*)[self.view viewWithTag:1];
    UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
    UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
    UIButton *btn3 = (UIButton*)[self.view viewWithTag:4];
    UIButton *btn4 = (UIButton*)[self.view viewWithTag:5];
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 1"]) {
        [btn setTitle:@"Level 1: 3 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 2"]) {
        [btn setTitle:@"Level 2: 4 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 3"]) {
        [btn setTitle:@"Level 3: 5 pictures" forState:UIControlStateNormal];
    }else if ([[[dict valueForKey:@"Table1"] valueForKey:@"Level"] isEqualToString:@"Level 4"]) {
        [btn setTitle:@"Level 4: no pictures" forState:UIControlStateNormal];
    }
    [btn1 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"ReinType"] forState:UIControlStateNormal];
    [btn2 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"ReinSchedule"] forState:UIControlStateNormal];
    [btn3 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"QueCat"] forState:UIControlStateNormal];
    [btn4 setTitle:[[dict valueForKey:@"Table1"] valueForKey:@"QueType"] forState:UIControlStateNormal];
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"VPrompt"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:10];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:10];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"AFeedback"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:11];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:11];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"SpokenVoice"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:12];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:12];
        swt.on=NO;
    }
    if ([[[dict valueForKey:@"Table1"] valueForKey:@"VisualTrials"] isEqualToString:@"ON"]) {
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:13];
        swt.on=YES;
    }else{
        UISwitch *swt = (UISwitch*)[self.view viewWithTag:13];
        swt.on=NO;
    }
    btnTag = 0;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark-
#pragma mark OptionVC delegate methods
- (void)rowOptionSelected:(NSString *)selectedStr {
    
    [self.popOverOVC dismissPopoverAnimated:YES];
    if ([selectedStr isEqualToString:@""]) {
        return;
    }
    if (btnTag==0) {
        
    }else if (btnTag==1 || btnTag==2 || btnTag==3 || btnTag==4){
        switch (btnTag) {
            case 1:
            {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:[[selectedStr componentsSeparatedByString:@":"] objectAtIndex:0],@"Level", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
            }
                break;
            case 2:
            {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinType", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
                break;
            case 3:
            {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"ReinSchedule", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
                break;
            default:
                break;
        }
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else{
    }
    
}
#pragma mark-
#pragma mark OptionVC1 delegate methods
- (void)rowOptionSelected1:(NSString *)selectedStr {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if ([selectedStr isEqualToString:@""]) {
        return;
    }
    if (btnTag==0) {
        
    }else if (btnTag==5){
        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueType", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else if (btnTag==4){
        [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:selectedStr,@"QueCat", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }else{
    }
}
#pragma mark-
#pragma mark custom methods

-(void)clickDone:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
    [_delegate groupSettingsDone];
}
-(void)clickSetOptions:(id)sender
{
    
    UIButton *btn = sender;
    NSLog(@"Tag:%d",btn.tag);
    btnTag = btn.tag;
    NSString *strStudID =@"Group";
    switch (btn.tag) {
        case 1:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Level 1: 3 pictures",@"Level 2: 4 pictures",@"Level 3: 5 pictures",@"Level 4: no pictures", nil]:strStudID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(250, -145, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];        
        }
            break;
        case 2:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Auditory",@"Visual",@"Both",@"None", nil]:strStudID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(250, 40, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 3:
        {
            self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
            _viewOptionsVC.delegate=self;
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
            [_viewOptionsVC getOptions:[NSArray arrayWithObjects:@"Intermittent",@"Continuous", nil]:strStudID];
            [self.popOverOVC presentPopoverFromRect:CGRectMake(250, 94, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 4:
        {
            //
            NSMutableArray *arrC = [NSMutableArray array];
            [arrC addObjectsFromArray:[NSArray arrayWithObjects:@"Community",@"Food & Drinks",@"Health",@"Home",@"Leisure/Recreational",@"School", nil]];
            NSMutableArray *arrStud = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:@"StudSettings" withCondition:@"StudGroup=" withColumnValue:@"'ON'"]];
            for (int i=0 ; i<[arrStud count]; i++) {
                NSMutableDictionary *diC = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"SELECT DISTINCT QCat FROM tblCustom where StudID='%@';",[[arrStud objectAtIndex:i] valueForKey:@"StudID"]]]];
                for (int i=0; i<[diC count]; i++) {
                    if ([arrC containsObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]]) {
                        
                    }else {
                        [arrC addObject:[[diC valueForKey:[NSString stringWithFormat:@"Table%d",i+1]] valueForKey:@"QCat"]];
                    }
                }
            }
            [arrC insertObject:@"all" atIndex:0];
            //
            self.viewOptionsVC1 = [[OptionVC1 alloc] initWithNibName:@"OptionVC1" bundle:nil];
            _viewOptionsVC1.delegate=self;
            [_viewOptionsVC1 getOptions:arrC:2];
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC1];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 174);            
            [self.popOverOVC presentPopoverFromRect:CGRectMake(250, 195, 285, 174) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;
        case 5:
        {
            self.viewOptionsVC1 = [[OptionVC1 alloc] initWithNibName:@"OptionVC1" bundle:nil];
            _viewOptionsVC1.delegate=self;
            [_viewOptionsVC1 getOptions:[NSArray arrayWithObjects:@"all",@"who",@"what",@"where",@"when", nil]:1];            
            self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC1];
            self.popOverOVC.popoverContentSize = CGSizeMake(285, 174);            
            [self.popOverOVC presentPopoverFromRect:CGRectMake(250, 252, 285, 174) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        default:
            break;
    }
    
}
-(void)clickSwitchOpt:(id)sender
{
    
    UISwitch *swt = sender;
    switch (swt.tag) {
        case 10:
        {
            if ([swt isOn]) {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VPrompt", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
            }else{
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VPrompt", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
        }
            break;
        case 11:
        {
            if ([swt isOn]) {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"AFeedback", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
            }else{
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"AFeedback", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
        }
            break;
        case 12:
        {
            if ([swt isOn]) {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"SpokenVoice", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
            }else{
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"SpokenVoice", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
        }
            break;
        case 13:
        {
            if ([swt isOn]) {
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"ON",@"VisualTrials", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
            }else{
                [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"OFF",@"VisualTrials", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];                
            }
        }
            break;
        default:
            break;
    }
}
@end
