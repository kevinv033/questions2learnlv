//
//  MainViewAppDelegate.m
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewAppDelegate.h"
#import "DataCollectChartVC.h"

@implementation MainViewAppDelegate

@synthesize window = _window;
@synthesize navController,obj;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self checkAndCreateDatabase];
    [self.window makeKeyAndVisible];
    return YES;
}
#pragma mark - 
#pragma mark - custom methods

-(void)checkAndCreateDatabase
{
	BOOL success;
	
	NSString *databaseName = @"Que2learn.sqlite";
	
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	dbPath = [documentsDir stringByAppendingPathComponent:databaseName];	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	success = [fileManager fileExistsAtPath:dbPath];
	
	if(success) return;
	
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	
	[fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
	
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if ([self.navController.viewControllers count]>1) {
        if ([[self.navController.viewControllers objectAtIndex:1] isKindOfClass:[DataCollectChartVC class]] && [self.navController.viewControllers count]==2) {
            DataCollectChartVC *viewD = (DataCollectChartVC*)[self.navController.viewControllers objectAtIndex:1];
            if (obj) {
                UIPopoverController *pop = obj;
                [pop dismissPopoverAnimated:NO];
                obj = nil;
            }
            [viewD.popOverOVC dismissPopoverAnimated:NO];
            [self.navController popViewControllerAnimated:NO];
        }        
    }
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
