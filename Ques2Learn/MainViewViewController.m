//
//  MainViewViewController.m
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewViewController.h"
//#import "PickStudentVC.h"
#import "DataCollectChartVC.h"
#import "GoVC.h"
#import "CreditsVC.h"
#import "MainViewAppDelegate.h"
@implementation MainViewViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}//20*16.5

- (void)viewWillDisappear:(BOOL)animated
{
    
	[super viewWillDisappear:animated];
}
 
- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma -
#pragma custom methods
-(void)clickOptions:(id)sender{
    UIButton *btn = sender;
    switch (btn.tag) {
        case 1:
        {
            UIViewController *viewController = [[DataCollectChartVC alloc] initWithNibName:@"DataCollectChartVC" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
            break;
        case 2:
        {
            GoVC *viewController = [[GoVC alloc] initWithNibName:@"GoVC" bundle:nil];
            [viewController getViewTag:17];
            [self.navigationController pushViewController:viewController animated:YES];    
        }
            break;
        case 3:
        {
            
        }
            break;
        case 4:
        {
            UIViewController *viewController = [[CreditsVC alloc] initWithNibName:@"CreditsVC" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];            
        }
            break;            
        default:
            break;
    }
}
-(IBAction)clickUpgrade:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/us/app/questions2learn/id537209705?ls=1&mt=8"];
    [[UIApplication sharedApplication] openURL:url];
}
@end
