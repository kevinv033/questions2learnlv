//
//  ModifyPreProgVC.h
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface ModifyPreProgVC : UIViewController <UITableViewDelegate,UITableViewDataSource>{
    int tagLevel;
    NSMutableArray *arrQue;
    DAL *objDAL;
    int idQue;
    NSString *strStudID;
    IBOutlet UITableView *tbl;
}
-(void)getStudID:(NSString*)idStr;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickBack:(id)sender;
-(void)reloadTable;
-(IBAction)clickChooseLevel:(id)sender;
-(IBAction)clickModify:(id)sender;
@end
