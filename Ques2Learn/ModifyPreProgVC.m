//
//  ModifyPreProgVC.m
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModifyPreProgVC.h"
#import "ModifyVC.h"
@implementation ModifyPreProgVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    tagLevel = 1;
    idQue = 0;
    arrQue = [[NSMutableArray alloc] init];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];

    UIButton *btn = (UIButton*)[self.view viewWithTag:1];
    btn.layer.cornerRadius = 2;
    btn.layer.borderWidth = 2;
    btn.layer.borderColor = [UIColor blackColor].CGColor;
    UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
    btn1.layer.cornerRadius = 2;
    btn1.layer.borderWidth = 2;
    btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
    btn2.layer.cornerRadius = 2;
    btn2.layer.borderWidth = 2;
    btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self reloadTable];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrQue count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [[arrQue objectAtIndex:indexPath.row] valueForKey:@"que"];
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    idQue = [[[arrQue objectAtIndex:indexPath.row] valueForKey:@"id"] intValue];
}
#pragma mark - Custom methods

-(void)getStudID:(NSString*)idStr
{
    strStudID = idStr;
}
-(void)clickHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)reloadTable
{
    [arrQue removeAllObjects];
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[objDAL SelectWithStar:[NSString stringWithFormat:@"tblLevel%d",tagLevel]]];
    for (int i=0; i<20; i++) {
        NSDictionary *di = [NSDictionary dictionaryWithObjectsAndKeys:[[arr objectAtIndex:i] valueForKey:@"id"],@"id",[[arr objectAtIndex:i] valueForKey:@"QText"],@"que", nil];
        [arrQue addObject:di];
    }
    tbl.delegate=self;
    tbl.dataSource=self;
    [tbl reloadData];
}
-(void)clickChooseLevel:(id)sender
{
    UIButton *btn = sender;
    btn.layer.borderColor = [UIColor blackColor].CGColor;
    
    switch (btn.tag) {
        case 1:
        {
            tagLevel=1;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
            break;
        case 2:
        {
            tagLevel=2;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:1];
            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:3];
            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
            break;
        case 3:
        {
            tagLevel=3;
            UIButton *btn1 = (UIButton*)[self.view viewWithTag:1];
            btn1.layer.borderColor = [UIColor lightGrayColor].CGColor;
            UIButton *btn2 = (UIButton*)[self.view viewWithTag:2];
            btn2.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
            break;
        default:
            break;
    }
   // [self reloadTable];
}
-(void)clickModify:(id)sender
{
    if (idQue==0 && tagLevel==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Question and Level are not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else if (idQue==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Question is not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else if (tagLevel==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Level is not selected" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    NSMutableArray *ar = [NSMutableArray array];
    [ar setArray:[objDAL SelectWithStar:@"tblModify" withCondition:[NSString stringWithFormat:@"StudID='%@'",strStudID] withColumnValue:@""]];
    if ([ar count]==1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy the full version of Questions2Learn to modify more programmed answer choices." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    ModifyVC *viewController = [[ModifyVC alloc] initWithNibName:nil bundle:nil];
    [viewController getID:idQue :tagLevel:strStudID];
    [self.navigationController pushViewController:viewController animated:YES];    
}
@end
