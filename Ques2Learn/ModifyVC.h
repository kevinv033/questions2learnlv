//
//  ModifyVC.h
//  Ques2Learn
//
//  Created by apple on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectSVC.h"

#import "NonRotatingUIImagePickerController.h"
@interface ModifyVC : UIViewController <UIPopoverControllerDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,UIScrollViewDelegate,SelectSVCDelegate> {
    
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UIView *view4;
    IBOutlet UIView *view5;
    IBOutlet UIButton *btnAns1;
    IBOutlet UIButton *btnAns2;
    IBOutlet UIButton *btnAns3;
    IBOutlet UIButton *btnAns4;
    IBOutlet UIButton *btnAns5;
    IBOutlet UIButton *btnCorAns1;
    IBOutlet UIButton *btnCorAns2;
    IBOutlet UIButton *btnCorAns3;
    IBOutlet UIButton *btnCorAns4;
    IBOutlet UIButton *btnCorAns5;
    IBOutlet UITextView *txtQue;
    
    SelectSVC *viewSVC;
    //
    int idQue;
    int idLevel;
    NSString *strStudID;
    DAL *objDAL;
    NSMutableDictionary *dictQue;
    int corAns;
    int tagOptions;
    CGFloat X,Y;
    int popDir;
    UIImagePickerController * pickerPhoto;
    UIViewController *containerController;
    int tagModify1;
    int tagModify2;
    int tagModify3;
    int tagModify4;
    int tagModify5;
    NSMutableArray *arrPhotos;
    NSMutableDictionary *dS;
}
@property (nonatomic, retain) SelectSVC *viewSVC;
@property (nonatomic, retain) UIPopoverController *popOverOVC;
-(void)loadQueView;
-(IBAction)setCorrectAns:(id)sender;
-(void)getID:(int)queID:(int)levelID:(NSString*)idStud;
-(UIImage*)loadImage:(NSString *)imgN;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickBack:(id)sender;
-(IBAction)clickSelectOptions:(id)sender;
-(UIImage *)rotateImage:(UIImage *)image;
-(IBAction)clickSave:(id)sender;
-(NSString*)nameModify;
-(NSString*)namePhoto:(int)img;
-(BOOL)removeImage:(NSString*)strImg;
-(NSString*)clickSaveImage:(NSData*)dataBtnImg:(int)btn:(NSString*)nameStr;
-(NSString*)getCorrectAns;
-(void)choosePreProgrmmedPhoto:(id)sender;
@end
