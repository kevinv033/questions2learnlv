//
//  OptionVC1.h
//  Ques2Learn
//
//  Created by apple on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OptionsVC1Delegate
- (void)rowOptionSelected1:(NSString *)selectedStr;
@end
@interface OptionVC1 : UIViewController <UITableViewDelegate,UITableViewDataSource>{
    id<OptionsVC1Delegate> _delegate;
    NSMutableArray *arrGroup;  
    NSMutableArray *_arrStudents;
    NSMutableArray *arrCollect;
//    UIPickerView *picViewOptions;
    UITableView *tblOptions;
    int tagOpt;
}
@property (nonatomic, retain) NSMutableArray *arrStudents;
@property (nonatomic, retain) id<OptionsVC1Delegate> delegate;
-(void)clickDone1:(id)sender;
-(IBAction)clickSelectRow:(int)sender;
-(void)getOptions:(NSMutableArray*)arrGet:(NSInteger)optTag;
@end
