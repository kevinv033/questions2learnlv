//
//  OptionsVC.m
//  Ques2Learn
//
//  Created by apple on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OptionsVC.h"
@implementation OptionsVC
@synthesize arrOptions = _arrOptions;
@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objDAL = [[DAL alloc] initDatabase:@"Que2learn.sqlite"];
	UIButton *btnDelete=[UIButton buttonWithType:UIButtonTypeCustom];
	[btnDelete addTarget:self action:@selector(clickDone:)forControlEvents:UIControlEventTouchDown];
	[btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
	[btnDelete setImage:[UIImage imageNamed:@"btnDone.png"] forState:UIControlStateNormal];
	btnDelete.frame = CGRectMake(230, 10, 50, 26);
	[self.view addSubview:btnDelete];
    
	UIPickerView *picViewOptions = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 285, 200)];
	picViewOptions.delegate = self;
	picViewOptions.dataSource = self;
	picViewOptions.showsSelectionIndicator = YES;
	[self pickerView:picViewOptions didSelectRow:0 inComponent:0];	
	tagRow = 0;
	[self.view addSubview:picViewOptions];
	[self.view	setBackgroundColor:[UIColor clearColor]];    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark -
#pragma mark UIPickerView delegate methods
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_arrOptions count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_arrOptions objectAtIndex:row];
}
// tell the picker the selected row for a given component
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    if ([[_arrOptions objectAtIndex:row] isEqualToString:@"Intermittent"]) {
        UIViewController *v= [[UIViewController alloc] init];
        v.view.frame = CGRectMake(0, 0, 120, 100);
        NSMutableDictionary *dict;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from tblGroup where GroupID='1'"]]];
        }else {
            dict = [NSMutableDictionary dictionaryWithDictionary:[objDAL executeDataSet:[NSString stringWithFormat:@"select * from StudSettings where StudID='%@'",strStudID]]];
        }
//        if ([strStudID isEqualToString:@"Group"]) {
//            
//        }else {
//            
//        }
        NSString *strRI=@"";
        if ([dict count]>0) {
            strRI = [[dict valueForKey:@"Table1"] valueForKey:@"ReinInterval"];
        }
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn addTarget:self action:@selector(btnSelectOp:)forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1;
        btn.frame = CGRectMake(0, 8, 36, 36);
        [v.view addSubview:btn];
        UIButton *btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn1 addTarget:self action:@selector(btnSelectOp:)forControlEvents:UIControlEventTouchUpInside];
        btn1.tag = 2;
        btn1.frame = CGRectMake(0, 58, 36, 36);
        if ([strRI isEqualToString:@"2"]) {
            [btn setBackgroundImage:[UIImage imageNamed:@"check@2x.png"] forState:UIControlStateNormal];
            [btn1 setBackgroundImage:nil forState:UIControlStateNormal];
        }else if ([strRI isEqualToString:@"3"]){
            [btn setBackgroundImage:nil forState:UIControlStateNormal];
            [btn1 setBackgroundImage:[UIImage imageNamed:@"check@2x.png"] forState:UIControlStateNormal];            
        }
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(46, 8, 75, 36)];
        lbl.text = @"2nd time";
        UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(46, 58, 75, 36)];
        lbl1.text = @"3rd time";
        [v.view addSubview:btn];
        [v.view addSubview:btn1];
        [v.view addSubview:lbl];
        [v.view addSubview:lbl1];
        popV = [[UIPopoverController alloc] initWithContentViewController:v];
        popV.delegate =self;
        popV.popoverContentSize = CGSizeMake(120, 100);
        [popV presentPopoverFromRect:CGRectMake(170, 80, 120, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    tagRow =row;
}
#pragma -
#pragma custom methods
-(void)btnSelectOp:(id)sender {
    NSLog(@"Tag");
    UIButton *btn = sender;
    if (btn.tag == 1) {
        UIButton *btn1 = (UIButton*)[self.view viewWithTag:2];
        [btn1 setBackgroundImage:nil forState:UIControlStateNormal];        
        [btn setBackgroundImage:[UIImage imageNamed:@"check@2x.png"] forState:UIControlStateNormal];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"2",@"ReinInterval", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        }else {
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"2",@"ReinInterval", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStudID]];
        }
    }else if (btn.tag == 2) {
        UIButton *btn1 = (UIButton*)[self.view viewWithTag:1];
        [btn1 setBackgroundImage:nil forState:UIControlStateNormal];        
        [btn setBackgroundImage:[UIImage imageNamed:@"check@2x.png"] forState:UIControlStateNormal];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Group"] == 2){
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"3",@"ReinInterval", nil] forID:@"GroupID=" inTable:@"tblGroup" withValue:@"1"];
        }else {
            [objDAL updateRecord:[NSDictionary dictionaryWithObjectsAndKeys:@"3",@"ReinInterval", nil] forID:@"StudID=" inTable:@"StudSettings" withValue:[NSString stringWithFormat:@"'%@'",strStudID]];
        }        
    }
    [popV dismissPopoverAnimated:YES];
}
-(void)clickDone:(id)sender
{
    [_delegate rowOptionSelected:[_arrOptions objectAtIndex:tagRow]];
}
-(void)getOptions:(NSMutableArray*)arrGet:(NSString*)idStr
{
    self.arrOptions = [NSMutableArray array];
    _arrOptions = [NSMutableArray arrayWithArray:arrGet];
    strStudID =idStr;
}
@end
