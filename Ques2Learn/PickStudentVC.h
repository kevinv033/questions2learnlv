//
//  PickStudentVC.h
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsVC.h"
@interface PickStudentVC : UIViewController<UIPopoverControllerDelegate,OptionsVCDelegate>{
    NSMutableArray *arrGroup;  
    NSMutableArray *arrStudents;
    NSMutableArray *arrCollect;
    OptionsVC *viewOptionsVC;
    UIPopoverController *popOverOVC;
    int btnTag;
    IBOutlet UIImageView *imgSwtNT;
}
@property (nonatomic, retain) OptionsVC *viewOptionsVC;
@property (nonatomic, retain) UIPopoverController *popOverOVC;
-(IBAction)clickHome:(id)sender;
-(IBAction)clickSwitch:(id)sender;
-(IBAction)clickSetCategory:(id)sender;
-(IBAction)clickGo:(id)sender;
-(IBAction)clickDC:(id)sender;
@end
