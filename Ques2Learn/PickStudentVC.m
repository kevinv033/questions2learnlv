//
//  PickStudentVC.m
//  Ques2Learn
//
//  Created by apple on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PickStudentVC.h"
#import "GoVC.h"
#import "DataCollectChartVC.h"
@implementation PickStudentVC
@synthesize viewOptionsVC = _viewOptionsVC;
@synthesize popOverOVC = _popOverOVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrGroup = [[NSMutableArray alloc] init];
    arrStudents = [[NSMutableArray alloc] init];
    for (int i=0; i<8; i++) {
        [arrStudents addObject:[NSString stringWithFormat:@"Student:%d",i]];
    }
    arrCollect = [[NSMutableArray alloc] init];
    for (int i=0; i<[arrStudents count]; i++) {
        [arrCollect addObject:@"NO"];
    }
    btnTag=0;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
#pragma mark -
#pragma mark UITableView Delegate mathods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrStudents count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    if ([[arrCollect objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:12];
    cell.textLabel.text = [arrStudents objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; 
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark){
        [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
        [arrGroup removeObject:[arrStudents objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"NO"];
    }
    else {
        if ([arrGroup count]==4) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Four Students are seleted" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
        [arrGroup addObject:[arrStudents objectAtIndex:indexPath.row]];
        [arrCollect replaceObjectAtIndex:indexPath.row withObject:@"YES"];
    }
    NSLog(@"arrCollect:%@",arrCollect);
}
#pragma mark -
#pragma OptionVC delegate methods
- (void)rowOptionSelected:(NSString *)selectedStr {
    [self.popOverOVC dismissPopoverAnimated:YES];
    if (btnTag==0) {
        
    }else{
        UIButton *btn = (UIButton*)[self.view viewWithTag:btnTag];
        [btn setTitle:selectedStr forState:UIControlStateNormal];
    }
}
#pragma mark-
#pragma mark custom methods
-(void)clickHome:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)clickSwitch:(id)sender
{
    UISwitch *swt = sender;
    switch (swt.tag) {
        case 10:
            NSLog(@"10");
            break;
        case 11:
            NSLog(@"11");
            break;
        case 12:
            NSLog(@"12");
            break;
        case 13:
            NSLog(@"13");
            break;
        case 14:
            NSLog(@"14");
            break;
        case 15:{
            if ([swt isOn]) {
                imgSwtNT.image = [UIImage imageNamed:@"On5.png"];
                swt.on =YES;
            }else{
                imgSwtNT.image = [UIImage imageNamed:@"On10.png"];
                swt.on =NO;
            }
        }
            break;
            
        default:
            break;  
    }
}
-(void)clickSetCategory:(id)sender
{   
    UIButton *btn = sender;
    self.viewOptionsVC = [[OptionsVC alloc] initWithNibName:@"OptionsVC" bundle:nil];
    _viewOptionsVC.delegate=self;
    self.popOverOVC = [[UIPopoverController alloc] initWithContentViewController:_viewOptionsVC];
    self.popOverOVC.popoverContentSize = CGSizeMake(285, 220);
    btnTag=btn.tag;
    switch (btn.tag) {
        case 1:
        {
            [self.popOverOVC presentPopoverFromRect:CGRectMake(365, 90, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 2:
        {
            [self.popOverOVC presentPopoverFromRect:CGRectMake(365, 136, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        } 
            break;
        case 3:
        {
            [self.popOverOVC presentPopoverFromRect:CGRectMake(365, 180, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
        case 4:
        {
            [self.popOverOVC presentPopoverFromRect:CGRectMake(365, 300, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;
        case 5:
        {
            [self.popOverOVC presentPopoverFromRect:CGRectMake(365, 344, 285, 220) 
                                             inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];            
        }
            break;            
    }
    
}
-(void)clickGo:(id)sender
{
    UIViewController *viewController = [[GoVC alloc] initWithNibName:@"GoVC" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)clickDC:(id)sender
{
    UIViewController *viewController = [[DataCollectChartVC alloc] initWithNibName:@"DataCollectChartVC" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
